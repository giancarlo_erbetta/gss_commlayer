
#include "stdafx.h"
#include "MMFLib.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Costruttore
//////////////////////////////////////////////////////////////////////
CMMFLib::CMMFLib()
{
	// All' inizio il file non e' mappato (quindi non e' neanche aperto)
	m_bFileOpen = FALSE;
	m_bAttached	= FALSE;
	m_hFile = NULL;
	m_hMap = NULL;
	m_pMapFile = NULL;
	m_dwLastError = 0;
}

//////////////////////////////////////////////////////////////////////
// Distruttore
//////////////////////////////////////////////////////////////////////
CMMFLib::~CMMFLib()
{
	// Se mappatura ancora attiva al momento della distruzione
	// dell' istanza allora ne forzo la chiusura
	if (IsMapped() == TRUE) Close();
}

//////////////////////////////////////////////////////////////////////
// Attiva la mappatura di un file in memoria
//
// Input:	fName		Path e nome del file su disco
//			  dwSize	Dimensione in byte del buffer
//			  bCreateAnyway	:
//           TRUE	  Il file viene aperto oppure creato ed inizializzato.
//					 FALSE	Il file viene aperto SOLO se gia'esistente.
//                  (viene comunque fatto il controllo sulla sua lunghezza)
//        bResize :
//            TRUE  Il file viene dimensionato come dwSize
//            FALSE dwSize impostata alla dimensione del file
// 
// Output:	void*		Puntatore al file mappato
//			    NULL		Errore (per il codice usare GetLastError)
//						      Il file non esiste (bCreateAnyway = FALSE)
//////////////////////////////////////////////////////////////////////
void* CMMFLib::Open(LPCTSTR fName, LPCTSTR lName, DWORD dwSize, BOOL bCreateAnyway, BOOL bResize)
{
	DWORD dwFileSize;

	// Gia' mappato ?
	if (IsMapped() == TRUE) return NULL;

	// preset file mappato
	m_bFileExists = TRUE;

	// Apre il file
	m_hFile = ::CreateFile(	
		fName,								          // File name
		GENERIC_READ|GENERIC_WRITE,			// Read-Write Access
		0,									            // No sharing
		NULL,								            // No security attributes
		(bCreateAnyway == TRUE) ? OPEN_ALWAYS : OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL | 
		FILE_FLAG_SEQUENTIAL_SCAN |
		FILE_FLAG_RANDOM_ACCESS,
		NULL);								

  // se errore termina
	if (m_hFile == INVALID_HANDLE_VALUE)
	{
		m_dwLastError = ::GetLastError();
		return NULL;
	}

	// Leggo la dimensione del file	
	dwFileSize = ::GetFileSize(m_hFile, NULL);
	// Se impossibile determinare la lunghezza del file termina
	if (dwFileSize == 0xffffffff)
	{
		m_dwLastError = ::GetLastError();
		Shutdown();
		return NULL;
	}
	
  // Se NON richiesta resize file
  if( !bResize)
    dwSize=dwFileSize;

	// se dimensione diversa da quella del buffer ridimensiono il file
	if (dwFileSize != dwSize)
	{
		// Se la InitializeFile fallisce allora esco
		if (InitializeFile(m_hFile, dwSize) == FALSE)
		{
			m_dwLastError = ::GetLastError();
			Shutdown();
			return NULL;
		}
		m_bFileExists = FALSE;
	}

	::SetLastError(0);

	// Create a mapping object from the file
	m_hMap = ::CreateFileMapping(
		m_hFile,				        // Handle we got from CreateFile
		NULL,					          // No security attributes
		PAGE_READWRITE,			    // read-write
		0, 0,					          // Max size = current size of file
		lName);					        // Mapping Object Name

	DWORD dwError;

	// Se la CreateFileMapping fallisce allora esco
	dwError = ::GetLastError();
	if (m_hMap == NULL || dwError == ERROR_ALREADY_EXISTS)
	{
		m_dwLastError = dwError;
		Shutdown();
		return NULL;
	}

  // mappa il file in memoria
	m_pMapFile = MapView(m_hMap);

	return m_pMapFile;
} // End of Open

//////////////////////////////////////////////////////////////////////
// Connette un file in memoria GIA' mappato
// Ritorna : ptr al file o NULL se errore
//////////////////////////////////////////////////////////////////////
void* CMMFLib::Attach(LPCTSTR lName)
{
  // Apertura del file
	m_hMap = ::OpenFileMapping(
				FILE_MAP_ALL_ACCESS,
				FALSE,
				lName);

	if (m_hMap == NULL)
	{
		m_dwLastError = ::GetLastError();
		Shutdown();
		return NULL;
	}

	m_pMapFile = MapView(m_hMap);

  if(m_pMapFile != NULL)
  	m_bAttached = TRUE;

	return m_pMapFile;
} // End of Attach


//////////////////////////////////////////////////////////////////////
// Disconnette un file in memoria GIA' mappato
//////////////////////////////////////////////////////////////////////
void CMMFLib::Detach()
{
  if(m_bAttached == FALSE)
    return;

  if(m_pMapFile != NULL)
  	::UnmapViewOfFile(m_pMapFile);

	if (m_hMap != NULL)
		::CloseHandle(m_hMap);

  m_bAttached = FALSE;
}

//////////////////////////////////////////////////////////////////////
// Inizializza il file con la dimensione voluta
//////////////////////////////////////////////////////////////////////
BOOL CMMFLib::InitializeFile(HANDLE hHandle, DWORD dwSize)
{
	if (::SetFilePointer(hHandle, dwSize, 0, FILE_BEGIN) != 0xFFFFFFFF)
		return ::SetEndOfFile(hHandle);

	return FALSE;
} // End of InitializeFile

//////////////////////////////////////////////////////////////////////
// Forza la scrittura dei dati eventualmente ancora presenti
// in cache
//
// Input:
//
// Output:	TRUE		Tutto ok
//			FALSE		Errore
//						oppure mappatura non attiva
//////////////////////////////////////////////////////////////////////
BOOL CMMFLib::Flush()
{
	BOOL bRet;

	// Se mappatura non attiva allora esco
	if (IsMapped() == FALSE) return FALSE;

	// Forzo scrittura
	bRet= ::FlushViewOfFile(m_pMapFile, 0);
	//bRet= ::FlushFileBuffers(m_hFile);

	return TRUE;
} // End of Flush

//////////////////////////////////////////////////////////////////////
// Esegue la "de-mappatura" del file in memoria
// Prima della chiusura esegue una Flush() di sicurezza
//
// Input:
//
// Output:	TRUE		Tutto ok, file chiuso
//			FALSE		Errore durante la Flush()
//						oppure il file non era aperto
//////////////////////////////////////////////////////////////////////
BOOL CMMFLib::Close()
{
	// Se mappatura non attiva allora esco
	if (IsMapped() == FALSE) return FALSE;

	// Se fallisce la Flush allora esco
	if (Flush() == FALSE) return FALSE;

	// Termino mappatura
	::UnmapViewOfFile(m_pMapFile);
	Shutdown();

	return TRUE;
} // End of Close


//////////////////////////////////////////////////////////////////////
// Esegue la "de-mappatura" del file in memoria e 
// dimensiona il file come dwSize
// Prima della chiusura esegue una Flush() di sicurezza
//
// Input:
//
// Output:	TRUE		Tutto ok, file chiuso
//			FALSE		Errore durante la Flush()
//						oppure il file non era aperto
//////////////////////////////////////////////////////////////////////
BOOL CMMFLib::Close(DWORD dwSize)
{
BOOL ret=FALSE;

	// Se mappatura non attiva allora esco
	if (IsMapped() == FALSE) return FALSE;

	// Se fallisce la Flush allora esco
	if (Flush() == FALSE) return FALSE;

	// Termino mappatura
	::UnmapViewOfFile(m_pMapFile);

	if (m_hMap != NULL)
		::CloseHandle(m_hMap);

	if (::SetFilePointer(m_hFile, dwSize, 0, FILE_BEGIN) != 0xFFFFFFFF)
		ret=::SetEndOfFile(m_hFile);

	if (m_hFile != NULL)
		::CloseHandle(m_hFile);

	// Impostazione valori di default
	m_bFileOpen = FALSE;
	m_hFile = NULL;
	m_hMap = NULL;
	m_pMapFile = NULL;

  return ret;
}


//////////////////////////////////////////////////////////////////////
// In caso di anomalia durante la fase di inizializzazione
// del file mappato in memoria (tramite Open) viene chiamata 
// per chiudere gli eventuali HANDLE aperti e per impostarli
// a valori di default (NULL)
//
// Input:
//
// Output:
//////////////////////////////////////////////////////////////////////
void CMMFLib::Shutdown()
{
	// Le seguenti due CloseHandle DEVONO essere eseguite
	// in quest' ordine: 
	// 1- m_hMap
	// 2- m_hFile
	// Questo perche' Shutdown viene chiamata anche da Close()
	// che esegue la "de-mappatura" del file e gli handle
	// devono essere chiusi in ordine inverso a come sono
	// stati aperti
	if (m_hMap != NULL)
		::CloseHandle(m_hMap);

	if (m_hFile != NULL)
		::CloseHandle(m_hFile);

	// Impostazione valori di default
	m_bFileOpen = FALSE;
	m_hFile = NULL;
	m_hMap = NULL;
	m_pMapFile = NULL;
} // End of Shutdown

//////////////////////////////////////////////////////////////////////
// Restituisce lo stato della mappatura
//
// Input:
//
// Output:	TRUE		Mappatura attiva
//			FALSE		Mappatura non attiva
//////////////////////////////////////////////////////////////////////
BOOL CMMFLib::IsMapped()
{
	return ( m_bFileOpen == TRUE && m_hFile != NULL && m_hMap != NULL && m_pMapFile != NULL);
} // End of IsMapped

//////////////////////////////////////////////////////////////////////
// Indica se al momento della Open(...) il file richiesto
// era gia' presente su disco oppure e' stato creato
//
// Input:
// 
// Output:	TRUE		Il file esisteva gia'
//			FALSE		Il file non esisteva oppure
//						era di dimensioni minori a quelle
//						della struttura da mappare
//						ATTENZIONE: la funzione restituisce 
//						FALSE anche quando viene chiamata prima
//						di attivare la mappatura (con Open)
//////////////////////////////////////////////////////////////////////
BOOL CMMFLib::FileExists()
{
	// Se mappatura non attiva allora esco
	if (IsMapped() == FALSE) return FALSE;

	return m_bFileExists;
} // End of FileExists

//////////////////////////////////////////////////////////////////////
// Restituisce l' ultimo errore identificato e
// memorizzato dalla classe
//
// Input:
//
// Output:	Ultimo errore
//////////////////////////////////////////////////////////////////////
DWORD CMMFLib::GetLastError()
{
	return m_dwLastError;
} // End of GetLastError

//////////////////////////////////////////////////////////////////////
// Restituisce la versione della classe.
// Sapere la versione puo' essere utile quando si usa
// la classe in progetti differenti
//
// Dividere per 10 per ottenere il numero reale di versione
//
// Input:
//
// Output:	Versione in formato numerico
//////////////////////////////////////////////////////////////////////
const short CMMFLib::GetVersionI()
{
	return 13;
} // End of GetVersionI

//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////
const char* CMMFLib::GetVersionC()
{
  return "1.3";
} // End of GetVersionC


//////////////////////////////////////////////////////////////////////
// Mappa il file in memoria
// Ritorna : ptr al file o NULL se errore
//////////////////////////////////////////////////////////////////////
void* CMMFLib::MapView(HANDLE hFile)
{
	void* pMapView;

	// Mappa il file in memoria
	pMapView = ::MapViewOfFile(
				hFile,				    // Handle we got from CreateFileMapping
				FILE_MAP_WRITE,		// read-write access
				0, 0,				      // Offset into file = beginning of file
				0);					      // Map entire file

	// Se la MapViewOfFile fallisce annullo 
	if (pMapView == NULL)
	{
		m_dwLastError = ::GetLastError();
		Shutdown();
	}
	else
  	m_bFileOpen = TRUE;

	return pMapView;
} // End of MapView

//////////////////////////////////////////////////////////////////////
// Effettua il resize del file
// Ritorna : ptr al file o NULL se errore
//////////////////////////////////////////////////////////////////////
void* CMMFLib::Resize(DWORD dwSize, LPCTSTR lName)
{

	// Se mappatura non attiva allora esco
	if (IsMapped() == FALSE) return NULL;

	// Se fallisce la Flush allora esco
	if (Flush() == FALSE) return NULL;

	// Termino mappatura
	::UnmapViewOfFile(m_pMapFile);

	if (m_hMap != NULL)
		::CloseHandle(m_hMap);

  // resize del file
	if (::SetFilePointer(m_hFile, dwSize, 0, FILE_BEGIN) == 0xFFFFFFFF)
    return NULL;

	if(::SetEndOfFile(m_hFile) == FALSE )
    return NULL;


	// Create a mapping object from the file
	m_hMap = ::CreateFileMapping(
		m_hFile,				        // Handle we got from CreateFile
		NULL,					          // No security attributes
		PAGE_READWRITE,			    // read-write
		0, 0,					          // Max size = current size of file
		lName);					        // Mapping Object Name

	DWORD dwError;

	// Se la CreateFileMapping fallisce allora esco
	dwError = ::GetLastError();
	if (m_hMap == NULL || dwError == ERROR_ALREADY_EXISTS)
	{
		m_dwLastError = dwError;
		Shutdown();
		return NULL;
	}

  // mappa il file in memoria
	m_pMapFile = MapView(m_hMap);

  return m_pMapFile;
}

//////////////////////////////////////////////////////////////////////
// Restituisce la dimensione del file
// Ritorna : 0 se errore
//////////////////////////////////////////////////////////////////////
DWORD CMMFLib::GetFileSize()
{
  // se errore termina
	if (m_hFile == NULL
  ||  m_hFile == INVALID_HANDLE_VALUE)
	{
		return 0;
	}

	// Leggo la dimensione del file	
	DWORD dwFileSize = ::GetFileSize(m_hFile, NULL);
	return (dwFileSize != 0xffffffff) ? dwFileSize : 0;
}
