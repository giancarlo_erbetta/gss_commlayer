//@////////////////////////////////////////////////////////////
//@
//@	Class:		CNetLink
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@
//@	Created:	19 February 2001
//@
//@	Author:		Giuseppe Alessio
//@
//@	Updated:
//@ 28 January 2005
//@ Support to BaseLib
//@
//@ March 24, 2005
//@ - Changes for external queue management 
//@
//@ August 4, 2005
//@ - Added lamp queue management
//@
//@////////////////////////////////////////////////////////////

#ifndef _NETLINK_H
#define _NETLINK_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ThreadLib.h"
#include "NetLinkImpl.h"

class CNetLink : 
	public CThreadLib, 
	public CNetLinkImpl
{
public:

	CNetLink(STRUCT_NETLINK_INIT& rcsInit);
	virtual ~CNetLink();
	inline void GetLDI(STRUCT_NETLINK_DEBUG_INFO* pLDI);
	inline HRESULT GetLDI(VARIANT* pvaDest);
  inline void ResetLDI();
	inline int GetLinkStatus();

private:
	static DWORD WINAPI EngineThread(LPVOID pParam);
  int m_linkStatus;
};

//@ Return the Link Debug Information
inline HRESULT CNetLink::GetLDI(VARIANT* pvaDest)
{
	wcsncpy_s(m_LDI.wszObjectName, MAX_PATH, CNetLinkImpl::m_wszObjectName, MAX_PATH);
  m_LDI.nStatus = m_linkStatus;
	wcsncpy_s(m_LDI.wszIPAddr, MAX_PATH, CNetLinkImpl::m_wszIPAddr, MAX_PATH);
	m_LDI.nPortID= m_nCurrentPortID;
	m_LDI.nLocalNodeID=m_bySourceNode;
	m_LDI.nRemoteNodeID=m_byDestNode;
	m_LDI.nQueueLen = m_queueSend.GetQueuelen();
	m_LDI.nQueueSize = m_queueSend.GetSizeQueue();
	m_LDI.nQueuePeak = 	m_queueSend.GetMaxQueuelen();
	m_LDI.nQueueLampLen = m_queueLamp.GetQueuelen();
	m_LDI.nQueueLampSize = m_queueLamp.GetSizeQueue();
	m_LDI.nQueueLampPeak = 	m_queueLamp.GetMaxQueuelen();
	//
	HRESULT hr=VariantMemCopy(pvaDest, (BYTE*)&m_LDI, sizeof(STRUCT_NETLINK_DEBUG_INFO));
	//
	return hr;
}

//@ Return the Link Debug Information
inline void CNetLink::GetLDI(STRUCT_NETLINK_DEBUG_INFO* pLDI)
{
	wcsncpy_s(m_LDI.wszObjectName, MAX_PATH, CNetLinkImpl::m_wszObjectName, MAX_PATH);
  m_LDI.nStatus = m_linkStatus;
	wcsncpy_s(m_LDI.wszIPAddr, MAX_PATH, CNetLinkImpl::m_wszIPAddr, MAX_PATH);
	m_LDI.nPortID= m_nCurrentPortID;
	m_LDI.nLocalNodeID=m_bySourceNode;
	m_LDI.nRemoteNodeID=m_byDestNode;
	m_LDI.nQueueLen = m_queueSend.GetQueuelen();
	m_LDI.nQueueSize = m_queueSend.GetSizeQueue();
	m_LDI.nQueuePeak = 	m_queueSend.GetMaxQueuelen();
	m_LDI.nQueueLampLen = m_queueLamp.GetQueuelen();
	m_LDI.nQueueLampSize = m_queueLamp.GetSizeQueue();
	m_LDI.nQueueLampPeak = 	m_queueLamp.GetMaxQueuelen();

	//
	*pLDI = m_LDI;
}

//@ Clears the Link Debug Informations
inline void CNetLink::ResetLDI()
{
  ::ZeroMemory(&m_LDI, sizeof(STRUCT_NETLINK_DEBUG_INFO));
	m_queueSend.SetMaxQueuelen(0);
	m_queueLamp.SetMaxQueuelen(0);
}

//@ Return the Link Status
inline int CNetLink::GetLinkStatus()
{
	return m_linkStatus;
}

extern CNetLink* G_pNetLinks[];

#endif 
