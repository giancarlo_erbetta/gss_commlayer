//@////////////////////////////////////////////////////////////
//@
//@	Class:		CQueue
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@
//@	Created:	06 February 2001
//@
//@	Author:		Giuseppe Alessio
//@
//@	Updated:	
//@ 17 May 2002
//@ Persistent queues
//@
//@	25 October 2002
//@ Bug fixing : set m_hEvRead at startup if there are pending messages
//@
//@ 28 January 2005
//@ Support to BaseLib
//@
//@ 13 July 2012
//@ Added Attach to PersistentQueue
//@
//@////////////////////////////////////////////////////////////

#ifndef _QUEUELIB_H
#define _QUEUELIB_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "mmflib.h"

class CQueue  
{
public:
	// traps default constructor
	CQueue();
	// Constructor
	CQueue(long _lRecordSize, long _lQueueSize, wchar_t* _pwszName);
	// Destructor
	virtual ~CQueue();

	// put an element in queue
	int PutItem(BYTE* _pbyRecord);

	// get an element from queue
	int GetItem(BYTE* _pbyRecord);

	// copy an element from queue
	// without cancel it
	int QueryNextItem(BYTE* _pbyRecord);

	// flush teh queue
	void FlushQueue();
	// returns instance state
	inline BOOL GetStatus() { return m_bStatus; }
	// returns the size of the queue
	inline long GetSizeQueue() { return m_lSizeQueue; }
	// returns the number of items in queue
	inline long GetQueuelen() { return m_lItemInQueue; }
	// returns the peak of the queue
	inline long GetMaxQueuelen() { return m_lMaxItemInQueue; }
	// sets the peak of the queue
	inline void SetMaxQueuelen(long lVal ) { m_lMaxItemInQueue=lVal; }
	// returns the queue name
	inline wchar_t* GetIdent() { return m_wszName; }
	// checks if the queue is full
	inline BOOL IsQueueFull() { return m_bMemFull; }
 	// returns the handle of 'item in queue' event
	inline HANDLE GetEvReadHandle() { return m_hEvRead;}

private:
	const long m_lSizeRecord;           // record size
	const long m_lSizeQueue;            // queue size
	long       m_lSizeByte;             // byte amount to be allocated
	BOOL       m_bStatus;               // queue state
	long       m_lItemInQueue;          // number of elements in queue
	long       m_lMaxItemInQueue;       // queue pick
	BOOL       m_bMemFull;              // full queue memory

	BYTE * m_pbyRecordPut;              // incoming pointer
	BYTE * m_pbyRecordGet;              // outgoing pointer
	BYTE * m_pbyTopQueue;               // top queue pointer
	BYTE * m_pbyBottomQueue;            // bottom queue pointer
	wchar_t m_wszName[MAX_PATH+1];      // queue name

	CRITICAL_SECTION m_cs_syncro;       // CS object
  HANDLE m_hEvRead;                   // EV object

};

class CPersistentQueue
{
public:
struct tagQueueInfo
{
	DWORD dwSignature;
  long  lWrIndex;              // Writing pointer
  long  lRdIndex;              // Reading pointer
	long  lItemInQueue;          // number of elements in queue
	long  lMaxItemInQueue;       // queue pick
	BOOL  bMemFull;              // full queue memory
};

public:
	// traps default constructor
	CPersistentQueue();
	// Constructor
	CPersistentQueue(long _lRecordSize, long _lQueueSize, wchar_t* _pwszName, wchar_t* _pwszFileName);
	// Destructor
	virtual ~CPersistentQueue();

	// put an element in queue
	int PutItem(BYTE* _pbyRecord);

	// get an element from queue
	int GetItem(BYTE* _pbyRecord);

	// copy an element from queue
	// without cancel it
	int QueryNextItem(BYTE* _pbyRecord);

	// flush teh queue
	void FlushQueue();
	// returns instance state
	inline BOOL GetStatus() { return m_bStatus; }
	// returns the size of the queue
	inline long GetSizeQueue() { return m_lSizeQueue; }
	// returns the number of items in queue
	inline long GetQueuelen() { return m_pcsQInfo->lItemInQueue; }
	// returns the pick of the queue
	inline long GetMaxQueuelen() { return m_pcsQInfo->lMaxItemInQueue; }
	// sets the pick of the queue
	inline void SetMaxQueuelen(long lVal ) { if(m_pcsQInfo != NULL) m_pcsQInfo->lMaxItemInQueue=lVal; }
	// returns the queue name
	inline wchar_t* GetIdent() { return m_wszName; }
	// checks if the queue is full
	inline BOOL IsQueueFull() { return m_pcsQInfo->bMemFull; }
 	// returns the handle of 'item in queue' event
	inline HANDLE GetEvReadHandle() { return m_hEvRead;}

private:
	long m_lSizeRecord;                 // record size
	long m_lSizeQueue;                  // queue size
	long       m_lSizeByte;             // byte amount to be allocated
	BOOL       m_bStatus;               // queue state

	BYTE * m_pbyRecordPut;              // incoming pointer
	BYTE * m_pbyRecordGet;              // outgoing pointer
	BYTE * m_pbyTopQueue;               // top queue pointer
	BYTE * m_pbyBottomQueue;            // bottom queue pointer
  //
  BYTE*  m_pbyData;
  tagQueueInfo* m_pcsQInfo;  
  //
	wchar_t m_wszName[MAX_PATH+1];      // queue name
	wchar_t m_wszFileName[MAX_PATH+1];  // queue file name
  //
	CRITICAL_SECTION m_cs_syncro;       // CS object
  HANDLE m_hEvRead;                   // EV object
  //
  CMMFLib m_mmf;
};

#endif
