#include "StdAfx.h"
#include "ProtocolConverter_FX.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif


void CProtocolConverter_FX::ConvertGssToMsgI(char *sName, BYTE *byBufferSrc, BYTE *byBufferDst, int nSrcLen)
{
	int i = GetGSSMsgIndex(sName);

	if (i != -1)
	{
		this->ConvertToMsgI(i, byBufferSrc, byBufferDst, nSrcLen);
	}
}

int CProtocolConverter_FX::GetGSSMsgIndex(char *sName)
{
	int i;
	for (i = 0; i < MAX_MESSAGES; i++)
	{
		if (!memcmp(this->strMsgFormat[i].szName, sName, 2))
		{
			return(i);
			break;
		}
	}

	return (-1);

}

