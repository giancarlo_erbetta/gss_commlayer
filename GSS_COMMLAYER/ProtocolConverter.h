
#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ProtocolConverterDef.h"
#include "GSS_ProtocolDef.h"


class CProtocolConverter
{
public:
	CProtocolConverter(void);
	~CProtocolConverter(void);
	BOOL InitProtocolConverter(LPTSTR p_sAPPL_FILES_PATH);
	int ConvertToMsgE(int nMsgId, BYTE *byBufferSrc, BYTE *byBufferDst);
	int ConvertToMsgI(int nMsgId, BYTE *byBufferSrc, BYTE *byBufferDst, int nSrcLen);

public:
	STRUCT_MSG_FORMAT strMsgFormat[MAX_MESSAGES];

private:
	int GetTypeId(CString sType, wchar_t *wszMsgFileName);
	int GetTypeLen(CString sType);
	void SetValue(BYTE *byBuffer, int nType, int nOffset, int nLen, CString sValue);
	void SetValue(BYTE *byBuffer, int nType, int nOffset, int nLen, BYTE *byValue, int nValueLen);
	void SetValue(BYTE *byBuffer, int nType, int nOffset, int nLen, DWORD dwValue);
	void ConvertField(BYTE *byBufferSrc, int nTypeSrc, int nOffsetSrc, int nLenSrc, BYTE *byBufferDst, int nTypeDst, int nOffsetDst, int nLenDst);

};

extern CProtocolConverter* G_pProtocolConverter;
