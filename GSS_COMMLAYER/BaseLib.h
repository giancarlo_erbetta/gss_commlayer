//@////////////////////////////////////////////////////////////
//@
//@	Class: <collection>
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@
//@	Created:	28 Genuary 2005
//@
//@	Author:		Giuseppe Alessio
//@
//@	Updated:	
//@ February 15, 2005 
//@ Added functions:
//@  - FindOneOfTokens
//@  - GetStreamLine
//@
//@ March 29, 2005 
//@  - added CFileStream
//@
//@ September 9, 2005 
//@  - added GetAppInfo
//@
//@////////////////////////////////////////////////////////////

#ifndef _BASELIB_H
#define _BASELIB_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <comdef.h>

#include "list.h"

//
// RESERVED FOR LIBRARY ALARMS
// 
#define ERR_FULL_QUEUE                 0
#define ERR_UPDATING_TRACE_FILE        1
#define ERR_RENAMING_TRACE_FILE        2
#define ERR_EXCEPTION_ON_THR_TRACE     3
#define ERR_EXCEPTION_ON_THR_NETLINK   4
#define ERR_TRACE_STRING_TOO_LONG      5
#define ERR_EXCEPTION_TRACELIB         6


#define MAX_GUI_INSTANCES 5

#define SIZE_BITMAP_ALARMS  4

struct STRUCT_GUI_INFO_RECORD
{
	BYTE    byUsed;
	wchar_t wszComputerName[MAX_PATH+1];
	wchar_t wszComputerAddress[MAX_PATH+1];
	wchar_t wszModuleName[MAX_PATH+1];
	wchar_t wszLastBuilding[MAX_PATH+1];
	wchar_t wszStartTime[MAX_PATH+1];
	int nGUIID;
	//
	STRUCT_GUI_INFO_RECORD() { ::ZeroMemory(this, sizeof(STRUCT_GUI_INFO_RECORD)); }
};

struct STRUCT_GUI_INFO
{
	BOOL m_bObjectLocked;
	STRUCT_GUI_INFO_RECORD csServer;
	STRUCT_GUI_INFO_RECORD csGUIs[MAX_GUI_INSTANCES];
};

#define VALIDPTR(X) if(X != NULL) X

enum _LinkStatus
{
	LINK_STOPPED=0,
	LINK_WAITING,
 	LINK_RUNNING,
 	LINK_ERROR,
};

///////////////////////////////////////////////////////////////////////////////////////
// Print binary buffer
//
// Usage:
//   char szBuf[512];
//	 PrintBinaryBuffer(szBuf, m_wsaTxData.buf, m_wsaTxData.len, sizeof(szBuf));
//   VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_1_HOST,__FILE__,__LINE__,0, "%s TX %s", m_szObjectName, szBuf);
// 
inline void PrintBinaryBuffer(char* pszDest, char* pszSrc, int nLen, int nMaxLen)
{
	for(int i=0, b=0; i<nLen && b<(nMaxLen-3); i++, b+=2)
	{
		sprintf_s(&pszDest[b], 3, "%02X", (BYTE)pszSrc[i]);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Print timestamp
//
// Usage:
//   wchar_t wszTimestamp[MAX_PATH+1];
//   PrintTimestamp(wszTimestamp, sizeof(wszTimestamp));

inline void PrintTimestamp(wchar_t* pwszDest, int nMaxLen)
{
	CTime curTime = CTime::GetCurrentTime();
	_snwprintf_s(pwszDest, nMaxLen, nMaxLen, L"%02d-%02d-%04d %02d:%02d:%02d",
						 curTime.GetDay(),
						 curTime.GetMonth(),
						 curTime.GetYear(),
						 curTime.GetHour(),
						 curTime.GetMinute(),
						 curTime.GetSecond());
}

///////////////////////////////////////////////////////////////////////////////////////
// Automatic Pointer
//
// Usage:
// 
// CAutoPtr<BYTE> pStream(new BYTE[100]);
// CAutoPtr<STRUCT_TEST> pCS(new STRUCT_TEST);
// CAutoPtr<STRUCT_TEST> pCS(new STRUCT_TEST[2]);
// 
#pragma warning(disable : 4284)
template <class T>
class CAutoPtr
{
public:
	CAutoPtr(T* ptr=NULL) : p(ptr) {}
	~CAutoPtr() { if(p != NULL) delete p; }
	void Delete() { if(p != NULL) { delete p; p=NULL; }}
	//
	inline      operator T*() const { return p; }
	inline T*   operator->() const { return p; }
	inline T&   operator*() const { return *p; }
	inline T**  operator&() const { return &p; }
	inline bool operator!() const { return (p == NULL); }
	inline bool operator<(T* pT) const { return p < pT; }
	inline bool operator==(T* pT) const { return p == pT; }

private:
	T* p;
};
#pragma warning(default : 4284)


///////////////////////////////////////////////////////////////////////////////////////
// FOLibrary Exception
//
class CFOLibException
{
public:
	int m_nErr;
	wchar_t m_wszDescription[MAX_PATH+1];
	char    m_szDescription[MAX_PATH+1];

public:
	CFOLibException(int nErr, wchar_t* pwszFormat, ...) 
	{ 
		m_nErr=nErr;  
		//
		va_list pArg;
		va_start(pArg, pwszFormat);
		vswprintf_s(m_wszDescription, MAX_PATH, pwszFormat, pArg);
		::WideCharToMultiByte(CP_ACP, NULL, m_wszDescription, -1, m_szDescription, MAX_PATH, NULL, NULL); 
		va_end(pArg);
	}

	
	char szComputerName[MAX_PATH+1];


	void Delete() { delete this; }
};


///////////////////////////////////////////////////////////////////////////////////////
// FOLibrary base class
//
class CBaseLib  
{
public:
	CBaseLib();
	virtual ~CBaseLib();

public:
	static STRUCT_GUI_INFO m_csGUIInfo;

	//////////////////////////////////////////////////////////////////////////////////////
	// Logging functions
	static void LogEvent(WORD wType, wchar_t* pwszFormat, ...)
	{
		wchar_t* lpwszStrings[1];
		wchar_t wszMsg[MAX_PATH+1];
		HANDLE hEventSource;

		va_list pArg;
		//
		va_start(pArg, pwszFormat);
		_vsntprintf_s(wszMsg, MAX_PATH, MAX_PATH, pwszFormat, pArg);
		wszMsg[MAX_PATH] = L'\0';
		va_end(pArg);
		//
		lpwszStrings[0] = wszMsg;
		// Get a handle to use with ReportEvent()
		hEventSource = RegisterEventSource(NULL, L"FOLIBRARY");

		if (hEventSource != NULL)
		{
				// Write to event log
				ReportEvent(hEventSource, 
										wType, 
										CAT_CRITICAL, 
										APP_MESSAGE, 
										NULL, 
										1, 
										0, 
										(const wchar_t**) &lpwszStrings[0], 
										NULL);
      
				DeregisterEventSource(hEventSource);
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////
	// Return infos about network and excutable (see STRUCT_GUI_INFO_RECORD)
	static BOOL GetAppInfo(wchar_t* pwszModuleName, STRUCT_GUI_INFO_RECORD& csSTRUCT_GUI_INFO_RECORD)
	{
		BOOL bRet=FALSE;
		char szHostName[MAX_PATH+1];
		char szHostAddress[MAX_PATH+1];
		struct hostent* pHost;
		//		
		::ZeroMemory(szHostName, sizeof(szHostName));
		::ZeroMemory(szHostAddress, sizeof(szHostAddress));
		//
		if (gethostname(szHostName, 127) == 0)
		{
			// Get host adresses
			pHost = gethostbyname(szHostName);

			for( int nListID = 0; pHost!= NULL && pHost->h_addr_list[nListID]!= NULL; nListID++ )
			{
				
				for(int nByteID = 0; nByteID < pHost->h_length; nByteID++)
				{
					if( nByteID > 0 ) sprintf_s(&szHostAddress[strlen(szHostAddress)], 2, "%c", '.');
					
					sprintf_s(&szHostAddress[strlen(szHostAddress)], 2,"%u", (unsigned int)((BYTE*)pHost->h_addr_list[nListID])[nByteID]);
				}
				// szHostAddress now contains one local IP address
				//
				::MultiByteToWideChar(CP_ACP, MB_ERR_INVALID_CHARS, szHostName, -1, csSTRUCT_GUI_INFO_RECORD.wszComputerName, MAX_PATH);
				::MultiByteToWideChar(CP_ACP, MB_ERR_INVALID_CHARS, szHostAddress, -1, csSTRUCT_GUI_INFO_RECORD.wszComputerAddress, MAX_PATH);
			
				// STOP at first address
				break;
			}
		}

		// Get module name
		wcsncpy_s( csSTRUCT_GUI_INFO_RECORD.wszModuleName, MAX_PATH, pwszModuleName, MAX_PATH);
		csSTRUCT_GUI_INFO_RECORD.wszModuleName[MAX_PATH]=L'\0';

		// cancel string delimiters " "			
		wchar_t wszModuleName[MAX_PATH+1];
		wcsncpy_s( wszModuleName, MAX_PATH, &csSTRUCT_GUI_INFO_RECORD.wszModuleName[1], MAX_PATH);
		wszModuleName[min(wcslen(wszModuleName)-1, MAX_PATH)]=L'\0';

		// Get last writing time
		CFileFind finder;
		if (finder.FindFile(wszModuleName))
		{
			finder.FindNextFile();
			CTime csLastWriteTime;

			if (finder.GetLastWriteTime(csLastWriteTime))
			{
				CString str;
				str += csLastWriteTime.Format(L"%c");
				wcsncpy_s(csSTRUCT_GUI_INFO_RECORD.wszLastBuilding, MAX_PATH, (LPWSTR)(LPCWSTR)str, MAX_PATH);
			}
		}
		if(wcslen(csSTRUCT_GUI_INFO_RECORD.wszLastBuilding) == 0)
		{
			wcscpy_s(csSTRUCT_GUI_INFO_RECORD.wszLastBuilding, MAX_PATH, L"Unknown");
		}
		csSTRUCT_GUI_INFO_RECORD.wszLastBuilding[MAX_PATH]=L'\0';

		// Get starting time
		CTime curTime = CTime::GetCurrentTime();
		swprintf_s(csSTRUCT_GUI_INFO_RECORD.wszStartTime, L"%d/%d/%d %02d:%02d:%02d",
						 curTime.GetMonth(), curTime.GetDay(), curTime.GetYear(),
						 curTime.GetHour(), curTime.GetMinute(), curTime.GetSecond());

		bRet = TRUE;

		
		return bRet;
	}

	//////////////////////////////////////////////////////////////////////////////////////
	// APPLICATION ALARM BITMAP MANAGEMENT
	//////////////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////////////////
	// Set/Reset bit
	static void SetBit( int nBit, BOOL bSet)
	{
		BYTE byteId=0;
		BYTE bitId=0;
		BYTE mask=0;

		if(nBit < 0 || nBit > (SIZE_BITMAP_ALARMS*8))
			return;

		byteId = nBit/8;
		bitId  = nBit%8;
  
		mask=1;
		for(int i=0; i<bitId; i++)
			mask *= 2;

		if( bSet == TRUE)
			m_byBitmap[byteId] |= mask;
		else
			m_byBitmap[byteId] &= ~mask;
	}
	//////////////////////////////////////////////////////////////////////////////////////
	// Set/Reset bit
	static void SetBitmap(BOOL bSet)
	{
		if(bSet)
			::memset(m_byBitmap, 0xFF,SIZE_BITMAP_ALARMS);
		else
			::ZeroMemory(m_byBitmap, SIZE_BITMAP_ALARMS);
	}
	//////////////////////////////////////////////////////////////////////////////////////
	// Return Bitmap
	static const BYTE* Bitmap() { return (const BYTE*)&m_byBitmap; }


private:
	static BYTE m_byBitmap[SIZE_BITMAP_ALARMS];

};

///////////////////////////////////////////////////////////////////////////////////////
// FOLibrary CFileStream Classes
//
class CFileStream : public CFile
{
public:
	CFileStream(wchar_t* wszFileName, UINT nOpenFlags)
	{
		if( !Open(wszFileName, nOpenFlags))
			throw CFOLibException(0, L"Unable to open file[%s]", wszFileName);
	}
	//
	//
	virtual ~CFileStream()
	{
		if( m_hFile != NULL)
			Close();
	}
};

/* USAGE SAMPLE
	
	CFileStreamWriter w(APPL_FILES_PATH L"TEST.TXT", TRUE);
	w.WriteLine("linea uno");
	w.WriteLine("linea due");

*/
class CFileStreamWriter : public CFileStream
{
public:
	//
	CFileStreamWriter(wchar_t* wszFileName, BOOL bAppend, UINT nOpenFlags = CFile::modeCreate | CFile::modeWrite | CFile::modeNoTruncate) :
	CFileStream(wszFileName, nOpenFlags)
	{
		if(bAppend)
			SeekToEnd();
	}

	//////////////////////////////////////////////////////////////////////////////////////
	// Write a line to the stream
	void WriteLine(char* pszLine)
	{
		try
		{
			Write(pszLine, strlen(pszLine));
			Write("\r\n", 2);
		}
		catch(CFileException* e)
		{
			e->Delete();
			throw CFOLibException(0, L"CFileException on Write");
		}
	}
};
/* USAGE SAMPLE

	CFileStreamReader f(APPL_FILES_PATH L"DDE_TABLECONFIG.CSV");
	char szLine[MAX_PATH+1];
	while(f.ReadLine(szLine, MAX_PATH))
	{
		...
	}

*/

class CFileStreamReader : public CFileStream
{
public:
	//////////////////////////////////////////////////////////////////////////////////////
	// Find one of the given tokens in a string
	static BOOL FindOneOfTokens(char* pszString, char* pszTokens, int nNumTokens)
	{
		int nIdxString=0;
		int nIdxToken=0;
		int nStringLength = strlen(pszString);

		// check also the string terminator as valid character
		while (nStringLength >= nIdxString)
		{
			nIdxToken=0;
			while( nIdxToken < nNumTokens)
			{
				if( pszString[nIdxString] == pszTokens[nIdxToken])
				{
					pszString[nIdxString]=NULL;
					return TRUE;
				}
				nIdxToken++;
			}
			nIdxString++;
		}

		return FALSE;
	}

public:
	//
	CFileStreamReader(wchar_t* wszFileName, UINT nOpenFlags = CFile::modeRead) :
	CFileStream(wszFileName, nOpenFlags)
	{
		m_pszStream=NULL;
		m_pszEnd=NULL;
		m_pszCurPos=NULL;
		
	}
	//
	virtual ~CFileStreamReader()
	{
		if( m_pszStream != NULL)
			delete m_pszStream;
	}
	//
	//////////////////////////////////////////////////////////////////////////////////////
	// Restart to beginning
	void Restart()
	{
		if( m_pszStream == NULL)
			throw CFOLibException(0, L"CFileStreamReader::Restart Object not initialized");

		m_pszEnd= m_pszStream+m_nFileLen;
		m_pszCurPos=m_pszStream;
	}

	//
	//////////////////////////////////////////////////////////////////////////////////////
	// Read a line from the memory stream
	BOOL ReadLine(char* pszLine, int nMaxLineLength)
	{
		if( m_pszStream == NULL)
			_OpenStream();

		if( m_pszStream == NULL || m_pszEnd == NULL || m_pszCurPos == NULL || pszLine == NULL
		||  nMaxLineLength <= 0
		||  m_pszCurPos >= m_pszEnd)
			return FALSE;
		
		nMaxLineLength--;
		int nIndex=0;
		while( m_pszEnd > m_pszCurPos 
		&&     m_pszCurPos[0] != '\r' 
		&&     m_pszCurPos[0] != '\n'
		&&     nIndex < nMaxLineLength)
		{
			pszLine[nIndex] = m_pszCurPos[0];
			nIndex++;
			m_pszCurPos++;
		}
		
		// close line
		pszLine[nIndex]=0;

		// move to next line
		while( m_pszCurPos[0] == '\r' || m_pszCurPos[0] == '\n')
		{
			m_pszCurPos++;

		  // 21/07/2010 BUG FIX
		  if(m_pszCurPos >= m_pszEnd)
		    break;
	  }

		return TRUE;
	}

private:
	//
	void _OpenStream()
	{
		if( m_hFile == NULL)
			throw CFOLibException(0, L"Invalid File Handle");

		try
		{
			m_nFileLen= (int)GetLength();
			if( m_nFileLen == 0)
			{
				throw CFOLibException(0, L"File is emtpy");
			}

			//
			m_pszStream = new char[m_nFileLen];
			m_pszEnd= m_pszStream+m_nFileLen;
			m_pszCurPos=m_pszStream;

			::ZeroMemory(m_pszStream, m_nFileLen);

			// read whole file
			Read(m_pszStream, m_nFileLen);
			Close();
			m_hFile = NULL;
		}
		catch(CMemoryException* e)
		{
			e->Delete();
			throw CFOLibException(0, L"CMemoryException");
		}
		catch(CFileException* e)
		{
			e->Delete();
			throw CFOLibException(0, L"CFileException on Read");
		}
	}

private:

	char* m_pszStream;
	char* m_pszCurPos;
	char* m_pszEnd;
	int m_nFileLen;
};


////////////////////////
#endif
