
#include "stdafx.h"
#include "BaseLib.h"
#include "TraceLib.h"
#include "conversionsLib.h"

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CTraceLib::CTraceLib
//@
//@ Parameters:
//@		[IN]	     _pszPathName 
//@		[IN]	     _pszLogName 
//@		[IN]       _pwszObjectName  : object name
//@		[IN]       _closingtimeout : timeout in ms for closing the thread
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CTraceLib::CTraceLib(wchar_t* _pwszPathName, wchar_t* _pwszLogName, wchar_t* pwszTracePath, int nQueueSize, wchar_t* _pwszObjectName, int _nClosingtimeout)
: CThreadLib(_pwszObjectName, _nClosingtimeout),
	m_qTraceA(sizeof(CTraceLib::STRUCT_TRACE_RECORDA), nQueueSize, L"QTRACEA"),
	m_qTraceU(sizeof(CTraceLib::STRUCT_TRACE_RECORDU), nQueueSize, L"QTRACEU")
{
	m_bObjectOk = FALSE;

  wsprintf(m_wszFileName, L"%s%s.DAT", _pwszPathName, _pwszLogName);
	m_wszFileName[MAX_PATH] = NULL;

	wcsncpy_s(m_wszTracePath, MAX_PATH, pwszTracePath, MAX_PATH);


	// open and map th DB ( create it if not exists )
  m_pData = (STRUCT_SETTINGS*)m_mmf.Open( m_wszFileName, _pwszLogName, 
                               sizeof(STRUCT_SETTINGS), 
                               TRUE, TRUE);
 
  if(m_pData == NULL)
  {
		throw CFOLibException(0, L"CTraceLib: Cannot open/map [%s] file", m_wszFileName);
  } 

  DWORD dwFileSize = m_mmf.GetFileSize();
  // check if new/changed
  if(m_pData->dwSignature != dwFileSize)
  {
 		_SetDefault(dwFileSize);
  }

	try
	{
		InitializeCriticalSection(&m_cs_syncro);
	}
	catch (...)
	{
		CThreadLib::m_bObjectOk = FALSE;
		throw CFOLibException(0, L"CTraceLib: InitializeCriticalSection failed");
	}

	// run worker thread
  CThreadLib::m_bObjectOk = CThreadLib::RunThread( CTraceLib::EngineThread, THREAD_PRIORITY_BELOW_NORMAL);

	m_bObjectOk = TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Destructor CTraceLib::~CTraceLib
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CTraceLib::~CTraceLib()
{
	if( m_bObjectOk == TRUE )
	{
		CThreadLib::StopThread();
		::DeleteCriticalSection(&m_cs_syncro);
	}

  if(m_pData != NULL)
  {
    m_mmf.Flush();
    m_mmf.Close();
  }
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Sets default parameters 
//@
//@ Parameters:
//@		[IN]	_dwSignature
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
void CTraceLib::_SetDefault(DWORD _dwSignature)
{
  int i=0;

  if( m_pData != NULL)
	{
		::ZeroMemory(m_pData, sizeof(STRUCT_SETTINGS));

		m_pData->dwSignature = _dwSignature;

		// always disable level 0
		m_pData->bTraceLevel[LEVEL_0_DEBUG]=FALSE;
		// 
		for( int i=1; i<NUM_TRACE_LEVELS; i++)
		{
			m_pData->bTraceLevel[i]=TRUE;
		}
		// Persistance
		m_pData->nPersistDaysLogFile=1;
	}
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Clear old trace files
//@
//@ Parameters:
//@		[IN]	
//@				
//@ Return value:
//@    BOOL
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
BOOL CTraceLib::ClearOldFiles()
{
	CConversionsLib utility;
	HANDLE hFind;
	WIN32_FIND_DATA fd;  
	wchar_t wszStrPath[MAX_PATH+1];
	int nDays=0;

	// 
	// Clears elapsed files
	//

	nDays = m_pData->nPersistDaysLogFile;

	TRACE_LIB(LEVEL_8_WARNINGS,__FILE__,__LINE__,0, "Clearing old Trace Files");

	FILETIME range;
	// current time less number of days
	utility.TimetToFileTime( (::time(NULL) - ((long)nDays * 24L * 60L * 60L)), &range);

	// search the first file
	wsprintf(wszStrPath, L"%s%s", m_wszTracePath, L"*.txt");
	if( (hFind = ::FindFirstFile(wszStrPath, &fd)) != INVALID_HANDLE_VALUE)
	{
 		if(!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		&& ::CompareFileTime(&fd.ftLastWriteTime, &range) == -1)
		{
			wsprintf(wszStrPath, L"%s%s", m_wszTracePath, fd.cFileName);
			::_wremove(wszStrPath);
		}

		while( ::FindNextFile( hFind, &fd))
		{
 			if(!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& ::CompareFileTime(&fd.ftLastWriteTime, &range) == -1)
			{
				wsprintf(wszStrPath, L"%s%s", m_wszTracePath, fd.cFileName);
				::_wremove(wszStrPath);
			}
		}
		::FindClose(hFind);
	}
	
	return TRUE;
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ Worker thread for Application Tracing Management
//@
//@ Parameters:
//@		[IN]       pParam  : pointer to parent object (this)
//@				
//@ Return value:
//@    exit value
//@
//@ Note:
//@    worker thread
//@
//@////////////////////////////////////////////////////////////////////
/*static*/DWORD WINAPI CTraceLib::EngineThread(LPVOID pParam)
{
const int MAX_MESSAGE_LEN = MAX_PATH*2;
enum tagEventsName
{
  ID_EXIT_EVENT=0,
  ID_READ_QUEUE_A_EVENT,
  ID_READ_QUEUE_U_EVENT,

  NUM_EVENTS
};

enum tagStep
{
  STEP_IDLE=0,
  STEP_EXIT,

  NUM_STEPS
};

CTraceLib* _this;
HANDLE        hEvents[NUM_EVENTS];
BOOL          bRun=TRUE;
tagStep       step=STEP_IDLE;

CTraceLib::STRUCT_TRACE_RECORDA TrRecA;
CTraceLib::STRUCT_TRACE_RECORDU TrRecU;
//struct        tm P_t;
//timeb         tempo;
wchar_t       wszBuf[MAX_MESSAGE_LEN+1];
char          szBuf[MAX_MESSAGE_LEN+1];
HANDLE        hFile;
wchar_t       wszTraceFileName[MAX_PATH+1];
wchar_t       wszNewFileName[MAX_PATH+1];
int           nByteCount;

DWORD dwError;

try
{
  _this = (CTraceLib*)pParam;

  hEvents[ID_EXIT_EVENT] = _this->GetExitHandle();
  hEvents[ID_READ_QUEUE_A_EVENT] = _this->m_qTraceA.GetEvReadHandle();
  hEvents[ID_READ_QUEUE_U_EVENT] = _this->m_qTraceU.GetEvReadHandle();

  _this->SetObjectReady();
  
  step=STEP_IDLE;

  // Loop
  while(bRun)
  {
    switch(step)
    {
      case STEP_IDLE:
        // waiting 
        switch(::WaitForMultipleObjects(NUM_EVENTS,hEvents, FALSE, INFINITE))
        {
          // 
          // Writes the queued message into ASCII tracement file
          //
          case ID_READ_QUEUE_A_EVENT:
					{
            hFile = INVALID_HANDLE_VALUE;
						long lPosFile=0L;

						wsprintf(wszTraceFileName, L"%s%s", _this->m_wszTracePath, TRACE_FILE_A);

						hFile = ::CreateFile(	
							wszTraceFileName,  		          // File name
							GENERIC_WRITE,			            // Write Access
							FILE_SHARE_READ,								// Read access shared
							NULL,								            // No security attributes
							OPEN_ALWAYS,
							FILE_ATTRIBUTE_NORMAL | 
							FILE_FLAG_SEQUENTIAL_SCAN |
							FILE_FLAG_RANDOM_ACCESS,
							NULL);								

						if(hFile != INVALID_HANDLE_VALUE)
						{
							// reads all messages in queue
							while(_this->m_qTraceA.GetItem( (BYTE*)&TrRecA))
							{
								if( (strlen(TrRecA.szMsgInfo) + strlen(TrRecA.szMsgTrace)) >= MAX_MESSAGE_LEN)
								{
									strcpy_s(TrRecA.szMsgTrace, MAX_PATH, "Message too long !!!!\r\n");
								}

								strcpy_s(szBuf,MAX_PATH, TrRecA.szMsgInfo);
								strcat_s(szBuf,MAX_MESSAGE_LEN, TrRecA.szMsgTrace);
								szBuf[MAX_MESSAGE_LEN]=NULL;

								nByteCount = strlen(szBuf);

								lPosFile=::GetFileSize(hFile, NULL);
								long lRes = ::SetFilePointer (hFile, lPosFile, NULL, FILE_BEGIN) ; 
								DWORD dwBytesWritten=0;

								if (lRes == lPosFile)
								{ 
									::WriteFile(hFile, szBuf, nByteCount, &dwBytesWritten, NULL);
								} // end if ::SetFilePointer
								else
								{
									dwError = ::GetLastError();
									CBaseLib::SetBit(ERR_UPDATING_TRACE_FILE, TRUE);
									break;
								}
							} // end while
							
							::CloseHandle(hFile);

							// rename old file if bigger than ...
							if( lPosFile > 5000000 )
							{
								SYSTEMTIME tTime;
								GetLocalTime(&tTime);
								
								//ftime(&tempo);
								//localtime_s(&P_t, &tempo.time);            
								wsprintf(wszNewFileName,L"%sLOG_%04d%02d%02d_%02d%02d%02dA.TXT", _this->m_wszTracePath,
															tTime.wYear,
															tTime.wMonth,
															tTime.wDay,
															tTime.wHour,
															tTime.wMinute,
															tTime.wSecond);
								/*
															P_t.tm_year, 
															P_t.tm_mon, 
															P_t.tm_mday, 
															P_t.tm_hour, 
															P_t.tm_min, 
															P_t.tm_sec);
															*/
								if(::MoveFile(wszTraceFileName, wszNewFileName) == 0)
								{
									dwError = ::GetLastError();
									CBaseLib::SetBit(ERR_RENAMING_TRACE_FILE, TRUE);
								}
							}
						} // end if
						else
						{
							dwError = ::GetLastError();
							CBaseLib::SetBit(ERR_UPDATING_TRACE_FILE, TRUE);
						}
					}
					break; // end case ID_READ_QUEUE_A_EVENT:
          // 
          // Writes the queued message into UNICODE tracement file
          //
          case ID_READ_QUEUE_U_EVENT:
					{
            hFile = INVALID_HANDLE_VALUE;
						long lPosFile=0L;

						wsprintf(wszTraceFileName, L"%s%s", _this->m_wszTracePath, TRACE_FILE_U);
						
						hFile = ::CreateFile(	
							wszTraceFileName,  		          // File name
							GENERIC_WRITE,			            // Write Access
							0,									            // No sharing
							NULL,								            // No security attributes
							OPEN_ALWAYS,
							FILE_ATTRIBUTE_NORMAL | 
							FILE_FLAG_SEQUENTIAL_SCAN |
							FILE_FLAG_RANDOM_ACCESS,
							NULL);								

			      if(hFile != INVALID_HANDLE_VALUE)
			      {
						// reads all messages in queue
            while(_this->m_qTraceU.GetItem( (BYTE*)&TrRecU))
						{
							if( (wcslen(TrRecU.wszMsgInfo) + wcslen(TrRecU.wszMsgTrace)) >= MAX_MESSAGE_LEN)
							{
								wcscpy_s(TrRecU.wszMsgTrace, MAX_PATH, L"Message too long !!!!\r\n");
							}

							wcscpy_s(wszBuf,MAX_MESSAGE_LEN, TrRecU.wszMsgInfo);
							wcscat_s(wszBuf,MAX_MESSAGE_LEN, TrRecU.wszMsgTrace);
							wszBuf[MAX_MESSAGE_LEN]=NULL;

							nByteCount = wcslen(wszBuf)*sizeof(wchar_t);

								lPosFile=::GetFileSize(hFile, NULL);
								long lRes = ::SetFilePointer (hFile, lPosFile, NULL, FILE_BEGIN) ; 
								DWORD dwBytesWritten=0;

								if (lRes == lPosFile)
								{ 
									// Add Unicode header for Notepad
									if(lPosFile == 0)
									{
										wchar_t wszCh=0xFEFF;
										::WriteFile(hFile,&wszCh,2,&dwBytesWritten,NULL);
									}

									::WriteFile(hFile, wszBuf, nByteCount, &dwBytesWritten, NULL);
								} 
								else
								{
									dwError = ::GetLastError();
									CBaseLib::SetBit(ERR_UPDATING_TRACE_FILE, TRUE);
									break;
								}
							} // end while

							::CloseHandle(hFile);

							// rename old file if bigger than ...
							if( lPosFile > 1000000 )
							{
								SYSTEMTIME tTime;
								GetLocalTime(&tTime);
								
								//ftime(&tempo);
								//localtime_s(&P_t, &tempo.time);            

								wsprintf(wszNewFileName,L"%sLOG_%04d%02d%02d_%02d%02d%02dU.TXT", _this->m_wszTracePath,
															tTime.wYear,
															tTime.wMonth,
															tTime.wDay,
															tTime.wHour,
															tTime.wMinute,
															tTime.wSecond);
								/*
															P_t.tm_year, 
															P_t.tm_mon, 
															P_t.tm_mday, 
															P_t.tm_hour, 
															P_t.tm_min, 
															P_t.tm_sec);
															*/
								if(::MoveFile(wszTraceFileName, wszNewFileName) == 0)
								{
								  dwError = ::GetLastError();
									CBaseLib::SetBit(ERR_RENAMING_TRACE_FILE, TRUE);
								}								
							}
						} // end if
						else
						{
							dwError = ::GetLastError();
							CBaseLib::SetBit(ERR_UPDATING_TRACE_FILE, TRUE);
						}
					}
					break; // end case ID_READ_QUEUE_U_EVENT:

          //
					// exit request
					//
          case ID_EXIT_EVENT:
          default:
            step=STEP_EXIT;
          break;
				} // end switch(::WaitForMultipleObjects

			break; // end case STEP_IDLE

      case STEP_EXIT:
        bRun=FALSE;
      break;

		} // end switch(step)

		::Sleep(1);
	} // end while(bRun)


  return 0;
}
catch(...)
{
	CBaseLib::SetBit(ERR_EXCEPTION_ON_THR_TRACE, TRUE);
	CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"Exception on TraceLib thread");
	//
	return 1;
}
}
