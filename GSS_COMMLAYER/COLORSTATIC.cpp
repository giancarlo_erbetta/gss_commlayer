/*@***********************************************************************
@
@  PROGETTO           :  Linjegods
@
@  FILE               :  ColorStatic.cpp
@
@  VERSIONE           :  1.0
@
@  TIPO               :  Classe
@
@  CREAZIONE          :  
@
@  AGGIORNAMENTO      :  /
@
@  AUTORI             :  G. Alessio
@
@  DESCRIZIONE        :  Gestisce la colorazione dei controlli statici
@
@  COMPILATORE        :  VisualC++
@
@  SISTEMA OP.        :  WIN NT
@
@  NOTE               :  
@************************************************************************/

#include "stdafx.h"
#include "ColorStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorStatic

CColorStatic::CColorStatic()
{
	m_crTextColor = RGB (0, 0, 0);
	m_crBkColor = ::GetSysColor (COLOR_3DFACE);
	m_brBkgnd.CreateSolidBrush (m_crBkColor);
  m_status=STOPPED_COLOR;
}

CColorStatic::~CColorStatic()
{


}

void CColorStatic::SetTextColor (COLORREF crColor)
{
    m_crTextColor = crColor;
    Invalidate ();
}

void CColorStatic::SetBkColor (COLORREF crColor)
{
    m_crBkColor = crColor;
    m_brBkgnd.DeleteObject ();
    m_brBkgnd.CreateSolidBrush (crColor);
    Invalidate ();
}

void CColorStatic::ChangeStatus(int status)
{
  m_status=status;

  switch(m_status)
  {
		case DATA_COLOR:
			SetTextColor(RGB(0,0,0));
			SetBkColor(RGB(173,211,204));
		break;

		case STOPPED_COLOR:
			SetTextColor(RGB(0,0,0));
			SetBkColor(::GetSysColor (COLOR_3DFACE));
		break;

		case WAITING_COLOR:
			SetTextColor(RGB(0,0,0));
			SetBkColor(RGB(255,255,0));
		break;

		case WAITING_FOR_SYNC_COLOR:
			SetTextColor(RGB(0,0,0));
			SetBkColor(RGB(255,128,0));
		break;

		case RUNNING_COLOR:
			SetTextColor(RGB(0,0,0));
			SetBkColor(RGB(0,255,0));
		break;

		case ERROR_COLOR:
    default:
			SetTextColor(RGB(0,0,0));
			SetBkColor(RGB(255,0,0));
		break;
  }
}

BEGIN_MESSAGE_MAP(CColorStatic, CStatic)
	//{{AFX_MSG_MAP(CColorStatic)
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorStatic message handlers

HBRUSH CColorStatic::CtlColor (CDC* pDC, UINT nCtlColor)
{
    pDC->SetTextColor (m_crTextColor);
    pDC->SetBkColor (m_crBkColor);
    return (HBRUSH) m_brBkgnd;
}

