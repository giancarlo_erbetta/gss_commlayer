// OverlappedSocket.cpp: implementation of the COverlappedSocket class.
//
//////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "OverlappedSocket.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// SERVER
//////////////////////////////////////////////////////////////////////


//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor COverlappedSocketSR::COverlappedSocketSR
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@   socket SERVER
//@
//@////////////////////////////////////////////////////////////////////
COverlappedSocketSR::COverlappedSocketSR()
{
  m_bIsObjectOk=FALSE;
  m_socket=INVALID_SOCKET;			
  m_socketIn=INVALID_SOCKET;		
  m_hNetEvent=WSA_INVALID_EVENT;
  m_ov.hEvent=WSA_INVALID_EVENT;

  ::ZeroMemory(&m_sockAddr, sizeof(SOCKADDR_IN));
  ::ZeroMemory(&m_sockAddrIn, sizeof(SOCKADDR_IN));
  ::ZeroMemory(&m_ov, sizeof(WSAOVERLAPPED));

  // Create network event
  if ((m_hNetEvent = ::WSACreateEvent()) == WSA_INVALID_EVENT)
  {
	  m_lastError=ERR_UNABLE_CREATE_EV;
    return ;
  }

  // Create event for overlapped operations
  if ( (m_ov.hEvent = ::WSACreateEvent()) == WSA_INVALID_EVENT)
  {
	  m_lastError=ERR_UNABLE_CREATE_EV;
    return;
  }

  // OK !
  m_lastError=ERR_NO_ERROR;
  m_bIsObjectOk=TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Destructor COverlappedSocketSR::~COverlappedSocketSR
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
COverlappedSocketSR::~COverlappedSocketSR()
{
  // Close ALL Events
  if(m_hNetEvent != WSA_INVALID_EVENT)
    ::WSACloseEvent(m_hNetEvent);

  if(m_ov.hEvent != WSA_INVALID_EVENT)
    ::WSACloseEvent(m_ov.hEvent);

  if( m_socketIn != INVALID_SOCKET)
    ::closesocket(m_socketIn);

  if( m_socket != INVALID_SOCKET)
    ::closesocket(m_socket);
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ Create the socket
//@
//@ Parameters:
//@		[IN]             pwszLocalIPAddr : host address
//@		[IN]             nPort           : used port
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_INVALID_IP_ADDR
//@  - ERR_WSA_SOCKET
//@  - ERR_WSA_SOCKET_OPTION
//@  - ERR_WSA_EVENT_SELECT
//@  - ERR_BIND_SOCKET
//@  - ERR_LISTEN_SOCKET
//@
//@////////////////////////////////////////////////////////////////////
//BOOL COverlappedSocketSR::Create( const wchar_t* pwszLocalIPAddr, const wchar_t* pwszPort )
BOOL COverlappedSocketSR::Create( const wchar_t* pwszLocalIPAddr, int nPort )
{
	wchar_t  wszAddrString[MAX_PATH+1];

	wsprintf(wszAddrString, L"%s:%d", pwszLocalIPAddr, nPort);

  // Resolve IP address and fill SOCKADDR struct
  m_nLen = sizeof(SOCKADDR_IN);
  m_nRetValue = ::WSAStringToAddress(wszAddrString, AF_INET, NULL, (SOCKADDR*)&m_sockAddr, &m_nLen);
  if (m_nRetValue == SOCKET_ERROR)
  {
    m_lastError=ERR_INVALID_IP_ADDR;
    return FALSE;
  }
  
  // Create socket
  if ( (m_socket = ::WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED)) == INVALID_SOCKET)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET;
    return FALSE;
  }

  //
  // Set socket options
  //

  // SO_KEEPALIVE
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_KEEPALIVE, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // SO_REUSEADDR
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // SO_LINGER
  m_linger.l_onoff = 1;
  m_linger.l_linger = 0;
  m_nLen=sizeof(LINGER);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_LINGER, (char*)&m_linger, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // TCP_NODELAY
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  //
  // DeSelect network events
  //
  if ( (::WSAEventSelect(m_socket, m_hNetEvent, 0)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_EVENT_SELECT;
    return FALSE;
  }

/* -------------------  SERVER  ----------------------- */

  //
  // Bind socket
  //
  m_nLen=sizeof(SOCKADDR_IN);
  if ( (::bind(m_socket, (SOCKADDR*)&m_sockAddr, m_nLen )) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_BIND_SOCKET;
    return FALSE;
  }

  //
  // Select network events
  //
  if ( (::WSAEventSelect(m_socket, m_hNetEvent, FD_ACCEPT)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_EVENT_SELECT;
    return FALSE;
  }

  m_bIsObjectOk=TRUE;
  m_lastError=ERR_NO_ERROR;
  return TRUE;
} // end COverlappedSocketSR::Create(


//@//////////////////////////////////////////////////////////////////////
//@
//@ Opens the link
//@
//@ Parameters:
//@		[IN]             hAbortEv  : abort event handler
//@		[IN]             dwTimeout : timeout value
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_EWOULDBLOCK
//@  - ERR_NOTHANDLED
//@  - ERR_TIMEOUT
//@  - ERR_WSA_EVENT_SELECT
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketSR::OpenLink(HANDLE hAbortEv, DWORD dwTimeout )
{
enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_ACCEPT,

  NUM_EVENTS
};
BOOL        bRetVal=FALSE;
HANDLE      hEvents[NUM_EVENTS];


  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  //
  // Listen, just ONE client
  //
  if ( (::listen(m_socket, 1)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_LISTEN_SOCKET;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_ACCEPT]= m_hNetEvent;

  //
  // ACCEPT
  //
  // ::WSAResetEvent(m_hNetEvent);
  switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, dwTimeout/*WSA_INFINITE*/, FALSE))
  {
	  // User Abort event
    case ID_EV_ABORT:		
      m_lastError=ERR_USER_ABORT;
      bRetVal = FALSE;
	  break;

	  // Network event (FD_ACCEPT)
    case ID_EV_ACCEPT:	
      m_nLen = sizeof(SOCKADDR_IN);
		  m_socketIn = ::WSAAccept(m_socket, (SOCKADDR*)&m_sockAddrIn, &m_nLen, NULL, 0);
		  if (m_socketIn == INVALID_SOCKET)
		  {
			  m_nWSAError = ::WSAGetLastError();
			  switch (m_nWSAError)
			  {
				  // No connection detected
          case WSAEWOULDBLOCK:	
					  //::WSAResetEvent(m_hNetEvent);
            m_lastError=ERR_EWOULDBLOCK;
            bRetVal = FALSE;
				  break;

				  // Some error
          default:				
            m_lastError=ERR_NOTHANDLED;
            bRetVal = FALSE;
				  break;
			  }
		  }
		  
      // Connection detected
      else	
		  {
        bRetVal = TRUE;
		  }

	  break;

	  case WSA_WAIT_TIMEOUT:		
      m_lastError=ERR_TIMEOUT;
      bRetVal = FALSE;
	  break;

	  case WSA_WAIT_FAILED:		
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;

	  default:					
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;
  }


  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;

    // Select network events
    ::WSAResetEvent(m_hNetEvent);
    if ( (::WSAEventSelect(m_socketIn, m_hNetEvent, FD_READ | FD_CLOSE)) == SOCKET_ERROR)
    {
	    m_nWSAError = ::WSAGetLastError();
      m_lastError=ERR_WSA_EVENT_SELECT;
      bRetVal = FALSE;
    }
  }
  
  return bRetVal;
} // end COverlappedSocketSR::OpenLink(

//@//////////////////////////////////////////////////////////////////////
//@
//@ Reads a READY message from the link
//@
//@ Parameters:
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarRead : number of bytes read
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_ENUM_NET_EV
//@  - ERR_REMOTE_CLOSE
//@  - ERR_SOCKET_READ
//@  - ERR_NOTHANDLED
//@  - ERR_NO_DATA
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketSR::ReadMessage(WSABUF* pwsaBuffer, DWORD* pdwCarRead )
{
enum tagEvents
{
  ID_EV_NET,

  NUM_EVENTS
};

WSANETWORKEVENTS wsaNetworkEvents;		
BOOL             bRetVal=FALSE;
DWORD            dwFlags;
HANDLE           hEvents[NUM_EVENTS];

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

	if ( (::WSAEnumNetworkEvents(m_socketIn, m_hNetEvent, &wsaNetworkEvents)) == SOCKET_ERROR)
	{
		m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_ENUM_NET_EV;
    bRetVal = FALSE;
	}
	// Manage events
  else	
	{
    if(wsaNetworkEvents.lNetworkEvents == 0)
    {
      m_lastError=ERR_NO_DATA;
      bRetVal = FALSE;
    }

		if (wsaNetworkEvents.lNetworkEvents & FD_CLOSE)
		{
      m_lastError=ERR_REMOTE_CLOSE;
      bRetVal = FALSE;
		} 
		else if (wsaNetworkEvents.lNetworkEvents & FD_READ)
		{
			// If read error
			if (wsaNetworkEvents.iErrorCode[FD_READ_BIT] != 0)
			{
        m_lastError=ERR_SOCKET_READ;
        bRetVal = FALSE;
			}
			else
			{

        dwFlags = 0;
        if ( (::WSARecv(m_socketIn, pwsaBuffer, 1, pdwCarRead, &dwFlags, &m_ov, NULL)) == SOCKET_ERROR)
        {
	        m_nWSAError = ::WSAGetLastError();
	        switch (m_nWSAError)
	        {
		        // Operation in progress
            case WSA_IO_PENDING:	
              hEvents[ID_EV_NET]= m_ov.hEvent;
              switch (::WSAWaitForMultipleEvents(1, hEvents, FALSE, WSA_INFINITE, FALSE))
              {
	              // Overlapped event
                case ID_EV_NET:		
		              // Some error ?
                  if (::WSAGetOverlappedResult(m_socketIn, &m_ov, pdwCarRead, FALSE, &dwFlags) != TRUE)
		              {
                    m_nWSAError = ::WSAGetLastError();
                    m_lastError=ERR_SOCKET_READ;
                    bRetVal = FALSE;
		              }

		              // Read ok
                  else	
		              {
                    bRetVal = TRUE;
		              }
		              break;

	              case WSA_WAIT_FAILED:		// Some error
                  m_lastError=ERR_SOCKET_READ;
                  bRetVal = FALSE;
	              break;
              }
            break;

            default:	
              m_lastError=ERR_SOCKET_READ;
              bRetVal = FALSE;
		        break;
	        }
        }
        // Overlapped operation completed immediately
        else	
        {
          bRetVal=TRUE;
        }
			}
		} // end else if READ flag
    else if(wsaNetworkEvents.lNetworkEvents != 0)
    {
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
    }

	} // end else manage events

  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;
} // end BOOL COverlappedSocketSR::ReadMessage(


//@//////////////////////////////////////////////////////////////////////
//@
//@ Receives a message from the link
//@
//@ Parameters:
//@		[IN]             hAbortEv   : abort event handler
//@		[IN]             dwTimeout  : timeout value
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarRead : number of bytes read
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_ENUM_NET_EV
//@  - ERR_REMOTE_CLOSE
//@  - ERR_SOCKET_READ
//@  - ERR_NOTHANDLED
//@  - ERR_TIMEOUT
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketSR::ReceiveMessage(HANDLE hAbortEv, DWORD dwTimeout,
                            WSABUF* pwsaBuffer, DWORD* pdwCarRead )
{
enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_NET,

  NUM_EVENTS
};

WSANETWORKEVENTS wsaNetworkEvents;		
BOOL             bRetVal=FALSE;
HANDLE           hEvents[NUM_EVENTS];
DWORD            dwFlags;

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_NET]= m_hNetEvent;

  switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, dwTimeout/*WSA_INFINITE*/, FALSE))
  {
	  // User Abort event
    case ID_EV_ABORT:		
      m_lastError=ERR_USER_ABORT;
      bRetVal = FALSE;
	  break;

	  // Network event (FD_READ | FD_CLOSE)
    case ID_EV_NET:	
		  if ( (::WSAEnumNetworkEvents(m_socketIn, m_hNetEvent, &wsaNetworkEvents)) == SOCKET_ERROR)
		  {
			  m_nWSAError = ::WSAGetLastError();
        m_lastError=ERR_ENUM_NET_EV;
        bRetVal = FALSE;
		  }
		  // Manage events
      else	
		  {
			  if (wsaNetworkEvents.lNetworkEvents & FD_CLOSE)
			  {
          m_lastError=ERR_REMOTE_CLOSE;
          bRetVal = FALSE;
			  } 

			  else if (wsaNetworkEvents.lNetworkEvents & FD_READ)
			  {
				  // If read error
				  if (wsaNetworkEvents.iErrorCode[FD_READ_BIT] != 0)
				  {
            m_lastError=ERR_SOCKET_READ;
            bRetVal = FALSE;
				  }
				  else
				  {

            dwFlags = 0;
            if ( (::WSARecv(m_socketIn, pwsaBuffer, 1, pdwCarRead, &dwFlags, &m_ov, NULL)) == SOCKET_ERROR)
            {
	            m_nWSAError = ::WSAGetLastError();
	            switch (m_nWSAError)
	            {
		            // Operation in progress
                case WSA_IO_PENDING:	
                  hEvents[ID_EV_NET]= m_ov.hEvent;
                  switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, WSA_INFINITE, FALSE))
                  {
	                  // Exit event
                    case ID_EV_ABORT:	
                      m_lastError=ERR_USER_ABORT;
                      bRetVal = FALSE;
	                  break;

	                  // Overlapped event
                    case ID_EV_NET:		
		                  // Some error ?
                      if (::WSAGetOverlappedResult(m_socketIn, &m_ov, pdwCarRead, FALSE, &dwFlags) != TRUE)
		                  {
                        m_nWSAError = ::WSAGetLastError();
                        m_lastError=ERR_SOCKET_READ;
                        bRetVal = FALSE;
		                  }

		                  // Read ok
                      else	
		                  {
                        bRetVal = TRUE;
		                  }
		                  break;

	                  case WSA_WAIT_FAILED:		// Some error
                      m_lastError=ERR_SOCKET_READ;
                      bRetVal = FALSE;
	                  break;
                  }
                break;

                default:	
                  m_lastError=ERR_SOCKET_READ;
                  bRetVal = FALSE;
		            break;
	            }
            }
            // Overlapped operation completed immediately
            else	
            {
              bRetVal=TRUE;
            }
				  }
			  } // end else if READ flag

        else
        {
          m_lastError=ERR_NOTHANDLED;
          bRetVal = FALSE;
        }

		  } // end else manage events

    break;

	  case WSA_WAIT_TIMEOUT:		
      m_lastError=ERR_TIMEOUT;
      bRetVal = FALSE;
	  break;

	  case WSA_WAIT_FAILED:		
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;

	  default:					
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;

  } // end switch (::WSAWaitForMultipleEvents(


  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;
} // end BOOL COverlappedSocketSR::ReceiveMessage(

//@//////////////////////////////////////////////////////////////////////
//@
//@ Writes a message to the link
//@
//@ Parameters:
//@		[IN]             hAbortEv   : abort event handler
//@		[IN]             dwTimeout  : timeout value
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarWritten : number of bytes written
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_SOCKET_WRITE
//@  - ERR_NOTHANDLED  
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketSR::WriteMessage(HANDLE hAbortEv, DWORD dwTimeout,
                            WSABUF* pwsaBuffer, DWORD* pdwCarWritten )
{

enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_NET,

  NUM_EVENTS
};

BOOL   bRetVal=FALSE;
HANDLE hEvents[NUM_EVENTS];
DWORD  dwFlags;

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_NET]= m_ov.hEvent;

  if (::WSASend(m_socketIn, pwsaBuffer, 1, pdwCarWritten, 0, &m_ov, NULL) == SOCKET_ERROR)
  {
	  int nRetErr=::WSAGetLastError();
    switch (nRetErr)
	  {
		  // Operation in progress
      case WSA_IO_PENDING:	
        switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, WSA_INFINITE, FALSE))
        {
	        // Exit event
          case ID_EV_ABORT:	
            m_lastError=ERR_USER_ABORT;
            bRetVal = FALSE;
	        break;

	        // Overlapped event
          case ID_EV_NET:		
		        // Some error ?
            if (::WSAGetOverlappedResult(m_socketIn, &m_ov, pdwCarWritten, FALSE, &dwFlags) != TRUE)
		        {
              m_nWSAError = ::WSAGetLastError();
              m_lastError=ERR_SOCKET_WRITE;
              bRetVal = FALSE;
		        }

		        // Write ok
            else	
		        {
              bRetVal = TRUE;
		        }
	        break;

          case WSA_WAIT_FAILED:		// Some error
            m_nWSAError = ::WSAGetLastError();
            m_lastError=ERR_SOCKET_WRITE;
            bRetVal = FALSE;
          break;
        } // end switch (::WSAWaitForMultipleEvents(
      break;

		  case WSAEWOULDBLOCK:
        m_lastError=ERR_SOCKET_WRITE;
        bRetVal = FALSE;
		  break;

		  default:	
        m_lastError=ERR_NOTHANDLED;
        bRetVal = FALSE;
		  break;

	  }
  } // end if (::WSASend(
  
  // Overlapped operation completed immediately
  else	
  {
    bRetVal = TRUE;
  }


  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;

} // end BOOL COverlappedSocketSR::WriteMessage(



//////////////////////////////////////////////////////////////////////
// CLIENT
//////////////////////////////////////////////////////////////////////

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor COverlappedSocketCL::COverlappedSocketCL
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@   SERVER
//@
//@////////////////////////////////////////////////////////////////////
COverlappedSocketCL::COverlappedSocketCL()
{
  m_bIsObjectOk=FALSE;
  m_socket=INVALID_SOCKET;			
  m_hNetEvent=WSA_INVALID_EVENT;
  m_ov.hEvent=WSA_INVALID_EVENT;

  ::ZeroMemory(&m_sockAddr, sizeof(SOCKADDR_IN));
  ::ZeroMemory(&m_ov, sizeof(WSAOVERLAPPED));

  // Create network event
  if ((m_hNetEvent = ::WSACreateEvent()) == WSA_INVALID_EVENT)
  {
	  m_lastError=ERR_UNABLE_CREATE_EV;
    return ;
  }

  // Create event for overlapped operations
  if ( (m_ov.hEvent = ::WSACreateEvent()) == WSA_INVALID_EVENT)
  {
	  m_lastError=ERR_UNABLE_CREATE_EV;
    return;
  }

  // OK !
  m_lastError=ERR_NO_ERROR;
  m_bIsObjectOk=TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Destructor COverlappedSocketCL::~COverlappedSocketCL
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
COverlappedSocketCL::~COverlappedSocketCL()
{
  // Close ALL Events
  if(m_hNetEvent != WSA_INVALID_EVENT)
    ::WSACloseEvent(m_hNetEvent);

  if(m_ov.hEvent != WSA_INVALID_EVENT)
    ::WSACloseEvent(m_ov.hEvent);

  if( m_socket != INVALID_SOCKET)
    ::closesocket(m_socket);
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Create the socket
//@
//@ Parameters:
//@		[IN]             pwszRemoteIPAddr : remote address
//@		[IN]             nPort            : used port
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_INVALID_IP_ADDR
//@  - ERR_WSA_SOCKET
//@  - ERR_WSA_SOCKET_OPTION
//@  - ERR_WSA_EVENT_SELECT
//@  - ERR_BIND_SOCKET
//@  - ERR_LISTEN_SOCKET
//@
//@////////////////////////////////////////////////////////////////////
//BOOL COverlappedSocketCL::Create( const wchar_t* pwszRemoteIPAddr, const wchar_t* pwszPort )
BOOL COverlappedSocketCL::Create( const wchar_t* pwszRemoteIPAddr, int nPort )
{
	wchar_t  wszAddrString[MAX_PATH+1];

	wsprintf(wszAddrString, L"%s:%d", pwszRemoteIPAddr, nPort);

  // Resolve IP address and fill SOCKADDR struct
  m_nLen = sizeof(SOCKADDR_IN);
  m_nRetValue = ::WSAStringToAddress((LPTSTR)(LPCTSTR)wszAddrString, AF_INET, NULL, (SOCKADDR*)&m_sockAddr, &m_nLen);
  if (m_nRetValue == SOCKET_ERROR)
  {
    m_lastError=ERR_INVALID_IP_ADDR;
    return FALSE;
  }

  // Create socket
  if ( (m_socket = ::WSASocket(PF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED)) == INVALID_SOCKET)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET;
    return FALSE;
  }

  //
  // Set socket options
  //

  // SO_KEEPALIVE
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_KEEPALIVE, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // SO_REUSEADDR
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // SO_LINGER
  m_linger.l_onoff = 1;
  m_linger.l_linger = 0;
  m_nLen=sizeof(LINGER);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_LINGER, (char*)&m_linger, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // TCP_NODELAY
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  //
  // Select network events
  //
  ::WSAResetEvent(m_hNetEvent);
  if ( (::WSAEventSelect(m_socket, m_hNetEvent, FD_CONNECT)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_EVENT_SELECT;
    return FALSE;
  }

  m_bIsObjectOk=TRUE;
  m_lastError=ERR_NO_ERROR;
  return TRUE;
} // end COverlappedSocketCL::Create(


//@//////////////////////////////////////////////////////////////////////
//@
//@ Opens the link
//@
//@ Parameters:
//@		[IN]             hAbortEv  : abort event handler
//@		[IN]             dwTimeout : timeout value
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_EWOULDBLOCK
//@  - ERR_NOTHANDLED
//@  - ERR_TIMEOUT
//@  - ERR_WSA_EVENT_SELECT
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketCL::OpenLink(HANDLE hAbortEv, DWORD dwTimeout )
{
enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_CONNECT,

  NUM_EVENTS
};
BOOL        bRetVal=FALSE;
HANDLE      hEvents[NUM_EVENTS];
WSANETWORKEVENTS wsaNetworkEvents;		

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_CONNECT]= m_hNetEvent;

  m_nLen = sizeof(SOCKADDR_IN);
  if (::WSAConnect(m_socket, (SOCKADDR*)&m_sockAddr, m_nLen, NULL, NULL, NULL, NULL) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
	  switch (m_nWSAError)
	  {
      case WSAEISCONN:
        bRetVal = TRUE;
      break;

		  // Connection in progress
      case WSAEWOULDBLOCK:
      case WSAEALREADY:
        switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, dwTimeout/*WSA_INFINITE*/, FALSE))
        {
	        // User Abort event
          case ID_EV_ABORT:		
            m_lastError=ERR_USER_ABORT;
            bRetVal = FALSE;
	        break;

	        // Network event (FD_CONNECT)
          case ID_EV_CONNECT:	
				    if (::WSAEnumNetworkEvents(m_socket, m_hNetEvent, &wsaNetworkEvents) == SOCKET_ERROR)
				    {
  				    m_nWSAError = ::WSAGetLastError();
              m_lastError=ERR_CONNECT_SOCKET;
              bRetVal = FALSE;
				    }
				    
            // Check for errors
            else if (wsaNetworkEvents.lNetworkEvents & FD_CONNECT)
				    {
              if(wsaNetworkEvents.iErrorCode[FD_CONNECT_BIT] != 0)
					    {
                m_lastError=ERR_TIMEOUT;
                bRetVal = FALSE;
					    }
					    else
					    {
                bRetVal=TRUE;
					    }
				    }
          break;

	        case WSA_WAIT_TIMEOUT:		
            m_lastError=ERR_TIMEOUT;
            bRetVal = FALSE;
	        break;

	        case WSA_WAIT_FAILED:		
            m_lastError=ERR_NOTHANDLED;
            bRetVal = FALSE;
	        break;

	        default:					
            m_lastError=ERR_NOTHANDLED;
            bRetVal = FALSE;
	        break;
        } // end switch (::WSAWaitForMultipleEvents(
		  break;

		  //	Connection not available at this moment
      case WSAECONNREFUSED:	
		  case WSAENETUNREACH:
		  case WSAETIMEDOUT:
        m_lastError=ERR_CONNECT_SOCKET;
        bRetVal = FALSE;
		  break;

      // Some error
		  default:				
        m_lastError=ERR_NOTHANDLED;
        bRetVal = FALSE;
		  break;
	  }
  }

  // Connected successfully
  else
  {
    bRetVal = TRUE;
  }

  
  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;

    // Select network events
    ::WSAResetEvent(m_hNetEvent);
    if ( (::WSAEventSelect(m_socket, m_hNetEvent, FD_READ | FD_CLOSE)) == SOCKET_ERROR)
    {
	    m_nWSAError = ::WSAGetLastError();
      m_lastError=ERR_WSA_EVENT_SELECT;
      bRetVal = FALSE;
    }
  }
  
  return bRetVal;
} // end COverlappedSocketCL::OpenLink(

//@//////////////////////////////////////////////////////////////////////
//@
//@ Reads a READY message from the link
//@
//@ Parameters:
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarRead : number of bytes read
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_ENUM_NET_EV
//@  - ERR_REMOTE_CLOSE
//@  - ERR_SOCKET_READ
//@  - ERR_NOTHANDLED
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketCL::ReadMessage(WSABUF* pwsaBuffer, DWORD* pdwCarRead )
{
enum tagEvents
{
  ID_EV_NET,

  NUM_EVENTS
};

WSANETWORKEVENTS wsaNetworkEvents;		
BOOL             bRetVal=FALSE;
DWORD            dwFlags;
HANDLE           hEvents[NUM_EVENTS];

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

	if ( (::WSAEnumNetworkEvents(m_socket, m_hNetEvent, &wsaNetworkEvents)) == SOCKET_ERROR)
	{
		m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_ENUM_NET_EV;
    bRetVal = FALSE;
	}
	// Manage events
  else	
	{
		if (wsaNetworkEvents.lNetworkEvents & FD_CLOSE)
		{
      m_lastError=ERR_REMOTE_CLOSE;
      bRetVal = FALSE;
		} 

		else if (wsaNetworkEvents.lNetworkEvents & FD_READ)
		{
			// If read error
			if (wsaNetworkEvents.iErrorCode[FD_READ_BIT] != 0)
			{
        m_lastError=ERR_SOCKET_READ;
        bRetVal = FALSE;
			}
			else
			{

        dwFlags = 0;
        if ( (::WSARecv(m_socket, pwsaBuffer, 1, pdwCarRead, &dwFlags, &m_ov, NULL)) == SOCKET_ERROR)
        {
	        m_nWSAError = ::WSAGetLastError();
	        switch (m_nWSAError)
	        {
		        // Operation in progress
            case WSA_IO_PENDING:	
              hEvents[ID_EV_NET]= m_ov.hEvent;
              switch (::WSAWaitForMultipleEvents(1, hEvents, FALSE, WSA_INFINITE, FALSE))
              {
	              // Overlapped event
                case ID_EV_NET:		
		              // Some error ?
                  if (::WSAGetOverlappedResult(m_socket, &m_ov, pdwCarRead, FALSE, &dwFlags) != TRUE)
		              {
                    m_nWSAError = ::WSAGetLastError();
                    m_lastError=ERR_SOCKET_READ;
                    bRetVal = FALSE;
		              }

		              // Read ok
                  else	
		              {
                    bRetVal = TRUE;
		              }
		              break;

	              case WSA_WAIT_FAILED:		// Some error
                  m_lastError=ERR_SOCKET_READ;
                  bRetVal = FALSE;
	              break;
              }
            break;

            default:	
              m_lastError=ERR_SOCKET_READ;
              bRetVal = FALSE;
		        break;
	        }
        }
        // Overlapped operation completed immediately
        else	
        {
          bRetVal=TRUE;
        }
			}
		} // end else if READ flag

    else
    {
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
    }
	} // end else manage events

  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;
} // end BOOL COverlappedSocketSR::ReadMessage(

//@//////////////////////////////////////////////////////////////////////
//@
//@ Receives a message from the link
//@
//@ Parameters:
//@		[IN]             hAbortEv   : abort event handler
//@		[IN]             dwTimeout  : timeout value
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarRead : number of bytes read
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_ENUM_NET_EV
//@  - ERR_REMOTE_CLOSE
//@  - ERR_SOCKET_READ
//@  - ERR_NOTHANDLED
//@  - ERR_TIMEOUT
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketCL::ReceiveMessage(HANDLE hAbortEv, DWORD dwTimeout,
                            WSABUF* pwsaBuffer, DWORD* pdwCarRead )
{
enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_NET,

  NUM_EVENTS
};

WSANETWORKEVENTS wsaNetworkEvents;		
BOOL             bRetVal=FALSE;
HANDLE           hEvents[NUM_EVENTS];
DWORD            dwFlags;

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_NET]= m_hNetEvent;

  switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, dwTimeout/*WSA_INFINITE*/, FALSE))
  {
	  // User Abort event
    case ID_EV_ABORT:		
      m_lastError=ERR_USER_ABORT;
      bRetVal = FALSE;
	  break;

	  // Network event (FD_READ | FD_CLOSE)
    case ID_EV_NET:	
		  if ( (::WSAEnumNetworkEvents(m_socket, m_hNetEvent, &wsaNetworkEvents)) == SOCKET_ERROR)
		  {
			  m_nWSAError = ::WSAGetLastError();
        m_lastError=ERR_ENUM_NET_EV;
        bRetVal = FALSE;
		  }
		  // Manage events
      else	
		  {
			  if (wsaNetworkEvents.lNetworkEvents & FD_CLOSE)
			  {
          m_lastError=ERR_REMOTE_CLOSE;
          bRetVal = FALSE;
			  } 

			  else if (wsaNetworkEvents.lNetworkEvents & FD_READ)
			  {
				  // If read error
				  if (wsaNetworkEvents.iErrorCode[FD_READ_BIT] != 0)
				  {
            m_lastError=ERR_SOCKET_READ;
            bRetVal = FALSE;
				  }
				  else
				  {

            dwFlags = 0;
            if ( (::WSARecv(m_socket, pwsaBuffer, 1, pdwCarRead, &dwFlags, &m_ov, NULL)) == SOCKET_ERROR)
            {
	            m_nWSAError = ::WSAGetLastError();
	            switch (m_nWSAError)
	            {
		            // Operation in progress
                case WSA_IO_PENDING:	
                  hEvents[ID_EV_NET]= m_ov.hEvent;
                  switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, WSA_INFINITE, FALSE))
                  {
	                  // Exit event
                    case ID_EV_ABORT:	
                      m_lastError=ERR_USER_ABORT;
                      bRetVal = FALSE;
	                  break;

	                  // Overlapped event
                    case ID_EV_NET:		
		                  // Some error ?
                      if (::WSAGetOverlappedResult(m_socket, &m_ov, pdwCarRead, FALSE, &dwFlags) != TRUE)
		                  {
                        m_nWSAError = ::WSAGetLastError();
                        m_lastError=ERR_SOCKET_READ;
                        bRetVal = FALSE;
		                  }

		                  // Read ok
                      else	
		                  {
                        bRetVal = TRUE;
		                  }
		                  break;

	                  case WSA_WAIT_FAILED:		// Some error
                      m_lastError=ERR_SOCKET_READ;
                      bRetVal = FALSE;
	                  break;
                  }
                break;

                default:	
                  m_lastError=ERR_SOCKET_READ;
                  bRetVal = FALSE;
		            break;
	            }
            }
            // Overlapped operation completed immediately
            else	
            {
              bRetVal=TRUE;
            }
				  }
			  } // end else if READ flag

        else
        {
          m_lastError=ERR_NOTHANDLED;
          bRetVal = FALSE;
        }

		  } // end else manage events

    break;

	  case WSA_WAIT_TIMEOUT:		
      m_lastError=ERR_TIMEOUT;
      bRetVal = FALSE;
	  break;

	  case WSA_WAIT_FAILED:		
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;

	  default:					
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;

  } // end switch (::WSAWaitForMultipleEvents(


  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;
} // end BOOL COverlappedSocketCL::ReceiveMessage(

//@//////////////////////////////////////////////////////////////////////
//@
//@ Writes a message to the link
//@
//@ Parameters:
//@		[IN]             hAbortEv   : abort event handler
//@		[IN]             dwTimeout  : timeout value
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarWritten : number of bytes written
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_SOCKET_WRITE
//@  - ERR_NOTHANDLED  
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketCL::WriteMessage(HANDLE hAbortEv, DWORD dwTimeout,
                            WSABUF* pwsaBuffer, DWORD* pdwCarWritten )
{
enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_NET,

  NUM_EVENTS
};

BOOL   bRetVal=FALSE;
HANDLE hEvents[NUM_EVENTS];
DWORD  dwFlags;

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_NET]= m_ov.hEvent;

  if (::WSASend(m_socket, pwsaBuffer, 1, pdwCarWritten, 0, &m_ov, NULL) == SOCKET_ERROR)
  {
	  int nErr=::WSAGetLastError();
    switch (nErr)
	  {
		  // Operation in progress
      case WSA_IO_PENDING:	
        switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, WSA_INFINITE, FALSE))
        {
	        // Exit event
          case ID_EV_ABORT:	
            m_lastError=ERR_USER_ABORT;
            bRetVal = FALSE;
	        break;

	        // Overlapped event
          case ID_EV_NET:		
		        // Some error ?
            if (::WSAGetOverlappedResult(m_socket, &m_ov, pdwCarWritten, FALSE, &dwFlags) != TRUE)
		        {
              m_nWSAError = ::WSAGetLastError();
              m_lastError=ERR_SOCKET_WRITE;
              bRetVal = FALSE;
		        }

		        // Write ok
            else	
		        {
              bRetVal = TRUE;
		        }
	        break;

          case WSA_WAIT_FAILED:		// Some error
            m_nWSAError = ::WSAGetLastError();
            m_lastError=ERR_SOCKET_WRITE;
            bRetVal = FALSE;
          break;
        } // end switch (::WSAWaitForMultipleEvents(
      break;

		  case WSAEWOULDBLOCK:
        m_lastError=ERR_SOCKET_WRITE;
        bRetVal = FALSE;
		  break;

		  default:	
        m_lastError=ERR_NOTHANDLED;
        bRetVal = FALSE;
		  break;

	  }
  } // end if (::WSASend(
  
  // Overlapped operation completed immediately
  else	
  {
    bRetVal = TRUE;
  }


  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;

} // end BOOL COverlappedSocketCL::WriteMessage(




//////////////////////////////////////////////////////////////////////
// UDP
//////////////////////////////////////////////////////////////////////


//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor COverlappedSocketUDP::COverlappedSocketUDP
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@   UPD socket
//@
//@////////////////////////////////////////////////////////////////////
COverlappedSocketUDP::COverlappedSocketUDP()
{
  m_bIsObjectOk=FALSE;
  m_socket=INVALID_SOCKET;			
  m_socketIn=INVALID_SOCKET;		
  m_hNetEvent=WSA_INVALID_EVENT;
  m_ov.hEvent=WSA_INVALID_EVENT;

  ::ZeroMemory(&m_sockAddr, sizeof(SOCKADDR_IN));
  ::ZeroMemory(&m_sockAddrIn, sizeof(SOCKADDR_IN));
  ::ZeroMemory(&m_ov, sizeof(WSAOVERLAPPED));

  // Create network event
  if ((m_hNetEvent = ::WSACreateEvent()) == WSA_INVALID_EVENT)
  {
	  m_lastError=ERR_UNABLE_CREATE_EV;
    return ;
  }

  // Create event for overlapped operations
  if ( (m_ov.hEvent = ::WSACreateEvent()) == WSA_INVALID_EVENT)
  {
	  m_lastError=ERR_UNABLE_CREATE_EV;
    return;
  }

  // OK !
  m_lastError=ERR_NO_ERROR;
  m_bIsObjectOk=TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Destructor COverlappedSocketUDP::~COverlappedSocketUDP
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
COverlappedSocketUDP::~COverlappedSocketUDP()
{
  // Close ALL Events
  if(m_hNetEvent != WSA_INVALID_EVENT)
    ::WSACloseEvent(m_hNetEvent);

  if(m_ov.hEvent != WSA_INVALID_EVENT)
    ::WSACloseEvent(m_ov.hEvent);

  if( m_socketIn != INVALID_SOCKET)
    ::closesocket(m_socketIn);

  if( m_socket != INVALID_SOCKET)
    ::closesocket(m_socket);
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ Create the socket
//@
//@ Parameters:
//@		[IN]             pwszLocalIPAddr : host address
//@		[IN]             nPort           : used port
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_INVALID_IP_ADDR
//@  - ERR_WSA_SOCKET
//@  - ERR_WSA_SOCKET_OPTION
//@  - ERR_WSA_EVENT_SELECT
//@  - ERR_BIND_SOCKET
//@  - ERR_LISTEN_SOCKET
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketUDP::Create( const wchar_t* pwszLocalIPAddr, int nPort )
{
	wchar_t  wszAddrString[MAX_PATH+1];

	wsprintf(wszAddrString, L"%s:%d", pwszLocalIPAddr, nPort);

  // Resolve IP address and fill SOCKADDR struct
  m_nLen = sizeof(SOCKADDR_IN);
  m_nRetValue = ::WSAStringToAddress(wszAddrString, AF_INET, NULL, (SOCKADDR*)&m_sockAddr, &m_nLen);
  if (m_nRetValue == SOCKET_ERROR)
  {
    m_lastError=ERR_INVALID_IP_ADDR;
    return FALSE;
  }
  
  // Create socket
  if ( (m_socket = ::WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED)) == INVALID_SOCKET)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET;
    return FALSE;
  }

  //
  // Set socket options
  //

  // SO_KEEPALIVE
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_KEEPALIVE, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // SO_REUSEADDR
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // SO_LINGER
  m_linger.l_onoff = 1;
  m_linger.l_linger = 0;
  m_nLen=sizeof(LINGER);
  if ( (::setsockopt(m_socket, SOL_SOCKET, SO_LINGER, (char*)&m_linger, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  // TCP_NODELAY
  m_bOption = 1;
  m_nLen=sizeof(BOOL);
  if ( (::setsockopt(m_socket, IPPROTO_UDP, TCP_NODELAY, (char*)&m_bOption, m_nLen)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_SOCKET_OPTION;
    return FALSE;
  }

  //
  // DeSelect network events
  //
  if ( (::WSAEventSelect(m_socket, m_hNetEvent, 0)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_EVENT_SELECT;
    return FALSE;
  }

/* -------------------  SERVER  ----------------------- */

  //
  // Bind socket
  //
  m_nLen=sizeof(SOCKADDR_IN);
  if ( (::bind(m_socket, (SOCKADDR*)&m_sockAddr, m_nLen )) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_BIND_SOCKET;
    return FALSE;
  }

  //
  // Select network events
  //
  if ( (::WSAEventSelect(m_socket, m_hNetEvent, FD_ACCEPT)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_WSA_EVENT_SELECT;
    return FALSE;
  }

  m_bIsObjectOk=TRUE;
  m_lastError=ERR_NO_ERROR;
  return TRUE;
} // end COverlappedSocketUDP::Create(


//@//////////////////////////////////////////////////////////////////////
//@
//@ Opens the link
//@
//@ Parameters:
//@		[IN]             hAbortEv  : abort event handler
//@		[IN]             dwTimeout : timeout value
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_EWOULDBLOCK
//@  - ERR_NOTHANDLED
//@  - ERR_TIMEOUT
//@  - ERR_WSA_EVENT_SELECT
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketUDP::OpenLink(HANDLE hAbortEv, DWORD dwTimeout )
{
enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_ACCEPT,

  NUM_EVENTS
};
BOOL        bRetVal=FALSE;
HANDLE      hEvents[NUM_EVENTS];


  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  //
  // Listen, just ONE client
  //
  if ( (::listen(m_socket, 1)) == SOCKET_ERROR)
  {
	  m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_LISTEN_SOCKET;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_ACCEPT]= m_hNetEvent;

  //
  // ACCEPT
  //
  // ::WSAResetEvent(m_hNetEvent);
  switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, dwTimeout/*WSA_INFINITE*/, FALSE))
  {
	  // User Abort event
    case ID_EV_ABORT:		
      m_lastError=ERR_USER_ABORT;
      bRetVal = FALSE;
	  break;

	  // Network event (FD_ACCEPT)
    case ID_EV_ACCEPT:	
      m_nLen = sizeof(SOCKADDR_IN);
		  m_socketIn = ::WSAAccept(m_socket, (SOCKADDR*)&m_sockAddrIn, &m_nLen, NULL, 0);
		  if (m_socketIn == INVALID_SOCKET)
		  {
			  m_nWSAError = ::WSAGetLastError();
			  switch (m_nWSAError)
			  {
				  // No connection detected
          case WSAEWOULDBLOCK:	
					  //::WSAResetEvent(m_hNetEvent);
            m_lastError=ERR_EWOULDBLOCK;
            bRetVal = FALSE;
				  break;

				  // Some error
          default:				
            m_lastError=ERR_NOTHANDLED;
            bRetVal = FALSE;
				  break;
			  }
		  }
		  
      // Connection detected
      else	
		  {
        bRetVal = TRUE;
		  }

	  break;

	  case WSA_WAIT_TIMEOUT:		
      m_lastError=ERR_TIMEOUT;
      bRetVal = FALSE;
	  break;

	  case WSA_WAIT_FAILED:		
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;

	  default:					
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;
  }


  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;

    // Select network events
    ::WSAResetEvent(m_hNetEvent);
    if ( (::WSAEventSelect(m_socketIn, m_hNetEvent, FD_READ | FD_CLOSE)) == SOCKET_ERROR)
    {
	    m_nWSAError = ::WSAGetLastError();
      m_lastError=ERR_WSA_EVENT_SELECT;
      bRetVal = FALSE;
    }
  }
  
  return bRetVal;
} // end COverlappedSocketUDP::OpenLink(

//@//////////////////////////////////////////////////////////////////////
//@
//@ Reads a READY message from the link
//@
//@ Parameters:
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarRead : number of bytes read
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_ENUM_NET_EV
//@  - ERR_REMOTE_CLOSE
//@  - ERR_SOCKET_READ
//@  - ERR_NOTHANDLED
//@  - ERR_NO_DATA
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketUDP::ReadMessage(WSABUF* pwsaBuffer, DWORD* pdwCarRead )
{
enum tagEvents
{
  ID_EV_NET,

  NUM_EVENTS
};

WSANETWORKEVENTS wsaNetworkEvents;		
BOOL             bRetVal=FALSE;
DWORD            dwFlags;
HANDLE           hEvents[NUM_EVENTS];

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

	if ( (::WSAEnumNetworkEvents(m_socketIn, m_hNetEvent, &wsaNetworkEvents)) == SOCKET_ERROR)
	{
		m_nWSAError = ::WSAGetLastError();
    m_lastError=ERR_ENUM_NET_EV;
    bRetVal = FALSE;
	}
	// Manage events
  else	
	{
    if(wsaNetworkEvents.lNetworkEvents == 0)
    {
      m_lastError=ERR_NO_DATA;
      bRetVal = FALSE;
    }

		if (wsaNetworkEvents.lNetworkEvents & FD_CLOSE)
		{
      m_lastError=ERR_REMOTE_CLOSE;
      bRetVal = FALSE;
		} 
		else if (wsaNetworkEvents.lNetworkEvents & FD_READ)
		{
			// If read error
			if (wsaNetworkEvents.iErrorCode[FD_READ_BIT] != 0)
			{
        m_lastError=ERR_SOCKET_READ;
        bRetVal = FALSE;
			}
			else
			{

        dwFlags = 0;
        if ( (::WSARecv(m_socketIn, pwsaBuffer, 1, pdwCarRead, &dwFlags, &m_ov, NULL)) == SOCKET_ERROR)
        {
	        m_nWSAError = ::WSAGetLastError();
	        switch (m_nWSAError)
	        {
		        // Operation in progress
            case WSA_IO_PENDING:	
              hEvents[ID_EV_NET]= m_ov.hEvent;
              switch (::WSAWaitForMultipleEvents(1, hEvents, FALSE, WSA_INFINITE, FALSE))
              {
	              // Overlapped event
                case ID_EV_NET:		
		              // Some error ?
                  if (::WSAGetOverlappedResult(m_socketIn, &m_ov, pdwCarRead, FALSE, &dwFlags) != TRUE)
		              {
                    m_nWSAError = ::WSAGetLastError();
                    m_lastError=ERR_SOCKET_READ;
                    bRetVal = FALSE;
		              }

		              // Read ok
                  else	
		              {
                    bRetVal = TRUE;
		              }
		              break;

	              case WSA_WAIT_FAILED:		// Some error
                  m_lastError=ERR_SOCKET_READ;
                  bRetVal = FALSE;
	              break;
              }
            break;

            default:	
              m_lastError=ERR_SOCKET_READ;
              bRetVal = FALSE;
		        break;
	        }
        }
        // Overlapped operation completed immediately
        else	
        {
          bRetVal=TRUE;
        }
			}
		} // end else if READ flag
    else if(wsaNetworkEvents.lNetworkEvents != 0)
    {
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
    }

	} // end else manage events

  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;
} // end BOOL COverlappedSocketUDP::ReadMessage(


//@//////////////////////////////////////////////////////////////////////
//@
//@ Receives a message from the link
//@
//@ Parameters:
//@		[IN]             hAbortEv   : abort event handler
//@		[IN]             dwTimeout  : timeout value
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarRead : number of bytes read
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_ENUM_NET_EV
//@  - ERR_REMOTE_CLOSE
//@  - ERR_SOCKET_READ
//@  - ERR_NOTHANDLED
//@  - ERR_TIMEOUT
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketUDP::ReceiveMessage(HANDLE hAbortEv, DWORD dwTimeout,
                            WSABUF* pwsaBuffer, DWORD* pdwCarRead )
{
enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_NET,

  NUM_EVENTS
};

WSANETWORKEVENTS wsaNetworkEvents;		
BOOL             bRetVal=FALSE;
HANDLE           hEvents[NUM_EVENTS];
DWORD            dwFlags;

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_NET]= m_hNetEvent;

  switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, dwTimeout/*WSA_INFINITE*/, FALSE))
  {
	  // User Abort event
    case ID_EV_ABORT:		
      m_lastError=ERR_USER_ABORT;
      bRetVal = FALSE;
	  break;

	  // Network event (FD_READ | FD_CLOSE)
    case ID_EV_NET:	
		  if ( (::WSAEnumNetworkEvents(m_socketIn, m_hNetEvent, &wsaNetworkEvents)) == SOCKET_ERROR)
		  {
			  m_nWSAError = ::WSAGetLastError();
        m_lastError=ERR_ENUM_NET_EV;
        bRetVal = FALSE;
		  }
		  // Manage events
      else	
		  {
			  if (wsaNetworkEvents.lNetworkEvents & FD_CLOSE)
			  {
          m_lastError=ERR_REMOTE_CLOSE;
          bRetVal = FALSE;
			  } 

			  else if (wsaNetworkEvents.lNetworkEvents & FD_READ)
			  {
				  // If read error
				  if (wsaNetworkEvents.iErrorCode[FD_READ_BIT] != 0)
				  {
            m_lastError=ERR_SOCKET_READ;
            bRetVal = FALSE;
				  }
				  else
				  {

            dwFlags = 0;
            if ( (::WSARecv(m_socketIn, pwsaBuffer, 1, pdwCarRead, &dwFlags, &m_ov, NULL)) == SOCKET_ERROR)
            {
	            m_nWSAError = ::WSAGetLastError();
	            switch (m_nWSAError)
	            {
		            // Operation in progress
                case WSA_IO_PENDING:	
                  hEvents[ID_EV_NET]= m_ov.hEvent;
                  switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, WSA_INFINITE, FALSE))
                  {
	                  // Exit event
                    case ID_EV_ABORT:	
                      m_lastError=ERR_USER_ABORT;
                      bRetVal = FALSE;
	                  break;

	                  // Overlapped event
                    case ID_EV_NET:		
		                  // Some error ?
                      if (::WSAGetOverlappedResult(m_socketIn, &m_ov, pdwCarRead, FALSE, &dwFlags) != TRUE)
		                  {
                        m_nWSAError = ::WSAGetLastError();
                        m_lastError=ERR_SOCKET_READ;
                        bRetVal = FALSE;
		                  }

		                  // Read ok
                      else	
		                  {
                        bRetVal = TRUE;
		                  }
		                  break;

	                  case WSA_WAIT_FAILED:		// Some error
                      m_lastError=ERR_SOCKET_READ;
                      bRetVal = FALSE;
	                  break;
                  }
                break;

                default:	
                  m_lastError=ERR_SOCKET_READ;
                  bRetVal = FALSE;
		            break;
	            }
            }
            // Overlapped operation completed immediately
            else	
            {
              bRetVal=TRUE;
            }
				  }
			  } // end else if READ flag

        else
        {
          m_lastError=ERR_NOTHANDLED;
          bRetVal = FALSE;
        }

		  } // end else manage events

    break;

	  case WSA_WAIT_TIMEOUT:		
      m_lastError=ERR_TIMEOUT;
      bRetVal = FALSE;
	  break;

	  case WSA_WAIT_FAILED:		
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;

	  default:					
      m_lastError=ERR_NOTHANDLED;
      bRetVal = FALSE;
	  break;

  } // end switch (::WSAWaitForMultipleEvents(


  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;
} // end BOOL COverlappedSocketUDP::ReceiveMessage(

//@//////////////////////////////////////////////////////////////////////
//@
//@ Writes a message to the link
//@
//@ Parameters:
//@		[IN]             hAbortEv   : abort event handler
//@		[IN]             dwTimeout  : timeout value
//@		[IN]             pwsaBuffer : pointer to len and destination buffer
//@		[IN,OUT]         pdwCarWritten : number of bytes written
//@				
//@ Return value:
//@    TRUE if OK
//@
//@ Note:
//@  Error codes :
//@  - ERR_OBJECT_NOK
//@  - ERR_USER_ABORT
//@  - ERR_SOCKET_WRITE
//@  - ERR_NOTHANDLED  
//@
//@////////////////////////////////////////////////////////////////////
BOOL COverlappedSocketUDP::WriteMessage(HANDLE hAbortEv, DWORD dwTimeout,
                            WSABUF* pwsaBuffer, DWORD* pdwCarWritten )
{

enum tagEvents
{
  ID_EV_ABORT=0,
  ID_EV_NET,

  NUM_EVENTS
};

BOOL   bRetVal=FALSE;
HANDLE hEvents[NUM_EVENTS];
DWORD  dwFlags;

  if( m_bIsObjectOk != TRUE)
  {
    m_lastError=ERR_OBJECT_NOK;
    return FALSE;
  }

  hEvents[ID_EV_ABORT]= hAbortEv;
  hEvents[ID_EV_NET]= m_ov.hEvent;

  if (::WSASend(m_socketIn, pwsaBuffer, 1, pdwCarWritten, 0, &m_ov, NULL) == SOCKET_ERROR)
  {
	  int nRetErr=::WSAGetLastError();
    switch (nRetErr)
	  {
		  // Operation in progress
      case WSA_IO_PENDING:	
        switch (::WSAWaitForMultipleEvents(2, hEvents, FALSE, WSA_INFINITE, FALSE))
        {
	        // Exit event
          case ID_EV_ABORT:	
            m_lastError=ERR_USER_ABORT;
            bRetVal = FALSE;
	        break;

	        // Overlapped event
          case ID_EV_NET:		
		        // Some error ?
            if (::WSAGetOverlappedResult(m_socketIn, &m_ov, pdwCarWritten, FALSE, &dwFlags) != TRUE)
		        {
              m_nWSAError = ::WSAGetLastError();
              m_lastError=ERR_SOCKET_WRITE;
              bRetVal = FALSE;
		        }

		        // Write ok
            else	
		        {
              bRetVal = TRUE;
		        }
	        break;

          case WSA_WAIT_FAILED:		// Some error
            m_nWSAError = ::WSAGetLastError();
            m_lastError=ERR_SOCKET_WRITE;
            bRetVal = FALSE;
          break;
        } // end switch (::WSAWaitForMultipleEvents(
      break;

		  case WSAEWOULDBLOCK:
        m_lastError=ERR_SOCKET_WRITE;
        bRetVal = FALSE;
		  break;

		  default:	
        m_lastError=ERR_NOTHANDLED;
        bRetVal = FALSE;
		  break;

	  }
  } // end if (::WSASend(
  
  // Overlapped operation completed immediately
  else	
  {
    bRetVal = TRUE;
  }


  if( bRetVal == TRUE)
  {
    m_lastError=ERR_NO_ERROR;
  }

  return bRetVal;

} // end BOOL COverlappedSocketUDP::WriteMessage(



