
// GSS_COMMLAYERDlg.h : header file
//

#pragma once
#include "COLORSTATIC.h"


// CGSS_COMMLAYERDlg dialog
class CGSS_COMMLAYERDlg : public CDialogEx
{
// Construction
public:
	CGSS_COMMLAYERDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_GSS_COMMLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	CColorStatic _ctlStaticGSS1;
	CColorStatic _ctlStaticGSS2;
	CColorStatic _ctlStaticCHI;
	CColorStatic _ctlStaticApp;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
protected:
	afx_msg LRESULT OnRefreshLinks(WPARAM wParam, LPARAM lParam);


public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
