//@////////////////////////////////////////////////////////////
//@
//@	Class:		CNetLink
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@
//@	Created:	19 February 2001
//@
//@	Author:		Giuseppe Alessio
//@
//@	Updated:
//@ 28 January 2005
//@ Support to BaseLib
//@
//@ February 23, 2005
//@ - added retry support for plc messages
//@
//@ March 24, 2005
//@ - Changes for external queue management 
//@
//@ August 4, 2005
//@ - Added extra queue "m_queueLamp"
//@
//@ May 4, 2007
//@ - Added <BuildCMLMessage> and <OnTelegramReceived> to handle fragmented packets
//@
//@
//@////////////////////////////////////////////////////////////

#ifndef _NETLINKIMPL_H
#define _NETLINKIMPL_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "QueueLib.h"
//#include "COMutility.h"
#include "CMLDefines.h"
#include "VMEDefines.h"


struct STRUCT_NETLINK_DEBUG_INFO
{
	wchar_t wszObjectName[MAX_PATH+1];
  int    nStatus;
	wchar_t  wszIPAddr[MAX_PATH+1];
	int    nPortID;
	int    nLocalNodeID;
	int    nRemoteNodeID;
	BYTE   byProgTx;
  BYTE   byProgRx;
  int    nNumTx;
  int    nNumRx;
  int    nNumTXerror;
  int    nNumRXerror;
  int    nNumTimeout;
  int    nNumPackets;
  int    nErrInvalidSom;
  int    nErrInvalidLength;
  int    nErrInvalidDestNode;
  int    nErrInvalidProg;
  int    nErrInvalidCks;
  int    nErrInvalidEom;
  int    nQueueSize;
	int    nQueueLen;
	int    nQueuePeak;
  int    nQueueLampSize;
	int    nQueueLampLen;
	int    nQueueLampPeak;

	STRUCT_NETLINK_DEBUG_INFO() { ::ZeroMemory(this, sizeof(STRUCT_NETLINK_DEBUG_INFO)); }
};

struct STRUCT_NETLINK_INIT
{
	int nInstanceID;
	wchar_t wszObjectName[MAX_PATH+1];
	int nClosingtimeout;
	int nThreadPriority;
	//
	BYTE bySourceNode;
	BYTE byDestNode;
	wchar_t wszIPAddr[MAX_PATH+1];
	int nPortID;
	int nPortRange;
	int nQueueSendRecordSize;
	int nQueueSendRecords;
  wchar_t wszQueueSendName[MAX_PATH+1];
	int nQueueLampRecordSize;
	int nQueueLampRecords;
  wchar_t wszQueueLampName[MAX_PATH+1];
	//
	BOOL bAsync;
	BOOL bServer;
	BOOL bMaster;
	//
	long lPollingTime;
	long lRecvTimeout;

	STRUCT_NETLINK_INIT() { ::ZeroMemory(this, sizeof(STRUCT_NETLINK_INIT)); }
};

class CNetLinkImpl
{
public:
	CNetLinkImpl(STRUCT_NETLINK_INIT& rcsInit);
	virtual ~CNetLinkImpl();
	inline BOOL BuildVMEMessage();
	inline BOOL CheckVMEProtocol(BYTE* pbyMsg, DWORD dwRxBytes);
	inline BOOL BuildPLCMessage();
	inline BOOL CheckPLCProtocol(BYTE* pbyMsg, DWORD dwRxBytes);
	//
	inline BOOL BuildCMLMessage();
	inline BOOL CheckCMLProtocol(BYTE* pbyMsg);

  CQueue m_queueSend;
  CQueue m_queueLamp;

protected:
	virtual BOOL OnCreate()           { return TRUE;}
	virtual BOOL OnCreateError()			{ return TRUE;}
	virtual BOOL OnMemoryOverflow()		{ return TRUE;}
	virtual BOOL OnOpenFailed()				{ return TRUE;}
	virtual BOOL OnRxTimeout()				{ return TRUE;}
	virtual BOOL OnRxEmpty()					{ return TRUE;}
	virtual BOOL OnRxError()					{ return TRUE;}
	virtual BOOL OnTxTimeout()				{ return TRUE;}
	virtual BOOL OnTxError()					{ return TRUE;}
	virtual BOOL OnShutdown()					{ return TRUE;}
	//
	virtual BOOL OnSendBeginKA()=0;
	virtual BOOL OnSendBegin()=0;
	virtual BOOL OnSendEnd()=0;
	virtual BOOL OnReceiveBegin()=0;
	virtual BOOL OnReceiveEnd()=0;
	virtual BOOL OnConnected()=0;
	virtual BOOL OnConnectionBroken()=0;
	virtual BOOL OnTelegramReceived()=0;

protected:
	STRUCT_NETLINK_DEBUG_INFO m_LDI;

	const int   SIZE_BUF_TX;
	const int   SIZE_BUF_RX;
	const BYTE  SOM_CH;
	const BYTE  EOM_CH;

	DWORD m_dwWriteBytes; 
	DWORD m_dwReadBytes; 
	WSABUF m_wsaTxData;
	WSABUF m_wsaRxData;
	BYTE m_byProgTx;
	BYTE m_byProgRx;
	BYTE 	m_bySourceNode;
	BYTE 	m_byDestNode;
	//
	BYTE* m_pbyRxMsg;
	int m_nRxMsgCnt;
	int m_nRxMsgLen;
	int m_nRxStep;
	//
	int m_nInstanceID;
	wchar_t m_wszObjectName[MAX_PATH+1];
	char    m_szObjectName[MAX_PATH+1];
  wchar_t m_wszIPAddr[MAX_PATH+1];
  int m_nPortID;
  int m_nPortRange;
	int m_nCurrentPortID;
	BOOL m_bAsync;
	BOOL m_bServer;
	BOOL m_bMaster;
	long m_lPollingTime;
	long m_lRecvTimeout;
	BOOL m_bRetry;

};

inline BOOL CNetLinkImpl::BuildVMEMessage()
{
	m_LDI.nNumPackets++;

	for(DWORD dwCnt=0; dwCnt < m_dwReadBytes; dwCnt++)
	{
		// force restart if buffer overflow
		if(m_nRxMsgCnt >= SIZE_BUF_RX)
			m_nRxStep=0;
		
		switch(m_nRxStep)
		{
			// start message
			case 0:
				
				if((BYTE)m_wsaRxData.buf[dwCnt] != SOM_CH)
					break;
				//
				::ZeroMemory(m_pbyRxMsg, SIZE_BUF_RX);
				m_nRxMsgCnt=0;
				//
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				//
				m_nRxMsgCnt++;
				m_nRxStep = 1;
			break;

			// message length
			case 1:
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				// bytes left to end of message
				m_nRxMsgLen = m_pbyRxMsg[m_nRxMsgCnt]+sizeof(STRUCT_TAILCMLVME);
				//
				m_nRxMsgCnt++;
				m_nRxStep = 2;
			break;

			// message
			case 2:
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				m_nRxMsgLen --;
				m_nRxMsgCnt++;
				// check end of message
				if( m_nRxMsgLen <= 0)
				{
					if((BYTE)m_pbyRxMsg[m_nRxMsgCnt-1] == EOM_CH)
					{
						OnTelegramReceived();
					}
					else
					{
						m_LDI.nErrInvalidLength++;
					}
					//
					m_nRxStep = 0;
				}
			break;
		} // end switch

	} // end for

	return TRUE;
}

//@ CheckVMEProtocol
inline BOOL CNetLinkImpl::CheckVMEProtocol(BYTE* pbyMsg, DWORD dwRxBytes)
{
  // check SOM
  if(pbyMsg[0] != SOM_CH)
  {
		m_LDI.nErrInvalidSom++;
		return FALSE;
  }

  // check EOM
  if(pbyMsg[dwRxBytes-1] != EOM_CH)
  {
		m_LDI.nErrInvalidEom++;
		return FALSE;
  }

  // check length
  if(pbyMsg[1] != dwRxBytes-4) // SOM, LUNG, CKS and EOM not included
  {
    m_LDI.nErrInvalidLength++;
		return FALSE;
  }

  // check Destination node
  if(pbyMsg[2] != m_bySourceNode)
  {
		m_LDI.nErrInvalidDestNode++;
		return FALSE;
  }

  m_LDI.byProgRx=pbyMsg[4];

  // check sequence number
  if(m_bMaster == TRUE)
	{
		if( pbyMsg[4] != m_byProgTx)
		{
			//
			m_LDI.nErrInvalidProg++;
			return FALSE;
		}
	}
	else
	{
		if( pbyMsg[4] == m_byProgRx  && pbyMsg[4] != 1)
		{
			m_LDI.nErrInvalidProg++;
			return FALSE;
		}
	}

	m_byProgRx = pbyMsg[4];

  // calculate checksum
  BYTE byCks=0;
	for(int i=0; i<pbyMsg[1]; i++)
		byCks ^= pbyMsg[i+2];

	// as per spec
	if (byCks == 0xFE)
		byCks = 0x00;

  // check checksum
  if( pbyMsg[dwRxBytes-2] != byCks)
  {
    m_LDI.nErrInvalidCks++;
		return FALSE;
  }

  return TRUE;
}

inline BOOL CNetLinkImpl::BuildPLCMessage()
{
	m_LDI.nNumPackets++;

	for(DWORD dwCnt=0; dwCnt < m_dwReadBytes; dwCnt++)
	{
		// force restart if buffer overflow
		if(m_nRxMsgCnt >= SIZE_BUF_RX)
			m_nRxStep=0;
		
		switch(m_nRxStep)
		{
			// start message
			case 0:
				
				if((BYTE)m_wsaRxData.buf[dwCnt] != SOM_CH)
					break;
				//
				::ZeroMemory(m_pbyRxMsg, SIZE_BUF_RX);
				m_nRxMsgCnt=0;
				//
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				//
				m_nRxMsgCnt++;
				m_nRxStep = 1;
			break;

			// message length
			case 1:
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				// bytes left to end of message
				m_nRxMsgLen = m_pbyRxMsg[m_nRxMsgCnt]+sizeof(STRUCT_TAILCMLVME);
				//
				m_nRxMsgCnt++;
				m_nRxStep = 2;
			break;

			// message
			case 2:
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				m_nRxMsgLen --;
				m_nRxMsgCnt++;
				// check end of message
				if( m_nRxMsgLen <= 0)
				{
					if((BYTE)m_pbyRxMsg[m_nRxMsgCnt-1] == EOM_CH)
					{
						OnTelegramReceived();
					}
					else
					{
						m_LDI.nErrInvalidLength++;
					}
					//
					m_nRxStep = 0;
				}
			break;
		} // end switch

	} // end for

	return TRUE;
}

//@ CheckPLCProtocol
inline BOOL CNetLinkImpl::CheckPLCProtocol(BYTE* pbyMsg, DWORD dwRxBytes)
{
  // check SOM
  if(pbyMsg[0] != SOM_CH)
  {
		m_LDI.nErrInvalidSom++;
		return FALSE;
  }

  // check EOM
  if(pbyMsg[dwRxBytes-1] != EOM_CH)
  {
		m_LDI.nErrInvalidEom++;
		return FALSE;
  }

  // check length
  if(pbyMsg[1] != dwRxBytes-4) // SOM, LUNG, CKS and EOM not included
  {
    m_LDI.nErrInvalidLength++;
		return FALSE;
  }

  // check Destination node
  if(pbyMsg[2] != m_bySourceNode)
  {
		m_LDI.nErrInvalidDestNode++;
		return FALSE;
  }

  m_LDI.byProgRx=pbyMsg[4];

  // check sequence number
  if(m_bMaster == TRUE)
	{
		if( pbyMsg[4] != m_byProgTx)
		{
			//
			m_LDI.nErrInvalidProg++;
			return FALSE;
		}
	}
	else
	{
		if(pbyMsg[4] != 1)
		{
			if( pbyMsg[4] == m_byProgRx)
			{
				m_bRetry = TRUE;
			}
			else
			{
				m_bRetry = FALSE;
			}
		}
	}

	m_byProgRx = pbyMsg[4];

  // calculate checksum
  BYTE byCks=0;
	for(int i=0; i<pbyMsg[1]; i++)
		byCks ^= pbyMsg[i+2];

	// as per spec
	if (byCks == 0xFE)
		byCks = 0x00;

  // check checksum
  if( pbyMsg[dwRxBytes-2] != byCks)
  {
    m_LDI.nErrInvalidCks++;
		return FALSE;
  }

  return TRUE;
}

inline BOOL CNetLinkImpl::BuildCMLMessage()
{
	m_LDI.nNumPackets++;

	for(DWORD dwCnt=0; dwCnt < m_dwReadBytes; dwCnt++)
	{
		// force restart if buffer overflow
		if(m_nRxMsgCnt >= SIZE_BUF_RX)
			m_nRxStep=0;
		
		switch(m_nRxStep)
		{
			// start message
			case 0:
				
				if((BYTE)m_wsaRxData.buf[dwCnt] != SOM_CH)
					break;
				//
				::ZeroMemory(m_pbyRxMsg, SIZE_BUF_RX);
				m_nRxMsgCnt=0;
				//
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				//
				m_nRxMsgCnt++;
				m_nRxStep = 1;
			break;

			// build envelope and get telegram length
			case 1:
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				m_nRxMsgCnt++;
				//
				if(m_nRxMsgCnt == sizeof(STRUCT_ENVELOPECML))
				{
					STRUCT_ENVELOPECML* pEnvelopeCML=(STRUCT_ENVELOPECML*)m_pbyRxMsg;
					// bytes left to end of message
					m_nRxMsgLen = (pEnvelopeCML->wLen + sizeof(STRUCT_TAILCML));
					//
					m_nRxStep = 2;
				}
			break;

			// message
			case 2:
				m_pbyRxMsg[m_nRxMsgCnt] = m_wsaRxData.buf[dwCnt];
				m_nRxMsgLen --;
				m_nRxMsgCnt++;
				// check end of message
				if( m_nRxMsgLen <= 0)
				{
					if((BYTE)m_pbyRxMsg[m_nRxMsgCnt-1] == EOM_CH)
					{
						OnTelegramReceived();
					}
					else
					{
						m_LDI.nErrInvalidLength++;
					}
					//
					m_nRxStep = 0;
				}
			break;
		} // end switch

	} // end for

	return TRUE;
}


//@ CheckCMLProtocol
inline BOOL CNetLinkImpl::CheckCMLProtocol(BYTE* pbyMsg)
{
	try
	{
		// check SOM
		if(pbyMsg[0] != SOM_CH)
		{
			m_LDI.nErrInvalidSom++;
			return FALSE;
		}

		STRUCT_ENVELOPECML* pEnvelopeCML=(STRUCT_ENVELOPECML*)pbyMsg;

		// check EOM
		if(pbyMsg[sizeof(STRUCT_ENVELOPECML)+ pEnvelopeCML->wLen + sizeof(STRUCT_TAILCML)-1] != EOM_CH)
		{
			m_LDI.nErrInvalidEom++;
			return FALSE;
		}

		// check destination node
		if( pEnvelopeCML->byDestNode != m_bySourceNode)
		{
			m_LDI.nErrInvalidDestNode++;
			//return FALSE; ma chi se ne fotte di sti cazzo di Dest and Souce che servono solo a non far funzionare la comunicazione!!!!
			// ma poi chi se li � inventati e perch�, non basta un indirizzo IP e una porta di comunicazione per essere sicure di parlare con chi
			// si trova dall'altra parte allo stesso indirizzo e porta????? ma allora perch� non aggiungere anche il nome e il cognome e il numero di passaporto
			// cos� la facciamo complicata ancora un po'???
		}

		// check sequence number
		if(m_bMaster == TRUE)
		{
			// check sequence number
			if( pEnvelopeCML->byProg == m_byProgRx
			&&  pEnvelopeCML->byProg != 0)
			{
				m_LDI.nErrInvalidProg++;
				return FALSE;
			}
		}
		else
		{
			if( pEnvelopeCML->byProg == m_byProgRx)
			{
				m_LDI.nErrInvalidProg++;
				return FALSE;
			}
		}

		m_byProgRx = pEnvelopeCML->byProg;
	}
	catch(...)
	{
		m_LDI.nErrInvalidEom++;
		return FALSE;
	}

	return TRUE;
}

#endif 
