#include "AppSettings.h"
#include "ProtocolConverter_FX.h"
#include "AppTrace.h"
#include "ChIntNetLink.h"
#include "GSS_Link.h"
#include "LinksManager.h"


CWnd* G_pCommLayerWnd;

CAppSettings* G_pAppSettings = NULL;


CProtocolConverter_FX* G_pProtocolConverter_FX = NULL;

CAppTrace* G_pTrace = NULL;

CGSS_Link* G_pGSS_Links[NUM_MAX_GSS_LINK_CONNECTIONS];
CChIntNetLink * G_pChIntNetLink = NULL;

//CCoreAppHnd			G_CoreAppHnd;

CLinksManager* G_pLinksManager = NULL;
