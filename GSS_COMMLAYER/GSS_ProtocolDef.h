
#pragma once

//////////////////////////////////////////////////////////////
// BRIDGE PROTOCOL
//////////////////////////////////////////////////////////////

#define NUM_MAX_GSS_LINK_CONNECTIONS 20
#define NUM_MAX_GSS_SOCKET NUM_MAX_GSS_LINK_CONNECTIONS / 2
#define GSS_LINK_CHANNEL_GSS1 0
#define GSS_LINK_CHANNEL_GSS2 (GSS_LINK_CHANNEL_GSS1 + NUM_MAX_GSS_SOCKET)

#define MAX_BRIDGE_CHUTES     9

//
// PROTOCOL
//
//
// NO for FedEx

//
// MESSAGES
//
/* MSG ID for internal use
enum _ENUM_wMSGID_GSS
{
	_wMSGID_GSS_HB=0,
	_wMSGID_GSS_PL,
	_wMSGID_GSS_DL,
	_wMSGID_GSS_DA,
	_wMSGID_GSS_RA,
	_wMSGID_GSS_DV,
	_wMSGID_GSS_ES,
	_wMSGID_GSS_CD,
	_wMSGID_GSS_PS,
	_wMSGID_GSS_XR,
	_wMSGID_GSS_DW,
	_wMSGID_GSS_LI,
	_wMSGID_GSS_LO,
	MAX_wMSGID_GSS
};
*/


/*
static const char* const GSS_MSG_NAME[] = { "HB",
																						"PL",
																						"DL",
																						"DA",
																						"RA",
																						"DV",
																						"ES",
																						"CD",
																						"PS",
																						"XR",
																						"DW",
																						"LI",
																						"LO",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						"",
																						""};

																						*/

//
//
//
//#pragma pack(1)
#pragma pack()
//
struct STRUCT_GSS_LINK_INIT
{
	int      nThreadPriority;                   // thread priority
	wchar_t  wszObjectName[MAX_PATH+1];         // object name
	int      nInstanceID;                       // instance ID
	int      nClosingtimeout;                   // closing thread timeout (ms)
	wchar_t  wszIPAddr[MAX_PATH+1];        // local IP address
	int      nPortID;                           // channel port ID
	long     lLinkLossTimeout;                  // RX timeout 
  int      nMaxRetries;                       // transmission retries  
	int      nQueueSendRecordSize;              // queue send record size
	int      nQueueSendRecords;                 // queue send records
	wchar_t  wszQueueSendName[MAX_PATH+1];      // queue send name
	wchar_t  wszQueueSendFileName[MAX_PATH+1];  // queue send file name
	// custom data
	int		nFrameSize;
	int		nFrameSizeLen;
	char		szFrameSize[32];
	int		nGSS_ID;
	//
	STRUCT_GSS_LINK_INIT() { ::ZeroMemory(this, sizeof(STRUCT_GSS_LINK_INIT)); }
};

struct STRUCT_GSS_LINK_DEBUG_INFO
{
	wchar_t wszObjectName[MAX_PATH+1];
	wchar_t wszIPAddress[MAX_PATH+1];
	int    nPortID;
	long   lLinkLossTimeout;
	int    nMaxRetries;
  int    nNumTXRetries;
  int    nNumTx;
  int    nNumRx;
  int    nNumTXTelegrams;
  int    nNumRXTelegrams;
  int    nMaxTxGroupedTelegrams;
  int    nMaxRxGroupedTelegrams;
  int    nNumTXerror;
  int    nNumRXerror;
  int    nQueueSize;
	int    nQueueLen;
	int    nQueuePeak;
	int    nErrInvalidSOM;
	int    nTotalRxMsg;
	
	STRUCT_GSS_LINK_DEBUG_INFO() { ::ZeroMemory(this, sizeof(STRUCT_GSS_LINK_DEBUG_INFO)); }
};

#define GSS_LINK_QUEUE_DATA_LEN 512
struct STRUCT_GSS_LINK_QUEUE_RECORD
{
  WORD wMSG_ID;
  WORD wMsgLen;
	char szMsgData[GSS_LINK_QUEUE_DATA_LEN + 1];
	//
	STRUCT_GSS_LINK_QUEUE_RECORD() { ::ZeroMemory(this, sizeof(STRUCT_GSS_LINK_QUEUE_RECORD)); }
};

/*
struct STRUCT_DEST_ASSIGNMENT_DATA
{
  char szPackageID[SIZEOF_PARCEL_INDEX];
  WORD wCellId;
  WORD dwChuteId[5];
	//
	STRUCT_DEST_ASSIGNMENT_DATA() { ::ZeroMemory(this, sizeof(STRUCT_DEST_ASSIGNMENT_DATA)); }
};
*/
/*
struct STRUCT_DEST_REQ_DATA
{
  char szIndex[SIZEOF_PARCEL_INDEX];
  int nZoneID;
  int nCell1;
  int nCell2;
  int nToRejectRequest;
  int nParcelFound;
	//
	STRUCT_DEST_REQ_DATA() { ::ZeroMemory(this, sizeof(STRUCT_DEST_REQ_DATA)); }
};

struct STRUCT_DEST_RES_DATA
{
  char szIndex[SIZEOF_PARCEL_INDEX];
  int nZoneID;
  char szParcelID[MAX_PATH+1];
  int nCell;
  int nRescan;
  int nPark;
  int nNumChutes;
  int nChutes[MAX_BRIDGE_CHUTES];
	//
	STRUCT_DEST_RES_DATA() { ::ZeroMemory(this, sizeof(STRUCT_DEST_RES_DATA)); }
};

struct STRUCT_CHUTE_INHIBIT_DATA
{
  int nChuteID;
  int nStatus;
	//
	STRUCT_CHUTE_INHIBIT_DATA() { ::ZeroMemory(this, sizeof(STRUCT_CHUTE_INHIBIT_DATA)); }
};
*/