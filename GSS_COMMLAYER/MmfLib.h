//@////////////////////////////////////////////////////////////
//@
//@	Class:		CMMFLib
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@
//@	Created:	12 April 1999
//@
//@	Author:		Davide Calabro'
//@
//@	Updated:	
//@ 10 January 2000
//@ 
//@ 
//@ 13 July 2012
//@ 	Modified method Connect
//@ 
//@////////////////////////////////////////////////////////////

#ifndef _MMFLIB_H
#define _MMFLIB_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CMMFLib  
{
public:
  DWORD GetFileSize();
	void* Resize(DWORD dwSize, LPCTSTR lName);
	void* Attach(LPCTSTR lName);
	void  Detach();
	CMMFLib();
	virtual ~CMMFLib();

  void* Open(LPCTSTR fName, LPCTSTR lName, DWORD dwSize, BOOL bCreateAnyway, BOOL bResize);
	BOOL Flush();
	BOOL Close();
	BOOL Close(DWORD dwSize);

	BOOL IsMapped();
	BOOL FileExists();

	DWORD GetLastError();
	
	static const short GetVersionI();
	static const char* GetVersionC();

private:
	void* MapView(HANDLE hFile);
	BOOL InitializeFile(HANDLE hHandle, DWORD dwSize);
	void Shutdown();

	BOOL m_bFileExists;
	BOOL m_bFileOpen;
	BOOL m_bAttached;

	HANDLE m_hFile;	// HANDLE restituito da CreateFile
	HANDLE m_hMap;	// HANLDE restituito da CreateFileMapping
	void* m_pMapFile;

	DWORD m_dwLastError;
};

#endif
