#pragma once
#include "AppTrace.h"


// CDlgTraceLevel dialog

class CDlgTraceLevel : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgTraceLevel)

public:
	CDlgTraceLevel(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgTraceLevel();

// Dialog Data
	enum { IDD = IDD_TRACE_LEVEL };

private:
	CAppTrace::STRUCT_SETTINGS m_csSettings;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck3();
	afx_msg void OnBnClickedCheck4();
	afx_msg void OnBnClickedCheck5();
	afx_msg void OnBnClickedCheck6();
	afx_msg void OnBnClickedCheck7();
	afx_msg void OnBnClickedCheck8();
	afx_msg void OnBnClickedCheck9();
	afx_msg void OnBnClickedCheck10();
	afx_msg void OnBnClickedCheck11();
	afx_msg void OnBnClickedCheck12();
	afx_msg void OnBnClickedCheck13();
	afx_msg void OnBnClickedCheck14();
	afx_msg void OnBnClickedCheck15();
	afx_msg void OnBnClickedOk();
};
