
// GSS_COMMLAYER.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CGSS_COMMLAYERApp:
// See GSS_COMMLAYER.cpp for the implementation of this class
//

class CGSS_COMMLAYERApp : public CWinApp
{
public:
	CGSS_COMMLAYERApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()

private:
	WSADATA m_wsaData;
	HANDLE m_hApplicationMutex;

};

extern CGSS_COMMLAYERApp theApp;