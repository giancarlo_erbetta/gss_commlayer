
#include "stdafx.h"
#include "threadlib.h"

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CThreadLib::CThreadLib
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CThreadLib::CThreadLib(wchar_t* _pwszObjectName, int _nClosingtimeout)
: m_nClosingTimeout(_nClosingtimeout) 
{
  ::wcsncpy_s(m_wszObjectName, MAX_PATH, _pwszObjectName, MAX_PATH);
	m_wszObjectName[MAX_PATH] = NULL;

  // not segnaled, manual reset
  m_hEvExitThread  = ::CreateEvent(NULL, TRUE, FALSE, NULL);
  m_hEvObjectReady = ::CreateEvent(NULL, TRUE, FALSE, NULL);

  m_bObjectOk = FALSE;
  if(m_hEvExitThread  == NULL || m_hEvObjectReady == NULL )
  {
		throw CFOLibException(0, L"CThreadLib: Init failed");
	}

  m_bObjectOk = TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Destructor CThreadLib::~CThreadLib
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CThreadLib::~CThreadLib()
{
  // Terminate the worker thread
  ::SetEvent(m_hEvExitThread);
  switch(::WaitForSingleObject(m_hThread, m_nClosingTimeout))
  {
    case WAIT_OBJECT_0:  break;
    default: 
      ErrorClosingThreadHandler();
    break;
  }
  
  ::CloseHandle(m_hEvExitThread);
  ::CloseHandle(m_hEvObjectReady);
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Run the target worker thread
//@
//@ Parameters:
//@		[IN]	    pThrFunc  : target thread entry point
//@		[IN]	    nPriority : thread priority
//@
//@ Return value:
//@   TRUE if ok
//@
//@ Note:
//@   if all right sets m_bObjectOk to TRUE
//@ 
//@/////////////////////////////////////////////////////////////////
BOOL CThreadLib::RunThread(LPTHREAD_START_ROUTINE pThrFunc, int nPriority)
{
  if(m_bObjectOk == FALSE) return FALSE;
    
  m_bObjectOk = FALSE;

  m_hThread = ::CreateThread(NULL, 0, pThrFunc, this, 0, &m_dwIdThread);
  if(m_hThread != NULL)
  {
    ::SetThreadPriority(m_hThread, nPriority);
    m_bObjectOk = TRUE;
  }
  return m_bObjectOk;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Stops the worker thread
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    TRUE if no errors on closing
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
BOOL CThreadLib::StopThread()
{
  // Terminate th worker thread
  ::SetEvent(m_hEvExitThread);
  switch(::WaitForSingleObject(m_hThread, m_nClosingTimeout))
  {
    case WAIT_OBJECT_0:  
      return TRUE;    
    break;
    default: 
      ErrorClosingThreadHandler();
      return FALSE;
    break;
  }
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Error handler for closing worker thread error 
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
void CThreadLib::ErrorClosingThreadHandler()
{
	#ifdef USE_FOLIBRARY_ENVIRONMENT
	_Module.LogEvent(EVENTLOG_ERROR_TYPE, L"Error closing [%s] thread", m_wszObjectName);
	#endif
}

