// Trace.cpp: implementation of the CAppTrace class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AppTrace.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAppTrace::CAppTrace(wchar_t* _pwszPathName, wchar_t* _pwszLogName, wchar_t* _pwszTracePath, int nQueueSize, wchar_t* _pwszObjectName, int _nClosingtimeout):
CTraceLib(_pwszPathName, _pwszLogName, _pwszTracePath, nQueueSize, _pwszObjectName, _nClosingtimeout)
{
}

CAppTrace::~CAppTrace()
{

}
