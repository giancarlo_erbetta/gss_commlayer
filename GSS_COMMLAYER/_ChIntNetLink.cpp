//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ChIntNetLink.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor
//@
CChIntNetLink::CChIntNetLink(STRUCT_NETLINK_INIT& rcsInit) :
CNetLink(rcsInit)
{
	m_LinkON=FALSE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Destructor
//@
CChIntNetLink::~CChIntNetLink()
{

}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Prepare the Keep Alive message to be sent
//@
BOOL CChIntNetLink::OnSendBeginKA()
{
	if(m_wsaTxData.buf == NULL)
	  return FALSE;

	BYTE* pbyData=NULL;
		
	::ZeroMemory(m_wsaTxData.buf, SIZE_BUF_TX);
	::ZeroMemory(&m_csSTRUCT_ENVELOPECML, sizeof(STRUCT_ENVELOPECML));
	::ZeroMemory(&m_csSTRUCT_TAILCML, sizeof(STRUCT_TAILCML));

	// 
	// ENVELOPE
	//
	m_csSTRUCT_ENVELOPECML.bySOM=SOM_CH;
	m_csSTRUCT_ENVELOPECML.byClass=1;
	m_csSTRUCT_ENVELOPECML.bySourceNode=m_bySourceNode;
	m_csSTRUCT_ENVELOPECML.byDestNode=m_byDestNode;
	m_csSTRUCT_ENVELOPECML.byProg=m_byProgTx;
	m_csSTRUCT_ENVELOPECML.wLen=0;
	m_csSTRUCT_ENVELOPECML.wNumMsg=0;

	// 
	// DATA
	//

  // no data

	// copy envelope
	memcpy(m_wsaTxData.buf, (BYTE*)&m_csSTRUCT_ENVELOPECML, sizeof(STRUCT_ENVELOPECML));

	//
	// TAIL
	//
  m_csSTRUCT_TAILCML.byEOM = EOM_CH;

	// copy tail
	memcpy(&m_wsaTxData.buf[sizeof(STRUCT_ENVELOPECML)+m_csSTRUCT_ENVELOPECML.wLen], (BYTE*)&m_csSTRUCT_TAILCML, sizeof(STRUCT_TAILCML));

  // update telegram length
	m_wsaTxData.len=sizeof(STRUCT_ENVELOPECML)+m_csSTRUCT_ENVELOPECML.wLen+sizeof(STRUCT_TAILCML);

  return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Prepare the message to be sent
//@
BOOL CChIntNetLink::OnSendBegin()
{
	if(m_wsaTxData.buf == NULL)
	  return FALSE;

	STRUCT_NETLINK_CHDAC_QUEUE_RECORD csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD;
	BYTE* pbyData=NULL;
		
	::ZeroMemory(m_wsaTxData.buf, SIZE_BUF_TX);
	::ZeroMemory(&m_csSTRUCT_ENVELOPECML, sizeof(STRUCT_ENVELOPECML));
	::ZeroMemory(&m_csSTRUCT_TAILCML, sizeof(STRUCT_TAILCML));

	// 
	// ENVELOPE
	//
	m_csSTRUCT_ENVELOPECML.bySOM=SOM_CH;
	m_csSTRUCT_ENVELOPECML.byClass=1;
	m_csSTRUCT_ENVELOPECML.bySourceNode=m_bySourceNode;
	m_csSTRUCT_ENVELOPECML.byDestNode=m_byDestNode;
	m_csSTRUCT_ENVELOPECML.byProg=m_byProgTx;
	m_csSTRUCT_ENVELOPECML.wLen=0;
	m_csSTRUCT_ENVELOPECML.wNumMsg=0;

	// 
	// DATI
	//

	pbyData=(BYTE*)&m_wsaTxData.buf[sizeof(STRUCT_ENVELOPECML)];

	// send all messages in queue
	while(m_queueSend.GetQueuelen())
	{
		// length of next message
		if( !m_queueSend.QueryNextItem((BYTE*)&csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD))
			break; 
		
		// can be held in the buffer? ( -10 to be sure )
		if(pbyData+csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD.wMsgLen > (BYTE*)&m_wsaTxData.buf[SIZE_BUF_TX-10])
			break; // stop for buffer end
		
		// pick next message
		if(!m_queueSend.GetItem((BYTE*)&csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD))
			break; 

		// cod msg
		*((WORD*)&pbyData[0]) = csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD.wMsgCode;
		// len msg
		*((WORD*)&pbyData[2]) = csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD.wMsgLen;
		// data msg
		memcpy(&pbyData[4],csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD.byMsgData,csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD.wMsgLen);

		// update length
		m_csSTRUCT_ENVELOPECML.wLen += csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD.wMsgLen+4;
		// update number of messages
		m_csSTRUCT_ENVELOPECML.wNumMsg +=1;

		// update buffer pointer
		pbyData += csSTRUCT_NETLINK_CHDAC_QUEUE_RECORD.wMsgLen+4;

  } // end while(m_queueSend.GetQueuelen())

	// copy envelope
	memcpy(m_wsaTxData.buf, (BYTE*)&m_csSTRUCT_ENVELOPECML, sizeof(STRUCT_ENVELOPECML));

	//
	// TAIL
	//
  m_csSTRUCT_TAILCML.byEOM = EOM_CH;

	// copy tail
	memcpy(&m_wsaTxData.buf[sizeof(STRUCT_ENVELOPECML)+m_csSTRUCT_ENVELOPECML.wLen], (BYTE*)&m_csSTRUCT_TAILCML, sizeof(STRUCT_TAILCML));

  // update telegram length
	m_wsaTxData.len=sizeof(STRUCT_ENVELOPECML)+m_csSTRUCT_ENVELOPECML.wLen+sizeof(STRUCT_TAILCML);

  return TRUE;
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ Message has been sent properly
BOOL CChIntNetLink::OnSendEnd()
{
	m_LDI.nNumTx++;
	m_LDI.byProgTx = m_byProgTx;

	// next sequence number
	m_byProgTx++;
	if(m_byProgTx == 0 )
		m_byProgTx=1;

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Prepare the receiving buffer
//@
BOOL CChIntNetLink::OnReceiveBegin()
{
	if(m_wsaRxData.buf == NULL)
	  return FALSE;

  m_wsaRxData.len=SIZE_BUF_RX;
  ::ZeroMemory(m_wsaRxData.buf, SIZE_BUF_RX);

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Packet has been received
//@
BOOL CChIntNetLink::OnReceiveEnd()
{
	// merge fragmented packets
	BuildCMLMessage();	

	return TRUE;
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ Telegram has been received
//@
BOOL CChIntNetLink::OnTelegramReceived()
{
	m_LDI.nNumRx++;

  BYTE* pbyMsg=NULL;
	BYTE* pbyData=NULL;

  pbyMsg = (BYTE*)m_pbyRxMsg;
  while( pbyMsg < (BYTE*)(&m_pbyRxMsg[m_nRxMsgCnt]))
  {

		if( CheckCMLProtocol(pbyMsg) == FALSE)
			return FALSE;

    // store sequence number
	  m_LDI.byProgRx = m_byProgRx;

	  //VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_0,__FILE__,__LINE__,0, "%s Prog RX %d", m_szObjectName, m_byProgRx);

		// copy envelope
		memcpy((BYTE*)&m_csSTRUCT_ENVELOPECML, pbyMsg, sizeof(STRUCT_ENVELOPECML));
	  //
	  // process received data
	  //
    pbyData=&pbyMsg[sizeof(STRUCT_ENVELOPECML)];
		//
    for(int i=0; i<m_csSTRUCT_ENVELOPECML.wNumMsg && pbyData < (pbyMsg+sizeof(STRUCT_ENVELOPECML)+m_csSTRUCT_ENVELOPECML.wLen); i++)
    {
      WORD wCodMsg = *((WORD*)&pbyData[0]);
      WORD wMsgLen = *((WORD*)&pbyData[2]);

	     switch(wCodMsg)
      {
        case wMSGID_DAC_MSG01_TRIGGER:
					// for Chronopost: DO NOTHING
				break;

				default:
          VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DAC||||%s Unhandled message %d received", m_szObjectName, wCodMsg);
				break;

      } // end switch

			// move to next message
			pbyData += wMsgLen+4;

    } // end for

    // continue with next telegram
    pbyMsg += sizeof(STRUCT_ENVELOPECML)+ m_csSTRUCT_ENVELOPECML.wLen + sizeof(STRUCT_TAILCML);

  } // end while

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Connected
//@
BOOL CChIntNetLink::OnConnected()
{
  m_byProgTx=1;
  m_byProgRx='?';
	m_LinkON=TRUE;

	VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_8_WARNINGS,__FILE__,__LINE__,0, "|DAC||||%s CONNECTED", m_szObjectName);
  return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Connection broken
//@
BOOL CChIntNetLink::OnConnectionBroken()
{
	if(	m_LinkON==TRUE)
	{
		m_LinkON=FALSE;

		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_8_WARNINGS,__FILE__,__LINE__,0, "|DAC||||%s CONNECTION DROPPED", m_szObjectName);
	}
  return TRUE;
}

