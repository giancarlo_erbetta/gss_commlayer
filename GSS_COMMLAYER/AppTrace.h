// Trace.h: interface for the CTrace class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRACE_H__E4DB8E72_52AD_4A35_9DA6_5E671CFC33D8__INCLUDED_)
#define AFX_TRACE_H__E4DB8E72_52AD_4A35_9DA6_5E671CFC33D8__INCLUDED_

#if _MSC_VER > 1000	
#pragma once
#endif // _MSC_VER > 1000

#include "TraceLib.h"

class CAppTrace : public CTraceLib  
{
public:
	CAppTrace(wchar_t* _pwszPathName, wchar_t* _pwszLogName, wchar_t* pwszTracePath, int nQueueSize, wchar_t* _pwszObjectName, int _nClosingtimeout=1000);
	virtual ~CAppTrace();

	inline STRUCT_SETTINGS GetData() { return (*m_pData);}
	inline void PutData(STRUCT_SETTINGS csSTRUCT_SETTINGS) { (*m_pData) = csSTRUCT_SETTINGS; }
};


extern CAppTrace* G_pTrace;

#endif // !defined(AFX_TRACE_H__E4DB8E72_52AD_4A35_9DA6_5E671CFC33D8__INCLUDED_)
