// AppSettings.cpp: implementation of the CAppSettings class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AppSettings.h"

#include "XmlDocument.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_SERIAL (CAppSettings, CObject, 1)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAppSettings::CAppSettings()
{
	try
	{
		InitializeCriticalSection(&m_cs_syncro);
	}
	catch(...)
	{
		//throw CFOLibException(0, L"CAppSettings: Critical Section creation failed");
	}

  SetDefault();
}

CAppSettings::~CAppSettings()
{
 	::DeleteCriticalSection(&m_cs_syncro);
}

void CAppSettings::SetDefault()
{
	m_nCommLayerID = 1;
	m_sConfigFileName = L"";
	m_sGSS_IPAddr[0] = L"127.0.0.1";
	m_sGSS_IPAddr[1] = L"127.0.0.1";
	m_sCHI_IPAddr = L"127.0.0.1";
	m_nGSSPort = 30404;
	m_nCHIPort = 50001;
	m_nGSSNumPorts = 1;
	m_sSorterEquipmentID = L"SS00001";

	m_nFrameSizeLen=4;
	m_nFrameSize=200;

	//
	m_sAppTracePath = L"C:\\CINETIC\\BRIDGE_CSM\\Traces\\";
	//
}

BOOL CAppSettings::LoadXML(CString sFileName)
{
	CXmlDocument		xmlDocument;
	CXmlElement*		pMainElement = NULL;
	CXmlElement*		pRootElement = NULL;
	CXmlElement*		pElement = NULL;
	BOOL bRetValue;
	CString fileName;

	fileName = m_sConfigFilesPath;
	fileName.Append(sFileName);

	bRetValue = xmlDocument.Load(fileName);
	if (bRetValue)
	{
		pMainElement = xmlDocument.GetRootElement();
		if (pMainElement)
		{
			pRootElement = xmlDocument.FindElement(pMainElement, XML_FILE_ROOT);
			if (pRootElement)
			{
				// "File" section
				pElement = xmlDocument.FindElement(pRootElement, XML_FILE_DATE);
				if (pElement && pElement->m_strData.IsEmpty())
				{
					return FALSE;
				}

				m_sConfigFileName = fileName;
				pElement = xmlDocument.FindElement(pRootElement, XML_FILE_UPDATE);
				if (pElement)
				{
					m_sUpdateTime = pElement->m_strData;
				}
			}
			pRootElement = xmlDocument.FindElement(pMainElement, XML_CONFIG_ROOT);
			if (pRootElement)
			{
				
				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_GSS1_IP);
				if (pElement)
					m_sGSS_IPAddr[0] = pElement->m_strData;

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_GSS2_IP);
				if (pElement)
					m_sGSS_IPAddr[1] = pElement->m_strData;

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_CHI_IP);
				if (pElement)
					m_sCHI_IPAddr = pElement->m_strData;
				
				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_GSS_IP_PORT);
				if (pElement)
					m_nGSSPort = _wtoi(pElement->m_strData);

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_CHI_IP_PORT);
				if (pElement)
					m_nCHIPort = _wtoi(pElement->m_strData);
				
					pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_GSS_NUM_PORT);
				if (pElement)
					m_nGSSNumPorts = _wtoi(pElement->m_strData);
				
				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_SORTER_EQUIPMENT_ID);
				if (pElement)
					m_sSorterEquipmentID = pElement->m_strData;


				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_FRAME_SIZELEN);
				if (pElement)
					m_nFrameSizeLen = _wtoi(pElement->m_strData);

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_FRAME_SIZE);
				if (pElement)
					m_nFrameSize = _wtoi(pElement->m_strData);
			}

			pRootElement = xmlDocument.FindElement(pMainElement, XML_LOG_ROOT);
			if (pRootElement)
			{
				pElement = xmlDocument.FindElement(pRootElement, XML_LOG_TRACE_PATH);
				if (pElement)
					m_sAppTracePath = pElement->m_strData;
			}
				
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CAppSettings::StoreXML(CString sFileName, BOOL bUpdate)
{
	CXmlDocument		xmlDocument;
	CXmlElement*		pMainElement = NULL;
	CXmlElement*		pRootElement = NULL;
	//CXmlElement*		pConfigurationElement = NULL;
	CXmlElement*		pElement = NULL;
	BOOL bRetValue;
	SYSTEMTIME tTime;
//	CString fileName;

//	fileName = m_sConfigFilesPath;
//	fileName.Append(sFileName);

	bRetValue = xmlDocument.Load(sFileName);
	if (bRetValue)
	{
		pMainElement = xmlDocument.GetRootElement();
		if (pMainElement)
		{
			pRootElement = xmlDocument.FindElement(pMainElement, XML_FILE_ROOT);
			if (pRootElement)
			{
				// "File" section
				GetLocalTime(&tTime);
				GetSystemTime(&tTime);
				pElement = xmlDocument.FindElement(pRootElement, XML_FILE_DATE);
				if (pElement)
				{
					pElement->m_strData.Format(_T("%d/%d/%d %d:%d:%d.%d"), tTime.wDay, tTime.wMonth, tTime.wYear, tTime.wHour, tTime.wMinute, tTime.wSecond, tTime.wMilliseconds);
				}
				if (bUpdate)
				{
					pElement = xmlDocument.FindElement(pRootElement, XML_FILE_UPDATE);
					if (pElement)
					{
						pElement->m_strData.Format(_T("%d/%d/%d %d:%d:%d.%d"), tTime.wDay, tTime.wMonth, tTime.wYear, tTime.wHour, tTime.wMinute, tTime.wSecond, tTime.wMilliseconds);
					}
				}
			}
			pRootElement = xmlDocument.FindElement(pMainElement, XML_CONFIG_ROOT);
			if (pRootElement)
			{
				// Application "Configuration" section
				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_GSS1_IP);
				if (pElement)
					pElement->m_strData = m_sGSS_IPAddr[0];

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_GSS2_IP);
				if (pElement)
					pElement->m_strData = m_sGSS_IPAddr[1];

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_GSS_IP_PORT);
				if (pElement)
					pElement->m_strData.Format(_T("%d"), m_nGSSPort);

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_CHI_IP);
				if (pElement)
					pElement->m_strData = m_sCHI_IPAddr;

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_CHI_IP_PORT);
				if (pElement)
					pElement->m_strData.Format(_T("%d"), m_nCHIPort);


				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_GSS_NUM_PORT);
				if (pElement)
					pElement->m_strData.Format(_T("%d"), m_nGSSNumPorts);

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_SORTER_EQUIPMENT_ID);
				if (pElement)
					pElement->m_strData = m_sSorterEquipmentID;

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_FRAME_SIZELEN);
				if (pElement)
					pElement->m_strData.Format(_T("%d"), m_nFrameSizeLen);

				pElement = xmlDocument.FindElement(pRootElement, XML_CONFIG_FRAME_SIZE);
				if (pElement)
					pElement->m_strData.Format(_T("%d"), m_nFrameSize);

			}

			pRootElement = xmlDocument.FindElement(pMainElement, XML_LOG_ROOT);
			if (pRootElement)
			{
				pElement = xmlDocument.FindElement(pRootElement, XML_LOG_TRACE_PATH);
				if (pElement)
					pElement->m_strData = m_sAppTracePath;
			}

			bRetValue = xmlDocument.Store(sFileName);
		}
	}

	return bRetValue ;
}