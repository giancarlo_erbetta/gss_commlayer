// NetLink.cpp: implementation of the CNetLink class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "BaseLib.h"
#include "NetLink.h"
#include "OverlappedSocket.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CNetLink::CNetLink
//@
//@ Parameters:
//@		[IN]       rcsInit  : init parameters (see STRUCT_NETLINK_INIT)
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CNetLink::CNetLink(STRUCT_NETLINK_INIT& rcsInit)
: CThreadLib(rcsInit.wszObjectName, rcsInit.nClosingtimeout),
  CNetLinkImpl(rcsInit)
{
	wcsncpy_s(CNetLinkImpl::m_wszObjectName, MAX_PATH, rcsInit.wszObjectName, MAX_PATH);
	CNetLinkImpl::m_wszObjectName[MAX_PATH] = '\0';
	//
	::WideCharToMultiByte(CP_ACP, NULL, CNetLinkImpl::m_wszObjectName, -1, 
												CNetLinkImpl::m_szObjectName, MAX_PATH, NULL, NULL);
	CNetLinkImpl::m_szObjectName[MAX_PATH] = 0;

	wcsncpy_s(m_wszIPAddr, MAX_PATH, rcsInit.wszIPAddr, MAX_PATH);
	m_wszIPAddr[MAX_PATH] = NULL;

	m_nPortID=rcsInit.nPortID;
	m_nPortRange=rcsInit.nPortRange;
	m_nInstanceID = rcsInit.nInstanceID;
	m_bServer = rcsInit.bServer;
	m_bAsync = rcsInit.bAsync;
	m_bMaster = rcsInit.bMaster;
	m_lPollingTime = rcsInit.lPollingTime;
	m_lRecvTimeout = rcsInit.lRecvTimeout;

	m_linkStatus = LINK_STOPPED;
	m_nCurrentPortID=m_nPortID;

	// run the worker thread
  CThreadLib::m_bObjectOk = CThreadLib::RunThread( CNetLink::EngineThread, rcsInit.nThreadPriority);
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Distructor CNetLink::~CNetLink
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CNetLink::~CNetLink()
{
  CThreadLib::StopThread();
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ Worker thread for link management
//@
//@ Parameters:
//@		[IN]       pParam  : pointer to parent object (this)
//@				
//@ Return value:
//@    exit value
//@
//@ Note:
//@    worker thread
//@
//@////////////////////////////////////////////////////////////////////
/*static*/DWORD WINAPI CNetLink::EngineThread(LPVOID pParam)
{
enum tagEventsName
{
  ID_EXIT_EVENT=0,
  ID_RECV_EVENT,
  ID_POLLING_EVENT,
  ID_RX_TIMEOUT_EVENT,
  ID_READ_QUEUE_SEND_EVENT,
  ID_READ_QUEUE_LAMP_EVENT,

  MAX_EVENTS
};

enum tagStep
{
  STEP_IDLE=0,
  STEP_CREATE,
  STEP_OPEN,
  STEP_SEND_KA,
  STEP_SEND,
  STEP_RECEIVE,
  STEP_RECYCLE,
  STEP_EXIT,
  STEP_EXIT_ON_ERROR,
  STEP_FREE,

  NUM_STEPS
};

COverlappedSocket* pLink=NULL;
CNetLink* _this=NULL;
HANDLE        hEvents[MAX_EVENTS];
BOOL          bRun=TRUE;
tagStep       step=STEP_IDLE;

LARGE_INTEGER liPollingTime;
LARGE_INTEGER liRxTimeoutTime;


const long   MS_CONST  = -10000L;  // coefficiente per millisecondi

try
{
  _this = (CNetLink*)pParam;

	long TIMEOUT_B = _this->m_lRecvTimeout;
	long POLLING_B = _this->m_lPollingTime;
	int nNumEvents=0;

	// preset ID_RECV_EVENT
	nNumEvents=1;

  hEvents[ID_EXIT_EVENT] = _this->GetExitHandle(); nNumEvents++;
  hEvents[ID_POLLING_EVENT] = CreateWaitableTimer(NULL, FALSE, NULL); nNumEvents++;
  hEvents[ID_RX_TIMEOUT_EVENT] = CreateWaitableTimer(NULL, FALSE, NULL); nNumEvents++;

	if(_this->m_bAsync == TRUE)
	{
		hEvents[ID_READ_QUEUE_SEND_EVENT] = _this->m_queueSend.GetEvReadHandle(); nNumEvents++;
		hEvents[ID_READ_QUEUE_LAMP_EVENT] = _this->m_queueLamp.GetEvReadHandle(); nNumEvents++;
	}

  _this->SetObjectReady();
  
  step=STEP_CREATE;

  liPollingTime.QuadPart   = POLLING_B * MS_CONST;
  liRxTimeoutTime.QuadPart = TIMEOUT_B * MS_CONST;

  // Loop
  while(bRun)
  {
		// earlier exit request
		if( ::WaitForSingleObject(hEvents[ID_EXIT_EVENT], 0) == WAIT_OBJECT_0
		&& step != STEP_EXIT_ON_ERROR
		&& step != STEP_EXIT
		&& step != STEP_FREE)
		{
			step=STEP_EXIT;
		}

    switch(step)
    {
      case STEP_IDLE:
        // Attesa 
        switch(::WaitForMultipleObjects(nNumEvents, hEvents, FALSE, INFINITE))
        {
          // Message to send
          case ID_READ_QUEUE_SEND_EVENT:
          case ID_READ_QUEUE_LAMP_EVENT:
            // restart polling timer
            SetWaitableTimer(hEvents[ID_POLLING_EVENT] ,&liPollingTime,0L,NULL,NULL,FALSE);

            step=STEP_SEND;
          break;

          // Message received
          case ID_RECV_EVENT:
						// restart receiving timeout
            SetWaitableTimer(hEvents[ID_RX_TIMEOUT_EVENT] ,&liRxTimeoutTime,0L,NULL,NULL,FALSE);
            step=STEP_RECEIVE;
          break;

          // Polling
          case ID_POLLING_EVENT:
            // start polling timer
            SetWaitableTimer(hEvents[ID_POLLING_EVENT] ,&liPollingTime,0L,NULL,NULL,FALSE);

            step=STEP_SEND_KA;
          break;

          // Receiving Timeout
          case ID_RX_TIMEOUT_EVENT:
            step=STEP_RECYCLE;
						_this->OnRxTimeout();
          break;

          // Exit request
          case ID_EXIT_EVENT:
          default:
            step=STEP_EXIT;
          break;

        }
      break;

      case STEP_CREATE:

        if( pLink == NULL)
        {
          if(_this->m_bServer == TRUE)
						pLink = new COverlappedSocketSR;
					else
						pLink = new COverlappedSocketCL;

          if(pLink == NULL)
					{
            step=STEP_EXIT_ON_ERROR;
						_this->OnMemoryOverflow();
          }
        }

			  _this->OnCreate();

        if( pLink->Create(_this->m_wszIPAddr, _this->m_nCurrentPortID) != TRUE)
        {  
          // step=STEP_EXIT_ON_ERROR;
					
					// Retry instead of quit
					step=STEP_RECYCLE;
					_this->m_linkStatus = LINK_ERROR;
          //
					_this->OnCreateError();
        }
        else
        {
          hEvents[ID_RECV_EVENT] = pLink->GetNetEventHandle();
          _this->m_linkStatus = LINK_WAITING;
          step=STEP_OPEN;
        }
      break;

      case STEP_OPEN:
        if( pLink->OpenLink( hEvents[ID_EXIT_EVENT], 1000/*WSA_INFINITE*/) != TRUE)
        {  
          switch(pLink->GetLastError())
          {
            case COverlappedSocket::ERR_USER_ABORT:
              step=STEP_EXIT;
            break;

            case COverlappedSocket::ERR_TIMEOUT:
							// Scan all ports
							if(_this->m_nPortRange > 0)
							{
								_this->m_nCurrentPortID++;
								if(_this->m_nCurrentPortID > (_this->m_nPortID+_this->m_nPortRange))
									_this->m_nCurrentPortID=_this->m_nPortID;

								step=STEP_RECYCLE;
							}
						break;
						case COverlappedSocket::ERR_CONNECT_IN_PROGRESS:
            break;

            // errore
            default:
							step=STEP_EXIT_ON_ERROR;
              //
							_this->OnOpenFailed();
            break;
          }
        }
        else
        {
          // wait for other channels ready
					::Sleep(100);

					if( _this->m_bAsync == TRUE  
					|| (_this->m_bAsync == FALSE && _this->m_bMaster == TRUE))
					{
						// start polling timer
						SetWaitableTimer(hEvents[ID_POLLING_EVENT] ,&liPollingTime,0L,NULL,NULL,FALSE);
					}

          // start receiving timeout
          SetWaitableTimer(hEvents[ID_RX_TIMEOUT_EVENT] ,&liRxTimeoutTime,0L,NULL,NULL,FALSE);

          _this->m_linkStatus = LINK_RUNNING;
 				  _this->OnConnected();
          
					// Start idle
          step=STEP_IDLE;
        }
      break;

      case STEP_SEND_KA:

				if( _this->OnSendBeginKA() != TRUE)
				{
          step = STEP_IDLE;
					break;
				}

        if( pLink->WriteMessage(hEvents[ID_EXIT_EVENT], TIMEOUT_B/*WSA_INFINITE*/, &_this->m_wsaTxData, &_this->m_dwWriteBytes) != TRUE)
        {  
          switch(pLink->GetLastError())
          {
            case COverlappedSocket::ERR_USER_ABORT:
              step=STEP_EXIT;
            break;

            case COverlappedSocket::ERR_TIMEOUT:
							_this->OnTxTimeout();
              step=STEP_RECYCLE;
            break;

            // errore
            default:
							_this->OnTxError();
              step=STEP_RECYCLE;
            break;
          }
        }
        else
        {
					// OFD CAUSE EXCEPTION ON PROGRAM EXIT !!!
				  _this->OnSendEnd();
          step=STEP_IDLE;
        }
      break;

      case STEP_SEND:

				if( _this->OnSendBegin() != TRUE)
				{
          step = STEP_IDLE;
					break;
				}

        if( pLink->WriteMessage(hEvents[ID_EXIT_EVENT], TIMEOUT_B/*WSA_INFINITE*/, &_this->m_wsaTxData, &_this->m_dwWriteBytes) != TRUE)
        {  
          switch(pLink->GetLastError())
          {
            case COverlappedSocket::ERR_USER_ABORT:
              step=STEP_EXIT;
            break;

            case COverlappedSocket::ERR_TIMEOUT:
							_this->OnTxTimeout();
              step=STEP_RECYCLE;
            break;

            // errore
            default:
							_this->OnTxError();
              step=STEP_RECYCLE;
            break;
          }
        }
        
        else
        {
				  if( _this->OnSendEnd() == TRUE)
					{
						// restart receiving timeout
						SetWaitableTimer(hEvents[ID_RX_TIMEOUT_EVENT] ,&liRxTimeoutTime,0L,NULL,NULL,FALSE);
					}
          step=STEP_IDLE;
        }
      break;

      case STEP_RECEIVE:

				if( _this->OnReceiveBegin() != TRUE)
				{
          step = STEP_IDLE;
					break;
				}

        if(pLink->ReadMessage(&_this->m_wsaRxData, &_this->m_dwReadBytes) != TRUE)
        {
          switch(pLink->GetLastError())
          {
            case COverlappedSocket::ERR_NO_DATA:
							_this->OnRxEmpty();
              step=STEP_IDLE;
            break;

            default:
							_this->OnRxError();
              step=STEP_RECYCLE;
            break;
          }  
        }
        else
        {
				  _this->OnReceiveEnd();

	        step=STEP_IDLE;
					
					if(_this->m_bAsync == FALSE && _this->m_bMaster == FALSE)
	          step=STEP_SEND;
        }
      break;

      case STEP_RECYCLE:
				
				_this->OnConnectionBroken();

				_this->m_linkStatus = LINK_ERROR;

        // Stop timers
        CancelWaitableTimer(hEvents[ID_POLLING_EVENT]);
        CancelWaitableTimer(hEvents[ID_RX_TIMEOUT_EVENT]);

        if(pLink != NULL)
        {
          delete pLink;
          pLink = NULL;
        }

        step=STEP_CREATE;
				// Retry delay on <create> error
        ::Sleep(500);
      break;

      case STEP_EXIT:
        _this->m_linkStatus = LINK_STOPPED;
        step=STEP_FREE;
      break;
  

      case STEP_EXIT_ON_ERROR:
        _this->m_linkStatus = LINK_ERROR;
        step=STEP_FREE;
      break;

      case STEP_FREE:

        if(hEvents[ID_POLLING_EVENT] != NULL)
          CloseHandle(hEvents[ID_POLLING_EVENT]);

        if(hEvents[ID_RX_TIMEOUT_EVENT] != NULL)
          CloseHandle(hEvents[ID_RX_TIMEOUT_EVENT]);

        if(pLink != NULL)
        {
          delete pLink;
          pLink = NULL;
        }

        bRun=FALSE;

				_this->OnShutdown();
      break;

		} // end switch(step)

		::Sleep(1);
	} // end while(bRun)

	return 0;
}
catch(...)
{
	CBaseLib::SetBit(ERR_EXCEPTION_ON_THR_NETLINK, TRUE);
	CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"Exception on Thread %.20s", _this->CThreadLib::m_wszObjectName);
	return 1;
}
	
}
