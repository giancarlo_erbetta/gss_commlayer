#pragma once


// CPasswordDlg dialog

class CPasswordDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPasswordDlg)

public:
	CPasswordDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPasswordDlg();

	void inline	SetTitle(_TCHAR* pszTitle=NULL);
	void inline	SetPassword(_TCHAR* pszPassword=NULL);

// Dialog Data
	enum { IDD = IDD_PSWDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	CEdit m_ctrlEditPassword;

private:
	CString	m_sTitle;
	CString	m_sPassword;
public:
	afx_msg void OnEnChangeEditPassword();
};

void inline CPasswordDlg::SetTitle(_TCHAR* pszTitle)
{
	m_sTitle.Format(_T("%s"),pszTitle);
}

void inline	CPasswordDlg::SetPassword(_TCHAR* pszPassword)
{
	m_sPassword.Format(_T("%s"),pszPassword);
}
