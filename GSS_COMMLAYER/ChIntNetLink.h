// CChIntNetLink.h: interface for the CChIntNetLink class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ThreadLib.h"
#include "QueueLib.h"
#include "AppTrace.h"
#include "ChIntNetLinkDef.h"

class CChIntNetLink : public CThreadLib
{
public:

	CChIntNetLink(STRUCT_CHI_NETLINK_INIT& csSTRUCT_CHI_NETLINK_INIT);
	virtual ~CChIntNetLink();
	//
	inline void GetLDI(STRUCT_CHI_NETLINK_DEBUG_INFO* pLDI);
	inline void ResetLDI();
	inline int GetLinkStatus();


public:
  CQueue m_queueSend;

protected:
	virtual BOOL OnCreate()           {/*VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_7_EVENTS,__FILE__,__LINE__,0, "%s Create soket on port %d", m_szObjectName, csSTRUCT_CHI_NETLINK_INIT.nPortID);*/ return TRUE;}
	virtual BOOL OnCreateError()			{/*VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "%s Create failed", m_szObjectName); */ return TRUE; }
	virtual BOOL OnMemoryOverflow()		{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s Memory overflow", m_szObjectName); return TRUE;}
	virtual BOOL OnOpenFailed()				{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s OpenLink failed", m_szObjectName); return TRUE;}
	virtual BOOL OnShutdown()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s Shut Down", m_szObjectName); return TRUE;}
	virtual BOOL OnRxError(int nLasterror)					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s RX Error %d", m_szObjectName, nLasterror); m_LDI.nNumRXerror++; return TRUE;}
	virtual BOOL OnRxEmpty()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s RX Empty", m_szObjectName); return TRUE;}
	virtual BOOL OnTxTimeout()				{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s TX Timeout", m_szObjectName); m_LDI.nNumTXerror++; return TRUE;}
	virtual BOOL OnTxError()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s TX Error", m_szObjectName); m_LDI.nNumTXerror++; return TRUE;}
	
	virtual BOOL OnConnected();
	virtual BOOL OnConnectionBroken();
	virtual BOOL OnReceiveBegin();
	virtual BOOL OnReceiveEnd();
	virtual BOOL OnSendBegin(STRUCT_CHI_NETLINK_QUEUE_RECORD& csSTRUCT_CHI_NETLINK_QUEUE_RECORD);
	virtual BOOL OnSendEnd();
	virtual BOOL OnRXTimeout();

	//
	virtual BOOL OnTelegramReceived(int nTelegramLen);

	BOOL On_TX_HB_Timeout();

protected:
	static DWORD WINAPI EngineThread(LPVOID pParam);
	STRUCT_CHI_NETLINK_INIT m_csSTRUCT_CHI_NETLINK_INIT;
	
	const int   SIZE_BUF_TX;
	const int   SIZE_BUF_RX;
	const BYTE  SOM_CH;
	const BYTE  EOM_CH;

	DWORD m_dwWriteBytes; 
	DWORD m_dwReadBytes; 
	WSABUF m_wsaTxData;
	WSABUF m_wsaRxData;
	BYTE*  m_byDatagramBuffer;
	int    m_nTxTimeout;

  int m_linkStatus;
	char m_szObjectName[MAX_PATH+1];
	int m_nInstanceID;
	STRUCT_CHI_NETLINK_DEBUG_INFO m_LDI;
	BOOL m_bLinkON;
	int m_nTxRetryCounter;

  int m_nBuildDatagramStep;
  int m_nDatagramWriterIndex;
  int m_nDatagramLength;
  
  DWORD m_dwRXBufIndex;
};

//@ Return the Link Debug Information
inline void CChIntNetLink::GetLDI(STRUCT_CHI_NETLINK_DEBUG_INFO* pLDI)
{
	// persistent data
	wcsncpy_s(m_LDI.wszObjectName, MAX_PATH, m_csSTRUCT_CHI_NETLINK_INIT.wszObjectName, MAX_PATH);
	m_LDI.nPortID= m_csSTRUCT_CHI_NETLINK_INIT.nPortID;
	m_LDI.lLinkLossTimeout = m_csSTRUCT_CHI_NETLINK_INIT.lLinkLossTimeout;
	m_LDI.nMaxRetries = m_csSTRUCT_CHI_NETLINK_INIT.nMaxRetries;
	m_LDI.nNumTXRetries = m_nTxRetryCounter;
	// fetch data
	m_LDI.nQueueLen = m_queueSend.GetQueuelen();
	m_LDI.nQueueSize = m_queueSend.GetSizeQueue();
	m_LDI.nQueuePeak = 	m_queueSend.GetMaxQueuelen();

	*pLDI = m_LDI;
}

//@ Clears the Link Debug Informations
inline void CChIntNetLink::ResetLDI()
{
  ::ZeroMemory(&m_LDI, sizeof(STRUCT_CHI_NETLINK_DEBUG_INFO));
	m_queueSend.SetMaxQueuelen(0);
}

//@ Return the Link Status
inline int CChIntNetLink::GetLinkStatus()
{
	return m_linkStatus;
}

extern CChIntNetLink* G_pChIntNetLink;
