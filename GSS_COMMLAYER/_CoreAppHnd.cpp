//////////////////////////////////////////////////////////////////
//
// NAME:										CoreAppHnd.cpp
// LAST MODIFICATION DATE:	23/10/2017
// AUTHOR:									G. Erbetta
// PURPOSE:									Application
//
//////////////////////////////////////////////////////////////////
#include "stdafx.h"

#include <afxinet.h>
#include "Externals.h"
//#include <io.h>
#include "GSS_Link.h"
#include "AppSettings.h"


CCoreAppHnd::CCoreAppHnd(void)
{
}

CCoreAppHnd::~CCoreAppHnd(void)
{
}

BOOL CCoreAppHnd::InitApplication(void)
{
	CString	sString;
	CString sTmp;
	CString sFullAppname;

	::CoInitialize(NULL);

	m_csLastitemTime = CTime::GetCurrentTime();
	m_csLastitemTime += CTimeSpan(0, 0, 10, 0);

	// Create Mutex In Order To Detect MySelf
	#define	TMP_APPLICATION_NAME _T("COMMLAYER")

	m_hMyMutex = ::CreateMutex(NULL,TRUE,TMP_APPLICATION_NAME);
	if(::GetLastError() == ERROR_ALREADY_EXISTS)
	{
		::MessageBox(NULL,_T("Program Already Active"),TMP_APPLICATION_NAME,MB_ICONINFORMATION);
		return FALSE;
	}
	#undef	TMP_APPLICATION_NAME


	m_hHandleWaitForSingleObject = ::CreateEvent(NULL,FALSE,FALSE,NULL);
	if(m_hHandleWaitForSingleObject == NULL)
	{
		::AfxMessageBox(_T("Error Create Handle"),MB_ICONSTOP);
		return FALSE;
	}

	// Get Actual Time
	m_csActualTime = CTime::GetCurrentTime();

	// Thread APPLICATION HANDLER
	m_pArrayTHREADS[THREAD_APPHND] = NULL;
	/*
	m_pArrayTHREADS[THREAD_APPHND] = AfxBeginThread(ThreadAPPHND,0,THREAD_PRIORITY_TIME_CRITICAL,0,CREATE_SUSPENDED);
	if(m_pArrayTHREADS[THREAD_APPHND] == NULL)
	{
		AfxMessageBox(_T("Error Creation ThreadAPPHND !"),MB_ICONSTOP);
		return FALSE;
	}
	m_pArrayTHREADS[THREAD_APPHND]->m_bAutoDelete = FALSE;
	DWORD_PTR dwThreadAffinityMask0=1<<0;
	DWORD_PTR dwPreviousAffinityMask = ::SetThreadAffinityMask(m_pArrayTHREADS[THREAD_APPHND]->m_hThread,dwThreadAffinityMask0);
	*/

	//
	// Create Application Queues
	//
	// TODO

	//
	//
	// Resume Threads
	//
	//
	// TODO m_pArrayTHREADS[THREAD_APPHND]->ResumeThread();
	//
	// Stat ImageBinaritation Application(s)
	//

	_OpenInternal_Link();
	
	// REMOVE only for debug
	_OpenGSS_Link();

	return TRUE;
}

void CCoreAppHnd::DestroyApplication(void)
{
	int		nLoop;
	CString	sString;
	//
	// Release My Identification Name
	//
	if(::ReleaseMutex(m_hMyMutex) == FALSE) // I'm Not The Owner
		::CloseHandle(m_hMyMutex);
	// Application Exit
	/*
	STRUCT_SINGLEMSGCML	csSINGLEMSGCML;
	csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_GENERIC_KILL_THREAD;
	csSINGLEMSGCML.csHeader.wDataLen = 0;
	m_QueueAPPHND.PutItem((LPBYTE)&csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
	*/

	_CloseInternal_Link();
	_CloseGSS_Link();


	//
	// Stop Threads
	//
	try
	{

		for(nLoop=0;nLoop<NUM_APPLICATION_THREADS;nLoop++)
		{
			if (m_pArrayTHREADS[nLoop] != NULL && m_pArrayTHREADS[nLoop]->m_hThread)
			{
				switch(::WaitForSingleObject(m_pArrayTHREADS[nLoop]->m_hThread,4000))
				{
				case WAIT_OBJECT_0: // Means OK...
					VERIFY(TRUE);
					break;
				default:
					VERIFY(FALSE);
					break;
				}
			}
	 	}
		//
		// Delete Threads
		//
		for(nLoop=0;nLoop<NUM_APPLICATION_THREADS;nLoop++)
			delete m_pArrayTHREADS[nLoop];
		//
		// Delete Application Queues
		//
	}
	catch(...)
	{
		::ExitProcess((UINT)-1);
	}
}

void CCoreAppHnd::_OpenInternal_Link(void)
{
}

void CCoreAppHnd::_OpenGSS_Link(void)
{
	STRUCT_GSS_LINK_INIT csInit;
	char szFormat[32];
	CString sLbl;
	CString sCSM_IPAddr[2];

	sCSM_IPAddr[0] = G_pAppSettings->m_sCSM_Production_IPAddr[0];
	sCSM_IPAddr[1] = G_pAppSettings->m_sCSM_Production_IPAddr[1];

	if (G_pAppSettings->m_nCMSNumPorts > NUM_MAX_GSS_SOCKET)
		G_pAppSettings->m_nCMSNumPorts = NUM_MAX_GSS_SOCKET;

	for (int i = 0; i < G_pAppSettings->m_nCMSNumPorts; i++)
	{
		// GSS1 (HOST) LINKs
		csInit.nInstanceID = GSS_LINK_CHANNEL_GSS1 + i;
		swprintf_s(csInit.wszObjectName, L"GSS1_CH%d", i);
		csInit.lLinkLossTimeout = 4000;	// as GSS frequency (every 4 seconds)
		csInit.nMaxRetries = 3;
		csInit.nClosingtimeout = 1000;
		csInit.nThreadPriority = THREAD_PRIORITY_NORMAL;
		wcsncpy_s(csInit.wszIPAddr, MAX_PATH, sCSM_IPAddr[0], MAX_PATH);
		csInit.nPortID = G_pAppSettings->m_nCSMPort;
		csInit.nQueueSendRecordSize = sizeof(STRUCT_GSS_LINK_QUEUE_RECORD);
		csInit.nQueueSendRecords = 500;
		//wcsncpy_s(csInit.wszQueueSendName, MAX_PATH, L"QHCHM", MAX_PATH);
		swprintf_s(csInit.wszQueueSendName, L"QGSS1_CH%d", i);
		//
		csInit.nFrameSizeLen = G_pAppSettings->m_nFrameSizeLen;
		csInit.nFrameSize = G_pAppSettings->m_nFrameSize;
		sprintf_s(szFormat, "%%0%dd", csInit.nFrameSizeLen);
		sprintf(csInit.szFrameSize, szFormat, 200);
		//
		//wcsncpy_s(csInit.wszQueueSendFileName, MAX_PATH, PERSISTENT_QUEUE_FILES_PATH L"QHCHM.DAT", MAX_PATH);
		swprintf(csInit.wszQueueSendFileName, L"%sQGSS1_CH%d.DAT", G_pAppSettings->m_sQueuePath, i);

		//
		G_pGSS_Links[csInit.nInstanceID] = new CGSS_Link(csInit);
	}

	for (int i = 0; i < G_pAppSettings->m_nCMSNumPorts; i++)
	{
		// GSS2 (HOST) LINKs
		csInit.nInstanceID = GSS_LINK_CHANNEL_GSS2 + i;
		swprintf_s(csInit.wszObjectName, L"GSS2_CH%d", i);
		csInit.lLinkLossTimeout = 4000;	// as GSS frequency (every 4 seconds)
		csInit.nMaxRetries = 3;
		csInit.nClosingtimeout = 1000;
		csInit.nThreadPriority = THREAD_PRIORITY_NORMAL;
		wcsncpy_s(csInit.wszIPAddr, MAX_PATH, sCSM_IPAddr[1], MAX_PATH);
		csInit.nPortID = G_pAppSettings->m_nCSMPort;
		csInit.nQueueSendRecordSize = sizeof(STRUCT_GSS_LINK_QUEUE_RECORD);
		csInit.nQueueSendRecords = 500;
		//wcsncpy_s(csInit.wszQueueSendName, MAX_PATH, L"QHCHM", MAX_PATH);
		swprintf_s(csInit.wszQueueSendName, L"QGSS2_CH%d", i);
		//
		csInit.nFrameSizeLen = G_pAppSettings->m_nFrameSizeLen;
		csInit.nFrameSize = G_pAppSettings->m_nFrameSize;
		sprintf_s(szFormat, "%%0%dd", csInit.nFrameSizeLen);
		sprintf(csInit.szFrameSize, szFormat, 200);
		//
		//wcsncpy_s(csInit.wszQueueSendFileName, MAX_PATH, PERSISTENT_QUEUE_FILES_PATH L"QHCHM.DAT", MAX_PATH);
		swprintf(csInit.wszQueueSendFileName, L"%sQGSS2_CH%d.DAT", G_pAppSettings->m_sQueuePath, i);

		//
		G_pGSS_Links[csInit.nInstanceID] = new CGSS_Link(csInit);
	}

}

void CCoreAppHnd::_CloseInternal_Link(void)
{
}

void CCoreAppHnd::_CloseGSS_Link(void)
{

	for (int i = 0; i < G_pAppSettings->m_nCMSNumPorts; i++)
	{
		if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + i] != NULL)
		{
			delete G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + i];
			G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + i] = NULL;
		}
		//_UpdateLinkBox(LINK_STOPPED, BOX_GSS1CH0 + i);

		if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + i] != NULL)
		{
			delete G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + i];
			G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + i] = NULL;
		}
		//_UpdateLinkBox(LINK_STOPPED, BOX_GSS2CH0 + i);
	}

}


//@//////////////////////////////////////////////////////////////////////
//@
//@ Worker thread 
//@
DWORD WINAPI CCoreAppHnd::_EngineThread(LPVOID pParam)
{
	enum tagEventsName
	{
		ID_EXIT_EVENT = 0,
		ID_READ_QUEUE_EVENT,

		NUM_EVENTS
	};

	enum tagStep
	{
		STEP_IDLE = 0,
		STEP_RUN,
		STEP_EXIT,

		NUM_STEPS
	};

	CCoreAppHnd* _this = NULL;
	HANDLE hEvents[NUM_EVENTS];
	BOOL bRun = TRUE;
	tagStep step = STEP_IDLE;

	//STRUCT_HOSTMANAGER_QUEUE_RECORD csSTRUCT_HOSTMANAGER_QUEUE_RECORD;

	try
	{
		_this = (CCoreAppHnd*)pParam;

		hEvents[ID_EXIT_EVENT] = _this->GetExitHandle();
		hEvents[ID_READ_QUEUE_EVENT] = _this->m_queueIn.GetEvReadHandle();

		_this->SetObjectReady();

		step = STEP_IDLE;

		// Loop
		while (bRun)
		{
			switch (step)
			{
			case STEP_IDLE:
				step = STEP_RUN;
			case STEP_RUN:
				// waiting 
				switch (::WaitForMultipleObjects(NUM_EVENTS, hEvents, FALSE, INFINITE))
				{
					// message from queue
				case ID_READ_QUEUE_EVENT:
					while (_this->m_queueIn.GetItem((BYTE*)&csSTRUCT_HOSTMANAGER_QUEUE_RECORD))
					{
						_this->_MsgFromQueue(csSTRUCT_HOSTMANAGER_QUEUE_RECORD);
					}

					break;

					// exit request
				case ID_EXIT_EVENT:
				default:
					step = STEP_EXIT;
					break;
				}
				break;

			case STEP_EXIT:
				bRun = FALSE;
				break;

			}
			::Sleep(1);
		}
		return 0;
	}
	catch (...)
	{
		CBaseLib::SetBit(16, TRUE);
		CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"Exception on HOSTManager thread");
		return 1;
	}
}