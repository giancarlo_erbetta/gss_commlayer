// GSS_Link.cpp: implementation of the CGSS_Link class.
//
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OverlappedSocket.h"
#include "AppSettings.h"
#include "GSS_Link.h"
#include "LinksManager.h"

#include "ConversionsLib.h"

#include "ProtocolConverter_FX.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CGSS_Link::CGSS_Link
//@
//@ Parameters:
//@		 [IN]       STRUCT_GSS_LINK_INIT& rcsInit
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CGSS_Link::CGSS_Link(STRUCT_GSS_LINK_INIT& _csSTRUCT_GSS_LINK_INIT) :
  CThreadLib(_csSTRUCT_GSS_LINK_INIT.wszObjectName, _csSTRUCT_GSS_LINK_INIT.nClosingtimeout),
	m_queueSend(_csSTRUCT_GSS_LINK_INIT.nQueueSendRecordSize, _csSTRUCT_GSS_LINK_INIT.nQueueSendRecords, 
	            _csSTRUCT_GSS_LINK_INIT.wszQueueSendName, _csSTRUCT_GSS_LINK_INIT.wszQueueSendFileName),
  SIZE_BUF_TX(4096),
	SIZE_BUF_RX(4096),
//	SOM_CH(2),
//	EOM_CH(3),
	m_csSTRUCT_GSS_LINK_INIT(_csSTRUCT_GSS_LINK_INIT)
{
	STRUCT_MSGMANAGER_QUEUE_RECORD csSTRUCT_MSGMANAGER_QUEUE_RECORD;

	//
	::WideCharToMultiByte(CP_ACP, NULL, _csSTRUCT_GSS_LINK_INIT.wszObjectName, -1, 
												m_szObjectName, MAX_PATH, NULL, NULL);
	m_szObjectName[MAX_PATH] = 0;

	m_nInstanceID = _csSTRUCT_GSS_LINK_INIT.nInstanceID;

  // initialization
	m_wsaTxData.len = SIZE_BUF_TX;
  m_wsaTxData.buf = new char[m_wsaTxData.len+1];
  if(m_wsaTxData.buf == NULL)
		throw CFOLibException(0, L"CGSS_Link: m_wsaTxData.buf == NULL");

	m_wsaRxData.len = SIZE_BUF_RX;
  m_wsaRxData.buf = new char[m_wsaRxData.len+1];
  if(m_wsaRxData.buf == NULL)
		throw CFOLibException(0, L"CGSS_Link: m_wsaRxData.buf == NULL");

  m_byDatagramBuffer = new BYTE[SIZE_BUF_RX];
  if(m_byDatagramBuffer == NULL)
		throw CFOLibException(0, L"CGSS_Link: m_byDatagramBuffer == NULL");

	m_nGSS_ID = _csSTRUCT_GSS_LINK_INIT.nGSS_ID;

	m_dwWriteBytes=0; 
	m_dwReadBytes=0; 
	m_bLinkON=FALSE;
  m_nTxWithoutRxCounter=0;
  m_nBuildDatagramStep=0;
  m_nDatagramWriterIndex=0;
  m_dwRXBufIndex=0;
  
	// run the worker thread
  CThreadLib::m_bObjectOk = CThreadLib::RunThread( CGSS_Link::EngineThread, m_csSTRUCT_GSS_LINK_INIT.nThreadPriority);
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Distructor CGSS_Link::~CGSS_Link
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CGSS_Link::~CGSS_Link()
{
  CThreadLib::StopThread();
	// make some Sleeps to allow thread stop before releasing buffers
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);

	if(m_wsaTxData.buf != NULL)
	{
		delete[] m_wsaTxData.buf;
		m_wsaTxData.buf = NULL;
	}

	if(m_wsaRxData.buf != NULL)
	{
		delete[] m_wsaRxData.buf;
		m_wsaRxData.buf = NULL;
	}
	
	if(m_byDatagramBuffer != NULL)
	{
	  delete m_byDatagramBuffer;
	  m_byDatagramBuffer = NULL;
	}

}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Prepare the message to be sent
//@ Grouped telegrams per tcp/ip frame
//@
BOOL CGSS_Link::OnSendBegin()
{
char szFormat[32];
int nTxMsgLen;

	if(m_wsaTxData.buf == NULL)
	  return FALSE;

	::ZeroMemory(m_wsaTxData.buf, SIZE_BUF_TX);
	STRUCT_GSS_LINK_QUEUE_RECORD csSTRUCT_GSS_LINK_QUEUE_RECORD;
  //
  BYTE* pbyData= (BYTE*)m_wsaTxData.buf;
  int nNumGroupedTelegrams=0;
 
	// send all messages in queue. Stop at max buffer length
	while(m_queueSend.GetQueuelen() )
	{
		// length of next message
		if(!m_queueSend.QueryNextItem((BYTE*)&csSTRUCT_GSS_LINK_QUEUE_RECORD))
			break; 

		// can be held in the buffer? ( -10 to be sure )
		// for FX the output frame length is fixed !!!!
		if(pbyData+m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen > (BYTE*)&m_wsaTxData.buf[SIZE_BUF_TX-10])
		//if(pbyData+(csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen) > (BYTE*)&m_wsaTxData.buf[SIZE_BUF_TX-10])
			break; // stop for buffer end
		
		// pick next message
		if(!m_queueSend.GetItem((BYTE*)&csSTRUCT_GSS_LINK_QUEUE_RECORD))
			break; 

    // 
	  // FILL HEADER
	  //
		char *szTxMes = (char*)pbyData;

		if (csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID >= MAX_wMSGID_GSS)
		{
		  VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|GSSLINK||||%s ERROR TX unknown  message %d! TX Aborted!!!!", csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID, m_szObjectName);
			return FALSE;
		}

		if (csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID == MAX_wMSGID_GSS)
		{
			szTxMes=szTxMes;
		}
		// for FX frame length 
		sprintf_s(szFormat,"%%0%dd",m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen);
		sprintf(szTxMes,szFormat, m_csSTRUCT_GSS_LINK_INIT.nFrameSize);
		szTxMes += m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen;

		// 
	  // FILL DATA
	  //
		// for FX REAL GSS message
		// convert it before !!!
		nTxMsgLen = G_pProtocolConverter_FX->ConvertToMsgE(csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID, (BYTE *)csSTRUCT_GSS_LINK_QUEUE_RECORD.szMsgData, (BYTE *)szTxMes);

		if (csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID != 0)
		{
			VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_10_FRAMES,__FILE__,__LINE__,0, "|GSSLINK|%.2s|||%s DaCo->GSS TX[%.200s]",szTxMes , m_szObjectName, szTxMes);
			if (G_pProtocolConverter_FX->strMsgFormat[csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID].bLOG)
			{
				// send RAW data info
				STRUCT_CHI_NETLINK_QUEUE_RECORD csSTRUCT_CHI_NETLINK_QUEUE_RECORD;
				STRUCT_RAWDATA_MSG csSTRUCT_RAWDATA_MSG;
				memset(&csSTRUCT_CHI_NETLINK_QUEUE_RECORD, 0, sizeof(csSTRUCT_CHI_NETLINK_QUEUE_RECORD));
				memset(&csSTRUCT_RAWDATA_MSG, 0, sizeof(csSTRUCT_RAWDATA_MSG));
				// fill message data
				csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.byMsgID = INT_MSG_TX_RAWDATA;
				csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen = sizeof(STRUCT_RAWDATA_MSG);
				csSTRUCT_RAWDATA_MSG.nGSS_ID = m_nGSS_ID;
				memcpy(csSTRUCT_RAWDATA_MSG.szMSG, szTxMes, min(sizeof(STRUCT_RAWDATA_MSG), nTxMsgLen));
				memcpy(csSTRUCT_CHI_NETLINK_QUEUE_RECORD.szMsgData, &csSTRUCT_RAWDATA_MSG, sizeof(STRUCT_RAWDATA_MSG));
				// send to CHI
				G_pChIntNetLink->m_queueSend.PutItem((BYTE*)&csSTRUCT_CHI_NETLINK_QUEUE_RECORD);
			}
		}
			
		//VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|GSSLINK|%.2s|||%s _MsgFromQueueI OnTelegramReceived and forwarded", szTxMes, m_szObjectName);
		// move pointer to end of telegram
	  pbyData+=m_csSTRUCT_GSS_LINK_INIT.nFrameSize;

  	// count TX telegrams
  	m_LDI.nNumTXTelegrams++;

	  // count grouped telegrams
	  nNumGroupedTelegrams++;

  } // end while

  // set total number of bytes to be sent
  m_wsaTxData.len = (pbyData - (BYTE*)m_wsaTxData.buf);

  // update max TX grouped messages
  if(m_LDI.nMaxTxGroupedTelegrams < nNumGroupedTelegrams)
    m_LDI.nMaxTxGroupedTelegrams = nNumGroupedTelegrams;
	
	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Message has been sent properly
//@
BOOL CGSS_Link::OnSendEnd()
{
	m_LDI.nNumTx++;

	return TRUE;
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ 
//@
BOOL CGSS_Link::OnReceiveBegin()
{
	if(m_wsaRxData.buf == NULL)
	  return FALSE;

  if(m_byDatagramBuffer == NULL)
    return FALSE;

  m_wsaRxData.len=SIZE_BUF_RX;
  ::ZeroMemory(m_wsaRxData.buf, SIZE_BUF_RX);

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ OnReceiveEnd
//@
//@	ho ricevuto un data frame da HOST
//@ lo analizzo ed eleboro
//@ 
//@
BOOL CGSS_Link::OnReceiveEnd()
{
  char cValue;
	bool bErrFound=0;

  // restart TX retry counter on RX timeout
  m_nTxWithoutRxCounter = 0;
   
	// parse all RX buffer
	while(m_dwRXBufIndex < m_dwReadBytes)
	{
		// force restart if destination buffer overflow
		if(m_nDatagramWriterIndex >= SIZE_BUF_RX)
			m_nDatagramWriterIndex=0;
		
		cValue = m_wsaRxData.buf[m_dwRXBufIndex];
		
		switch(m_nBuildDatagramStep)
		{
			// WAIT FOR STX
			case 0:
				//m_csSTRUCT_GSS_LINK_INIT.nFrameLength=0;
				if (memcmp(&m_wsaRxData.buf[m_dwRXBufIndex], m_csSTRUCT_GSS_LINK_INIT.szFrameSize, m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen) )
				{
					if (!bErrFound)
					{
						bErrFound = 1;
						m_LDI.nNumTXerror++;
					}
				  break;
				}
				//
				::ZeroMemory(m_byDatagramBuffer, SIZE_BUF_RX);
				m_nDatagramWriterIndex=0;
				//
			  m_byDatagramBuffer[m_nDatagramWriterIndex++] = cValue;
        
        // next step: receive message body
				m_nBuildDatagramStep = 1;
			break;
			
			// WAIT FOR ETX, RECEIVE MESSAGE BODY
			case 1:
			  bErrFound=0;
			  m_byDatagramBuffer[m_nDatagramWriterIndex++] = cValue;

				// ETX ?
				if(m_nDatagramWriterIndex == m_csSTRUCT_GSS_LINK_INIT.nFrameSize)
				{
          m_nNumRxGroupedTelegrams++;          

          m_byDatagramBuffer[m_nDatagramWriterIndex] = 0;    
          
          // PROCESS TELEGRAM
          OnTelegramReceived(m_nDatagramWriterIndex);

          // next step: receive SOM
          m_nBuildDatagramStep = 0;
        }
			  
      break;
    }
		m_dwRXBufIndex++;
  } // while(m_dwRXBufIndex < m_dwReadBytes)


  // update max RX grouped messages
  if(m_LDI.nMaxRxGroupedTelegrams < m_nNumRxGroupedTelegrams)
    m_LDI.nMaxRxGroupedTelegrams = m_nNumRxGroupedTelegrams;

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Process received telegram
//@
BOOL CGSS_Link::OnTelegramReceived(int nTelegramLen)
{
	STRUCT_MSGMANAGER_QUEUE_RECORD csSTRUCT_MSGMANAGER_QUEUE_RECORD;
	char szPrintString[255];
	BOOL bHB = FALSE;

	// count telegrams
	m_LDI.nNumRXTelegrams++;
	memset(csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgData, 0, sizeof(csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgData));

	//
  // PROCESS RX MESSAGES
  //
	if (!memcmp("HB",&m_byDatagramBuffer[m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen],2))
	{
		// for FedEx no replay is needed
    STRUCT_GSS_LINK_QUEUE_RECORD csSTRUCT_GSS_LINK_QUEUE_RECORD;
		bHB = TRUE;
//		m_LDI.
	}
	else 
	{
		m_LDI.nTotalRxMsg++;
		// forward message to LinksManager
		csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode = G_pProtocolConverter_FX->GetGSSMsgIndex((char*)&m_byDatagramBuffer[m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen]);
		if (csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode < MAX_wMSGID_GSS)
		{
			csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen = G_pProtocolConverter_FX->strMsgFormat[csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode].nTotalLenI; // G_pAppSettings->m_nFrameSize;// 
		
			G_pProtocolConverter_FX->ConvertToMsgI(csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode, &m_byDatagramBuffer[m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen], csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgData, csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen);

			//memcpy(csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgData, &m_byDatagramBuffer[m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen], csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen);

			VALIDPTR(G_pLinksManager)->m_QueueExternal.PutItem((BYTE*)&csSTRUCT_MSGMANAGER_QUEUE_RECORD);
		}
		else
		{
			sprintf(szPrintString, "|GSSLINK||||Invalid MSG ID: %d", csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode);
		}
	}
	if (bHB != TRUE)
	{
		sprintf(szPrintString, "|GSSLINK|%%.2s|||%%s RX[%%.%ds]", nTelegramLen - m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen);
		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_10_FRAMES, __FILE__, __LINE__, 0, szPrintString, m_szObjectName, &m_byDatagramBuffer[m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen]);
		if (csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode < MAX_wMSGID_GSS && G_pProtocolConverter_FX->strMsgFormat[csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode].bLOG)
		{
			// send RAW data info
			STRUCT_CHI_NETLINK_QUEUE_RECORD csSTRUCT_CHI_NETLINK_QUEUE_RECORD;
			STRUCT_RAWDATA_MSG csSTRUCT_RAWDATA_MSG;
			memset(&csSTRUCT_CHI_NETLINK_QUEUE_RECORD, 0, sizeof(csSTRUCT_CHI_NETLINK_QUEUE_RECORD));
			memset(&csSTRUCT_RAWDATA_MSG, 0, sizeof(csSTRUCT_RAWDATA_MSG));
			// fill message data
			csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.byMsgID = INT_MSG_RX_RAWDATA;
			csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen = sizeof(STRUCT_RAWDATA_MSG);
			csSTRUCT_RAWDATA_MSG.nGSS_ID = m_nGSS_ID;
			memcpy(csSTRUCT_RAWDATA_MSG.szMSG, &m_byDatagramBuffer[m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen], min(sizeof(STRUCT_RAWDATA_MSG), nTelegramLen - m_csSTRUCT_GSS_LINK_INIT.nFrameSizeLen));
			memcpy(csSTRUCT_CHI_NETLINK_QUEUE_RECORD.szMsgData, &csSTRUCT_RAWDATA_MSG, sizeof(STRUCT_RAWDATA_MSG));
			// send to CHI
			G_pChIntNetLink->m_queueSend.PutItem((BYTE*)&csSTRUCT_CHI_NETLINK_QUEUE_RECORD);
		}
	}
	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Process HB timeout
//@
//@ Send a HB at fixet interval time without consider other network traffic.
//@ When max TX are reached without no RX : close the link
//@
BOOL CGSS_Link::OnHBTimeout()
{
STRUCT_GSS_LINK_QUEUE_RECORD csSTRUCT_GSS_LINK_QUEUE_RECORD;
char szString[10];

  // count retries
  if(m_nTxWithoutRxCounter < m_csSTRUCT_GSS_LINK_INIT.nMaxRetries)
  {
		// post HB message
    m_nTxWithoutRxCounter++;

		csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID = G_pProtocolConverter_FX->GetGSSMsgIndex("HB");
		if (csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID == (USHORT) -1)
			return FALSE;

		csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen = G_pProtocolConverter_FX->strMsgFormat[csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID].nTotalLenI;

		sprintf(szString, "%S", G_pAppSettings->m_sSorterEquipmentID);

		memcpy(csSTRUCT_GSS_LINK_QUEUE_RECORD.szMsgData, szString, csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen);

		m_queueSend.PutItem((BYTE*)&csSTRUCT_GSS_LINK_QUEUE_RECORD);
  
	  return FALSE;
  }
  // close channel
  else
  {
    VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s MAX TX (%d) WITHOUT LIFE SIGNAL - CLOSING LINK", 
      m_szObjectName, m_nTxWithoutRxCounter);

	  return TRUE;
	}

	return TRUE;

}



//@//////////////////////////////////////////////////////////////////////
//@
//@ Connected
//@
BOOL CGSS_Link::OnConnected()
{
	STRUCT_GSS_LINK_QUEUE_RECORD csSTRUCT_GSS_LINK_QUEUE_RECORD;
	STRUCT_MSGMANAGER_QUEUE_RECORD csSTRUCT_MSGMANAGER_QUEUE_RECORD;

	// send LI telegram
	char szString[10];

	m_bLinkON = TRUE;
  m_nTxWithoutRxCounter = 0;
  m_nBuildDatagramStep = 0;
  m_nDatagramWriterIndex = 0;

	VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_8_WARNINGS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s CONNECTED", m_szObjectName);
	//
	csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID = (WORD) G_pProtocolConverter_FX->GetGSSMsgIndex("LI");
	if (csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID == (WORD)-1 )
		return TRUE;

	csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen = G_pProtocolConverter_FX->strMsgFormat[csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID].nTotalLenI;

	sprintf (szString, "%S", G_pAppSettings->m_sSorterEquipmentID);

	memcpy(csSTRUCT_GSS_LINK_QUEUE_RECORD.szMsgData, szString, csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen);

	m_queueSend.PutItem((BYTE*)&csSTRUCT_GSS_LINK_QUEUE_RECORD);

	csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode = INT_MSG_DIAG;
	csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen = 0;
	VALIDPTR(G_pLinksManager)->m_QueueExternal.PutItem((BYTE*)&csSTRUCT_MSGMANAGER_QUEUE_RECORD);

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Connection broken
//@
BOOL CGSS_Link::OnConnectionBroken()
{
STRUCT_MSGMANAGER_QUEUE_RECORD csSTRUCT_MSGMANAGER_QUEUE_RECORD;

  // one shot action
  if(	m_bLinkON==TRUE)
  {
	  m_bLinkON=FALSE;
	  VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_8_WARNINGS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s CONNECTION DROPPED", m_szObjectName);

  }

	csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode = INT_MSG_DIAG;
	csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen = 0;
	VALIDPTR(G_pLinksManager)->m_QueueExternal.PutItem((BYTE*)&csSTRUCT_MSGMANAGER_QUEUE_RECORD);

  return TRUE;

}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Worker thread for TIM protocol channel management
//@
//@ Parameters:
//@		[IN]       pParam  : pointer to parent object (this)
//@				
//@ Return value:
//@    exit value
//@
//@ Note:
//@    worker thread, Server - Full duplex
//@
//@////////////////////////////////////////////////////////////////////
/*static*/DWORD WINAPI CGSS_Link::EngineThread(LPVOID pParam)
{
enum tagEventsName
{
  ID_EXIT_EVENT=0,
	ID_HB_TIMEOUT_EVENT,
	ID_RX_EVENT,
  ID_READ_SENDQUEUE_EVENT,

  NUM_EVENTS
};

enum tagSteps
{
  STEP_CREATE=0,
  STEP_OPEN,
  STEP_IDLE,
  STEP_RECEIVE,
  STEP_SEND,
  STEP_RECYCLE,
  STEP_EXIT,
  STEP_EXIT_ON_ERROR,
  STEP_FREE,

  NUM_STEPS
};

const long TO_MILLISECONDS = -10000L;  // for milliseconds conversion
const long NET_TIMEOUT    = 1000L;    // Network timeout

CGSS_Link* _this=NULL;
COverlappedSocketCL* pLink=NULL;
BOOL bRun=TRUE;
tagSteps step=STEP_IDLE;
//
LARGE_INTEGER liLinkLossTimeout;
//
HANDLE hEvents[NUM_EVENTS];

try
{
  _this = (CGSS_Link*)pParam;
  hEvents[ID_EXIT_EVENT] = _this->GetExitHandle();
  hEvents[ID_HB_TIMEOUT_EVENT] = CreateWaitableTimer(NULL, FALSE, NULL);
  hEvents[ID_READ_SENDQUEUE_EVENT] = _this->m_queueSend.GetEvReadHandle();

  _this->SetObjectReady();
  
  step=STEP_CREATE;

	liLinkLossTimeout.QuadPart = _this->m_csSTRUCT_GSS_LINK_INIT.lLinkLossTimeout * TO_MILLISECONDS;

  // Loop
  while(bRun)
  {
    switch(step)
    {
      case STEP_CREATE:

        // check for exit request
        if(::WaitForSingleObject(hEvents[ID_EXIT_EVENT] ,0) == 0)
        {
          step=STEP_EXIT;
          break;
        }

        if( pLink == NULL)
        {
          // Create a server object
          pLink = new COverlappedSocketCL;
          if(pLink == NULL)
					{
            step=STEP_EXIT_ON_ERROR;
						_this->OnMemoryOverflow();
          }
        }

			  _this->OnCreate();

        if( pLink->Create(_this->m_csSTRUCT_GSS_LINK_INIT.wszIPAddr, _this->m_csSTRUCT_GSS_LINK_INIT.nPortID) != TRUE)
        {  
          // step=STEP_EXIT_ON_ERROR;
					
					// Retry instead of quit
					step=STEP_RECYCLE;
					_this->m_linkStatus = LINK_ERROR;
          //
					_this->OnCreateError();
        }
        else
        {
          hEvents[ID_RX_EVENT] = pLink->GetNetEventHandle();
          _this->m_linkStatus = LINK_WAITING;
          
					// next step
					step=STEP_OPEN;
        }
      break;

      case STEP_OPEN:
        if( pLink->OpenLink( hEvents[ID_EXIT_EVENT], 1000/*WSA_INFINITE*/) != TRUE)
        {  
          switch(pLink->GetLastError())
          {
            case COverlappedSocketCL::ERR_USER_ABORT:
              step=STEP_EXIT;
            break;

            case COverlappedSocketCL::ERR_TIMEOUT:
						case COverlappedSocketCL::ERR_CONNECT_IN_PROGRESS:
            break;

            // errore
            default:
							step=STEP_EXIT_ON_ERROR;
              //
							_this->OnOpenFailed();
            break;
          }
        }
        else
        {
          // wait for peer ready
					::Sleep(100);

          // start link loss timer
          SetWaitableTimer(hEvents[ID_HB_TIMEOUT_EVENT] ,&liLinkLossTimeout,0L,NULL,NULL,FALSE);

          _this->m_linkStatus = LINK_RUNNING;
 				  _this->OnConnected();
          
					// next step
          step=STEP_IDLE;
        }
      break;

      case STEP_IDLE:
				// Waiting
        switch(DWORD dwEvent=::WaitForMultipleObjects(NUM_EVENTS, hEvents, FALSE, INFINITE))
        {
          // Message to send
          case ID_READ_SENDQUEUE_EVENT:

						step = STEP_SEND;

          break;

          // Message received
          case ID_RX_EVENT:

            // restart link loss timer
            // SetWaitableTimer(hEvents[ID_HB_TIMEOUT_EVENT] ,&liLinkLossTimeout,0L,NULL,NULL,FALSE);

            step=STEP_RECEIVE;
          break;

          // receiving timeout
          case ID_HB_TIMEOUT_EVENT:
						// 
						if(_this->OnHBTimeout() == FALSE)
						{
              // restart link loss timer
              SetWaitableTimer(hEvents[ID_HB_TIMEOUT_EVENT] ,&liLinkLossTimeout,0L,NULL,NULL,FALSE);
						}
						else
						{
						  // comment it out to avoid closing channel during debuggin
						  step=STEP_RECYCLE;
						}
          break;

          // Exit request
          case ID_EXIT_EVENT:
						step = STEP_EXIT;
					default:
            step=STEP_EXIT;
          break;

				} // end switch
			break;

      case STEP_RECEIVE:

				if( _this->OnReceiveBegin() != TRUE)
				{
          step = STEP_IDLE;
					break;
				}

        if(pLink->ReadMessage(&_this->m_wsaRxData, &_this->m_dwReadBytes) != TRUE)
        {
          switch(pLink->GetLastError())
          {
            case COverlappedSocketCL::ERR_NO_DATA:
							_this->OnRxEmpty();
              step=STEP_IDLE;
            break;

            default:
							_this->OnRxError();
              step=STEP_RECYCLE;
            break;
          }  
        }
        else
        {
					step=STEP_IDLE;

					// dump telegram
					_this->m_wsaRxData.buf[_this->m_dwReadBytes] = 0;
					VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_0_DEBUG,__FILE__,__LINE__,0, "|GSSLINK|%.2s|||%s RX[%.200s]",
						&_this->m_wsaRxData.buf[3] , 
						_this->m_szObjectName, 
						_this->m_wsaRxData.buf);

					_this->m_LDI.nNumRx++;
					_this->m_dwRXBufIndex = 0;
					_this->m_nNumRxGroupedTelegrams = 0;
					_this->OnReceiveEnd();

        } // end else
			break;

      case STEP_SEND:

				if( _this->OnSendBegin() != TRUE)
				{
          step = STEP_IDLE;
					break;
				}

        if( pLink->WriteMessage(hEvents[ID_EXIT_EVENT], NET_TIMEOUT/*WSA_INFINITE*/, &_this->m_wsaTxData, &_this->m_dwWriteBytes) != TRUE)
        {  
          switch(pLink->GetLastError())
          {
            case COverlappedSocketCL::ERR_USER_ABORT:
              step=STEP_EXIT;
            break;

            case COverlappedSocketCL::ERR_TIMEOUT:
							_this->OnTxTimeout();
              step=STEP_RECYCLE;
            break;

            // errore
            default:
							_this->OnTxError();
              step=STEP_RECYCLE;
            break;
          }
        }
        else
        {
					// dump telegram
					_this->m_wsaTxData.buf[_this->m_dwWriteBytes] = 0;
					VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_0_DEBUG,__FILE__,__LINE__,0, "|GSSLINK|%.2s|||%s TX[%s]", &_this->m_wsaTxData.buf[4], _this->m_szObjectName, _this->m_wsaTxData.buf);

				  if( _this->OnSendEnd() == TRUE)
					{
					}
          step=STEP_IDLE;
        }
      break;

      case STEP_RECYCLE:
				
				_this->OnConnectionBroken();

				_this->m_linkStatus = LINK_ERROR;

        // Stop timers
        CancelWaitableTimer(hEvents[ID_HB_TIMEOUT_EVENT]);

        if(pLink != NULL)
        {
          delete pLink;
          pLink = NULL;
        }

        step=STEP_CREATE;
				// Retry delay on <create> error
        ::Sleep(500);
      break;

      case STEP_EXIT:
        _this->m_linkStatus = LINK_STOPPED;
        step=STEP_FREE;
      break;
  

      case STEP_EXIT_ON_ERROR:
        _this->m_linkStatus = LINK_ERROR;
        step=STEP_FREE;
      break;

      case STEP_FREE:

        if(hEvents[ID_HB_TIMEOUT_EVENT] != NULL)
          CloseHandle(hEvents[ID_HB_TIMEOUT_EVENT]);

        if(pLink != NULL)
        {
          delete pLink;
          pLink = NULL;
        }

        bRun=FALSE;

				_this->OnShutdown();
      break;

		} // end switch(step)

		::Sleep(1);
	
	} // end while(bRun)


	return 0;
}
catch(CFOLibException& e)
{
	CBaseLib::SetBit(13, TRUE);
	CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"%.250s", e.m_szDescription);	
	return 1;
}
catch(...)
{
	CBaseLib::SetBit(13, TRUE);
	CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"Exception on Thread %.20s", _this->CThreadLib::m_wszObjectName);
	return 1;
}
	
}

