//@////////////////////////////////////////////////////////////
//@
//@	Class:		CTraceLib
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@
//@	Created:	06 February 2001
//@
//@	Author:		Giuseppe Alessio
//@
//@	Updated:	
//@ 28 January 2005
//@ Support to BaseLib
//@
//@ August 4, 2005
//@ Log string when full queue
//@
//@////////////////////////////////////////////////////////////

#ifndef _TRACELIB_H
#define _TRACELIB_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>

#include <sys\timeb.h>
#include <time.h>

#include "ThreadLib.h"
#include "QueueLib.h"
#include "MMFLib.h"

#define MAX_LEN_TRACE_STRING_TMP 6000

#define TRACE_FILE_A   L"traceA.txt"
#define TRACE_FILE_U   L"traceU.txt"

enum tagTraceLevels
{
  LEVEL_0_DEBUG=0,
  LEVEL_1_CHI,
  LEVEL_2_GSS_LINK,
  LEVEL_3_SPARE,
  LEVEL_4_SPARE,
  LEVEL_5_SPARE,
  LEVEL_6_SPARE,
  LEVEL_7_EVENTS,
  LEVEL_8_WARNINGS,
  LEVEL_9_ERRORS,
  LEVEL_10_FRAMES,
  LEVEL_11_SPARE,
  LEVEL_12_SPARE,
  LEVEL_13_SPARE,
  LEVEL_14_SPARE,
	//
	NUM_TRACE_LEVELS
};

class CTraceLib : public CThreadLib  
{
public:
// define persistent data
struct STRUCT_SETTINGS
{
	DWORD dwSignature;
  BOOL bTraceLevel[NUM_TRACE_LEVELS];  // Tracing
	int nPersistDaysLogFile;             // Persistence of log files
};

// defines tracing message structure
struct STRUCT_TRACE_RECORDU
{
	int nInstanceID;
	wchar_t wszMsgInfo[MAX_LEN_TRACE_STRING_TMP+1];  
	wchar_t wszMsgTrace[MAX_LEN_TRACE_STRING_TMP+1]; 
};

struct STRUCT_TRACE_RECORDA
{
	int nInstanceID;
	char szMsgInfo[MAX_LEN_TRACE_STRING_TMP+1];  
	char szMsgTrace[MAX_LEN_TRACE_STRING_TMP+1]; 
};


public:
	CTraceLib(wchar_t* _pwszPathName, wchar_t* _pwszLogName, wchar_t* pwszTracePath, int nQueueSize, wchar_t* _pwszObjectName, int _nClosingtimeout=1000);
	virtual ~CTraceLib();

  inline void TRACE_LIB(BYTE byTrcLevel, LPCTSTR pwszSrcFile, UINT uLine, int nInstanceID, LPCTSTR pwszFormat,... );
  inline void TRACE_LIB(BYTE byTrcLevel, LPCSTR pszSrcFile, UINT uLine, int nInstanceID, LPCSTR pszFormat,... );
	inline BOOL IsObjectOk();
	inline BOOL IsLevelEnabled(int nLevel);
	BOOL ClearOldFiles();
  
private:
	void _SetDefault(DWORD _dwSignature);

public:
	CQueue m_qTraceU;
  CQueue m_qTraceA;

protected:
  STRUCT_SETTINGS* m_pData;
  CMMFLib m_mmf;

private:
	static DWORD WINAPI EngineThread(LPVOID pParam);
	CRITICAL_SECTION m_cs_syncro;
	wchar_t m_wszFileName[MAX_PATH+1];
	wchar_t m_wszTracePath[MAX_PATH+1];
	BOOL m_bObjectOk;
	char m_szTempString[MAX_LEN_TRACE_STRING_TMP+1];
};

//@ Returns the object status
inline BOOL CTraceLib::IsObjectOk()
{
	return m_bObjectOk;
}

//@ Checks if the trace level is enabled
inline BOOL CTraceLib::IsLevelEnabled(int nLevel)
{
	if( nLevel < 0 || nLevel >= NUM_TRACE_LEVELS)
	  return FALSE;

	return m_pData->bTraceLevel[nLevel];
}

//@ Unicode Tracer 
inline void CTraceLib::TRACE_LIB(BYTE byTrcLevel, LPCTSTR pwszSrcFile, UINT uLine, int nInstanceID, LPCTSTR pwszFormat,... )
{                                  
throw CFOLibException(0, L"NOT IMPLEMENTED");

	if(CThreadLib::m_bObjectOk != TRUE)
	  return;

	::EnterCriticalSection(&m_cs_syncro);

	try
	{
		STRUCT_TRACE_RECORDU traceRecU;
		va_list pArg;
		//timeb  traceTime;
		//struct  tm P_t;
		wchar_t wszSourceFile[MAX_PATH+1];

		if(IsLevelEnabled(byTrcLevel) == TRUE)
		{
			SYSTEMTIME tTime;

			int len = wcslen(pwszSrcFile);
			while(len > 0 && pwszSrcFile[len] != L'\\') len--;
			wcscpy_s(wszSourceFile, MAX_PATH, &pwszSrcFile[len+1]);

			memset(&traceRecU,0,sizeof(STRUCT_TRACE_RECORDU));

			// take the time
			GetLocalTime(&tTime);

			//ftime(&traceTime);
			//::localtime_s(&P_t, &traceTime.time);
			wsprintf(traceRecU.wszMsgInfo, L"|%02d:%02d:%02d|%3.3d|[#%d %s-%4.4d]",
							tTime.wHour,
							tTime.wMinute,
							tTime.wSecond,
							tTime.wMilliseconds,
							byTrcLevel,
							wszSourceFile,
							uLine);
/*
			wsprintf(traceRecU.wszMsgInfo, L"#%d %s-%4.4d [%02d:%02d:%02d,%3.3d]",
							byTrcLevel,
							wszSourceFile,
							uLine,
							P_t.tm_hour,
							P_t.tm_min,
							P_t.tm_sec,
							traceTime.millitm);
*/

			va_start(pArg, pwszFormat);
			vswprintf_s(traceRecU.wszMsgTrace, pwszFormat, pArg);
			va_end(pArg);

			if(wcslen(traceRecU.wszMsgTrace) >= MAX_PATH-3)
			{
				wcscpy_s(traceRecU.wszMsgTrace, MAX_PATH, L"Message too long !!!!\r\n");
			}
			else
			{
				wcscat_s(traceRecU.wszMsgTrace, MAX_PATH, L"\r\n");
			}
			
			traceRecU.wszMsgTrace[MAX_PATH]=NULL;

			traceRecU.nInstanceID=nInstanceID;
			//
			if( m_qTraceU.PutItem((BYTE*)&traceRecU) == 0)
			{
				CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, traceRecU.wszMsgTrace);
			}
		}
	}
	catch(CFOLibException& e)
	{
		CBaseLib::SetBit(ERR_TRACE_STRING_TOO_LONG, TRUE);
		CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"%.250s", e.m_szDescription);
	}
	catch(...)
	{
		CBaseLib::SetBit(ERR_EXCEPTION_TRACELIB, TRUE);
	}

	::LeaveCriticalSection(&m_cs_syncro);
}

//@ Ascii Tracer 
inline void CTraceLib::TRACE_LIB(BYTE byTrcLevel, LPCSTR pszSrcFile, UINT uLine, int nInstanceID, LPCSTR pszFormat,... )
{                                  
	if(CThreadLib::m_bObjectOk != TRUE)
	  return;

	::EnterCriticalSection(&m_cs_syncro);

	STRUCT_TRACE_RECORDA traceRecA;
	va_list pArg;
	//timeb  traceTime;
	//struct  tm P_t;
	char szSourceFile[MAX_PATH+1];

	int nLineFeedLen = 0;
	int nMAX = 0;
	int nPrinted=0;
	int nToPrint=0;

	try
	{
		int len = strlen(pszSrcFile);
		while(len > 0 && pszSrcFile[len] != '\\') len--;
		if (len>0)
			strcpy_s(szSourceFile, MAX_PATH, &pszSrcFile[len+1]);
		else
			strcpy_s(szSourceFile, MAX_PATH, pszSrcFile);

		if(IsLevelEnabled(byTrcLevel) == TRUE)
		{
			SYSTEMTIME tTime;

			memset(&traceRecA,0,sizeof(STRUCT_TRACE_RECORDA));

			// take the time
			GetLocalTime(&tTime);
//			ftime(&traceTime);
//			::localtime_s(&P_t, &traceTime.time);

			sprintf_s(traceRecA.szMsgInfo, MAX_PATH, "|%02d:%02d:%02d|%3.3d|[#%d %s-%4.4d]",
							tTime.wHour,
							tTime.wMinute,
							tTime.wSecond,
							tTime.wMilliseconds,
							byTrcLevel,
							szSourceFile,
							uLine);
/*
							P_t.tm_hour,
							P_t.tm_min,
							P_t.tm_sec,
							traceTime.millitm,
*/

			nLineFeedLen = strlen("\r\n");
			nMAX = (MAX_PATH-nLineFeedLen);
			nPrinted=0;
			nToPrint=0;

			//
			va_start(pArg, pszFormat);
			if(_vsnprintf_s(m_szTempString, MAX_LEN_TRACE_STRING_TMP, MAX_LEN_TRACE_STRING_TMP, pszFormat, pArg) < 0)
			{
				throw CFOLibException(0, L"TRACE STRING TOO LONG: %.100S...", m_szTempString);
			}
			va_end(pArg);
			//
			m_szTempString[MAX_LEN_TRACE_STRING_TMP] = 0;
			//
			int nTot=strlen(m_szTempString);
			//
			while(nTot > 0)
			{
				if(nTot > nMAX)
					nToPrint = (min (nTot, nMAX));
				else
					nToPrint = nTot;

				memcpy(traceRecA.szMsgTrace, &m_szTempString[nPrinted], nToPrint);
				memcpy(&traceRecA.szMsgTrace[nToPrint], "\r\n", nLineFeedLen);
				traceRecA.szMsgTrace[nToPrint+nLineFeedLen]=0;
				//
				traceRecA.nInstanceID=nInstanceID;

				if( m_qTraceA.PutItem((BYTE*)&traceRecA) == 0)
				{
					CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"%S", traceRecA.szMsgTrace);
				}
				//			
				nPrinted += nToPrint;
				nTot -= nToPrint;
			}
			if (byTrcLevel == 0)
				Sleep(1);
		}
	}
	catch(CFOLibException& e)
	{
		CBaseLib::SetBit(ERR_TRACE_STRING_TOO_LONG, TRUE);
		CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"%.250s", e.m_wszDescription);
	}
	catch(...)
	{
		CBaseLib::SetBit(ERR_EXCEPTION_TRACELIB, TRUE);
	}

	::LeaveCriticalSection(&m_cs_syncro);
}

#endif
