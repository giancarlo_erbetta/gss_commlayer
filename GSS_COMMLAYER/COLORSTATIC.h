/*@***********************************************************************
@
@  PROGETTO           :  Linjegods
@
@  FILE               :  ColorStatic.h
@
@  VERSIONE           :  1.0
@
@  TIPO               :  File Include
@
@  CREAZIONE          :  
@
@  AGGIORNAMENTO      :  /
@
@  AUTORI             :  G. Alessio
@
@  DESCRIZIONE        :  Gestisce la colorazione dei controlli statici
@
@  COMPILATORE        :  VisualC++
@
@  SISTEMA OP.        :  WIN NT
@
@  NOTE               :  
@************************************************************************/

#if !defined(AFX_COLORSTATIC_H__66D25331_5E8A_11D2_8971_00A0245BCF44__INCLUDED_)
#define AFX_COLORSTATIC_H__66D25331_5E8A_11D2_8971_00A0245BCF44__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ColorStatic.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorStatic window

class CColorStatic : public CStatic
{
public:
// Enum status colors
enum tagColorLink
{
  STOPPED_COLOR=0,
  WAITING_COLOR,
  WAITING_FOR_SYNC_COLOR,
  RUNNING_COLOR,
  ERROR_COLOR,
  DATA_COLOR,
};

// Construction
public:
	CColorStatic();
	virtual ~CColorStatic();

// Attributes
public:

protected:
    COLORREF m_crTextColor;
    COLORREF m_crBkColor;
    CBrush   m_brBkgnd;
    int      m_status;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorStatic)
	//}}AFX_VIRTUAL

// Implementation
public:
  void   SetTextColor(COLORREF);
  void   SetBkColor(COLORREF);
  void   ChangeStatus(int status);
  inline int GetCurrentStatus();

	// Generated message map functions
protected:
	//{{AFX_MSG(CColorStatic)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

inline int CColorStatic::GetCurrentStatus()
{
  return m_status;
}

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORSTATIC_H__66D25331_5E8A_11D2_8971_00A0245BCF44__INCLUDED_)
