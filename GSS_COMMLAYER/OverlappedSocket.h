//@////////////////////////////////////////////////////////////
//@
//@	Class:		COverlappedSocketxx
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@
//@	Created:	19 February 2001
//@
//@	Author:		Giuseppe Alessio
//@
//@	Updated:
//@ 31 January 2005
//@ Changed BOOL COverlappedSocketCL/SR::Create( const wchar_t* pwszRemoteIPAddr, int nPort )
//@
//@ 16 February 2005
//@ BUG fixed on COverlappedSocketCL/SR::WriteMessage (bRetVal)
//@
//@////////////////////////////////////////////////////////////

#ifndef _OVERLAPPEDSOCKET_H
#define _OVERLAPPEDSOCKET_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class COverlappedSocket
{
public:
enum tagIntErrors
{
  ERR_NO_ERROR=0,
  ERR_UNABLE_CREATE_EV,
  ERR_OBJECT_NOK,
  ERR_INVALID_IP_ADDR,
  ERR_WSA_SOCKET,
  ERR_WSA_SOCKET_OPTION,
  ERR_WSA_EVENT_SELECT,
  ERR_BIND_SOCKET,
  ERR_LISTEN_SOCKET,
  ERR_REMOTE_CLOSE,
  ERR_USER_ABORT,
  ERR_NOTHANDLED,
  ERR_EWOULDBLOCK,
  ERR_TIMEOUT,
  ERR_ENUM_NET_EV,
  ERR_SOCKET_READ,
  ERR_SOCKET_WRITE,
  ERR_NO_DATA,
	//
  ERR_CONNECT_SOCKET,
  ERR_CONNECT_IN_PROGRESS,
};

public:
	COverlappedSocket() { }
	virtual ~COverlappedSocket() { }

public:
  inline int GetLastError();
  inline HANDLE GetNetEventHandle();
  //
	virtual BOOL Create( const wchar_t* pwszLocalIPAddr, int nPort )=0;
  virtual BOOL OpenLink(HANDLE hAbortEv, DWORD dwTimeout )=0;
  virtual BOOL ReadMessage(WSABUF* pwsaBuffer, DWORD* pdwCarRead )=0;
  virtual BOOL ReceiveMessage(HANDLE hAbortEv, DWORD dwTimeout,  WSABUF* pwsaBuffer, DWORD* pdwCcarRead )=0;
  virtual BOOL WriteMessage(HANDLE hAbortEv, DWORD dwTimeout, WSABUF* pwsaBuffer, DWORD* pdwCarWritten )=0;

protected:
  BOOL              m_bIsObjectOk;
  tagIntErrors      m_lastError;
  SOCKET            m_socket;			
  SOCKET            m_socketIn;		
  SOCKADDR_IN       m_sockAddr;
  SOCKADDR_IN	      m_sockAddrIn;	
  LINGER            m_linger;
  WSAEVENT          m_hNetEvent;	
  WSAOVERLAPPED     m_ov;				  

  BOOL              m_bOption;
  int               m_nLen;
  int               m_nRetValue;
  int               m_nWSAError;

};

inline int COverlappedSocket::GetLastError()
{
  return m_lastError;
}

inline HANDLE COverlappedSocket::GetNetEventHandle()
{
  return m_hNetEvent;
}


class COverlappedSocketSR : public COverlappedSocket
{
public:
	COverlappedSocketSR();
	virtual ~COverlappedSocketSR();

public:
  virtual BOOL Create( const wchar_t* pwszLocalIPAddr, int nPort );
  virtual BOOL OpenLink(HANDLE hAbortEv, DWORD dwTimeout );
  virtual BOOL ReadMessage(WSABUF* pwsaBuffer, DWORD* pdwCarRead );
  virtual BOOL ReceiveMessage(HANDLE hAbortEv, DWORD dwTimeout,  WSABUF* pwsaBuffer, DWORD* pdwCcarRead );
  virtual BOOL WriteMessage(HANDLE hAbortEv, DWORD dwTimeout, WSABUF* pwsaBuffer, DWORD* pdwCarWritten );
};

class COverlappedSocketCL : public COverlappedSocket
{
public:
	COverlappedSocketCL();
	virtual ~COverlappedSocketCL();

public:
  virtual BOOL Create( const wchar_t* pwszRemoteIPAddr, int nPort );
  virtual BOOL OpenLink(HANDLE hAbortEv, DWORD dwTimeout );
  virtual BOOL ReadMessage(WSABUF* pwsaBuffer, DWORD* pdwCarRead );
  virtual BOOL ReceiveMessage(HANDLE hAbortEv, DWORD dwTimeout,  WSABUF* pwsaBuffer, DWORD* pdwCarRead );
  virtual BOOL WriteMessage(HANDLE hAbortEv, DWORD dwTimeout, WSABUF* pwsaBuffer, DWORD* pdwCarWritten );
};


class COverlappedSocketUDP : public COverlappedSocket
{
public:
	COverlappedSocketUDP();
	virtual ~COverlappedSocketUDP();

public:
  virtual BOOL Create( const wchar_t* pwszRemoteIPAddr, int nPort );
  virtual BOOL OpenLink(HANDLE hAbortEv, DWORD dwTimeout );
  virtual BOOL ReadMessage(WSABUF* pwsaBuffer, DWORD* pdwCarRead );
  virtual BOOL ReceiveMessage(HANDLE hAbortEv, DWORD dwTimeout,  WSABUF* pwsaBuffer, DWORD* pdwCarRead );
  virtual BOOL WriteMessage(HANDLE hAbortEv, DWORD dwTimeout, WSABUF* pwsaBuffer, DWORD* pdwCarWritten );
};


#endif
