#pragma once
#pragma pack(1)

#define MAX_MESSAGE_FIELDS 100
#define MAX_MESSAGE_LEN 512
#define MAX_MESSAGES 32
#define FORMAT_FILENAME_ROOT           L"MSG"
#define FORMAT_FILENAME_EXT           L".CSV"

#define STR_TYPE_VOID L"VOID"
#define STR_TYPE_I8 L"I8"
#define STR_TYPE_BYTE L"BYTE"
#define STR_TYPE_I16 L"I16"
#define STR_TYPE_WORD L"WORD"
#define STR_TYPE_I32 L"I32"
#define STR_TYPE_DWORD L"DWORD"
#define STR_TYPE_ASCII L"A"
#define STR_TYPE_HEXSTR_FX L"HF"	// Hex string FedEx format
#define STR_TYPE_HEXSTR L"HEX"	

enum ENUM_DATA_TYPES_ID
{
  ID_TYPE_VOID = 0,
  ID_TYPE_I8,
  ID_TYPE_I16,
  ID_TYPE_I32,
  ID_TYPE_ASCII,
  ID_TYPE_HEX,
  ID_TYPE_HF,
  NUM_DATA_TYPES
};

typedef struct _STRUCT_DATA_FORMAT
{
  int nTypeI;   // internal data format
  int nLenI;  	// internal data len
  int nOffsetI;  	// internal data offest
  int nTypeE;	// external data format
  int nLenE;	// external data len
  int nOffsetE;  	// external data offest
} STRUCT_DATA_FORMAT;

typedef struct _STRUCT_MSG_FORMAT
{
  STRUCT_DATA_FORMAT strFielFormat[MAX_MESSAGE_FIELDS];
  int nTotalLenI;
  int nTotalLenE;
  int nTotalFields;
	char szName[32];
	bool bLOG;
	BYTE	byDefaultMsgI[MAX_MESSAGE_LEN];
  BYTE	byDefaultMsgE[MAX_MESSAGE_LEN];
} STRUCT_MSG_FORMAT;

