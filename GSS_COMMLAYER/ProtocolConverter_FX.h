#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ProtocolConverter.h"

class CProtocolConverter_FX : public CProtocolConverter
{
public:
	//corpo della classe
	int GetGSSMsgIndex(char *sName);
	void ConvertGssToMsgI(char *sName, BYTE *byBufferSrc, BYTE *byBufferDst, int nSrcLen);

};

extern CProtocolConverter_FX* G_pProtocolConverter_FX;
