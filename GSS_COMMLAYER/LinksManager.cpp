#include "stdafx.h"
#include "LinksManager.h"
#include "AppTrace.h"
#include "AppSettings.h"
//#include "Externals.h"
#include "GSS_Link.h"

#include "ProtocolConverter_FX.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CLinksManager::CLinksManager(wchar_t* _pwszObjectName, int _nClosingtimeout) :
CThreadLib(_pwszObjectName, _nClosingtimeout),
m_QueueInternal(sizeof(STRUCT_MSGMANAGER_QUEUE_RECORD), 2000, L"Q_MSG_MGM_I"),
m_QueueExternal(sizeof(STRUCT_MSGMANAGER_QUEUE_RECORD), 2000, L"Q_MSG_MGM_E")
{

	// run worker thread
	CThreadLib::m_bObjectOk = CThreadLib::RunThread(CLinksManager::_EngineThread, THREAD_PRIORITY_NORMAL);
	
	memset(&m_csSTRUCT_COMMLAYER_DIAGNO, 0, sizeof(STRUCT_COMMLAYER_DIAGNO));

	m_nLastGSS1Port = 0;
	m_nLastGSS2Port = 0;

}

CLinksManager::~CLinksManager()
{
	CThreadLib::StopThread();
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ Worker thread 
//@
DWORD WINAPI CLinksManager::_EngineThread(LPVOID pParam)
{
	enum tagEventsName
	{
		ID_EXIT_EVENT = 0,
		ID_READ_QUEUE_I_EVENT,
		ID_READ_QUEUE_E_EVENT,
		NUM_EVENTS
	};

	enum tagStep
	{
		STEP_IDLE = 0,
		STEP_RUN,
		STEP_EXIT,

		NUM_STEPS
	};

	CLinksManager* _this = NULL;
	HANDLE hEvents[NUM_EVENTS];
	BOOL bRun = TRUE;
	tagStep step = STEP_IDLE;

	STRUCT_MSGMANAGER_QUEUE_RECORD csSTRUCT_MSGMANAGER_QUEUE_RECORD;

	try
	{
		_this = (CLinksManager*)pParam;

		hEvents[ID_EXIT_EVENT] = _this->GetExitHandle();
		hEvents[ID_READ_QUEUE_I_EVENT] = _this->m_QueueInternal.GetEvReadHandle();
		hEvents[ID_READ_QUEUE_E_EVENT] = _this->m_QueueExternal.GetEvReadHandle();

		_this->SetObjectReady();

		step = STEP_IDLE;

		// Loop
		while (bRun)
		{
			switch (step)
			{
			case STEP_IDLE:
				step = STEP_RUN;
			case STEP_RUN:
				// waiting 
				switch (::WaitForMultipleObjects(NUM_EVENTS, hEvents, FALSE, INFINITE))
				{
					// message from queue
				case ID_READ_QUEUE_I_EVENT:
					while (_this->m_QueueInternal.GetItem((BYTE*)&csSTRUCT_MSGMANAGER_QUEUE_RECORD))
					{
						_this->_MsgFromQueueI(&csSTRUCT_MSGMANAGER_QUEUE_RECORD);
						Sleep(10);
					}

				case ID_READ_QUEUE_E_EVENT:
					while (_this->m_QueueExternal.GetItem((BYTE*)&csSTRUCT_MSGMANAGER_QUEUE_RECORD))
					{
						_this->_MsgFromQueueE(&csSTRUCT_MSGMANAGER_QUEUE_RECORD);
						Sleep(10);
					}

					break;

					// exit request
				case ID_EXIT_EVENT:
				default:
					step = STEP_EXIT;
					break;
				}
				break;

			case STEP_EXIT:
				bRun = FALSE;
				break;

			}
			::Sleep(1);
		}
		return 0;
	}
	catch (...)
	{
		CBaseLib::SetBit(16, TRUE);
		CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"Exception on HOSTManager thread");
		return 1;
	}
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Handle messages from queue
//@
void CLinksManager::_MsgFromQueueI(STRUCT_MSGMANAGER_QUEUE_RECORD* pcsSTRUCT_MSGMANAGER_QUEUE_RECORD)
{
	// TODO
	// check for service telegram
	if (pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgCode == INT_MSG_CFG)
	{
		// MANAGE CONFIG message
		STRUCT_COMMLAYER_CFG *pSIZE_STRUCT_COMMLAYER_CFG;
		char szGSS1_IP_add[32];
		char szGSS2_IP_add[32];

		pSIZE_STRUCT_COMMLAYER_CFG = (STRUCT_COMMLAYER_CFG *)pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgData;
		sprintf(szGSS1_IP_add, "%S", G_pAppSettings->m_sGSS_IPAddr[0]);
		sprintf(szGSS2_IP_add, "%S", G_pAppSettings->m_sGSS_IPAddr[1]);
		
		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_7_EVENTS, __FILE__, __LINE__, 0, "RX config message %s %s %d", pSIZE_STRUCT_COMMLAYER_CFG->szGSS1_IP_add, pSIZE_STRUCT_COMMLAYER_CFG->szGSS2_IP_add, pSIZE_STRUCT_COMMLAYER_CFG->wGSS_port);

		if (memcmp(pSIZE_STRUCT_COMMLAYER_CFG->szGSS1_IP_add, szGSS1_IP_add, strlen(szGSS1_IP_add)) ||
			memcmp(pSIZE_STRUCT_COMMLAYER_CFG->szGSS2_IP_add, szGSS2_IP_add, strlen(szGSS2_IP_add)) ||
			strlen(pSIZE_STRUCT_COMMLAYER_CFG->szGSS1_IP_add) != strlen(szGSS1_IP_add) ||
			strlen(pSIZE_STRUCT_COMMLAYER_CFG->szGSS2_IP_add) != strlen(szGSS2_IP_add) ||
			pSIZE_STRUCT_COMMLAYER_CFG->wGSS_port != G_pAppSettings->m_nGSSPort)
		{
			// MANAGE NEW CONFIG
			_CloseGSS_Link();
			G_pAppSettings->m_sGSS_IPAddr[0] = CString(pSIZE_STRUCT_COMMLAYER_CFG->szGSS1_IP_add);
			G_pAppSettings->m_sGSS_IPAddr[1] = CString(pSIZE_STRUCT_COMMLAYER_CFG->szGSS2_IP_add);
			G_pAppSettings->m_nGSSPort = pSIZE_STRUCT_COMMLAYER_CFG->wGSS_port;
			G_pAppSettings->StoreXML(G_pAppSettings->m_sConfigFileName, TRUE);

			Sleep(1000);
			_OpenGSS_Link();
		}

	}
	else 	if (pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgCode < MAX_wMSGID_GSS)
	{
		STRUCT_GSS_LINK_QUEUE_RECORD csSTRUCT_GSS_LINK_QUEUE_RECORD;
		csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen = pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->wMsgLen;
		csSTRUCT_GSS_LINK_QUEUE_RECORD.wMSG_ID = pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgCode;
		if (csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen > sizeof(pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgData))
		{
			VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "_MsgFromQueueI TX message to GSS too big (len %d)!!!!", csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen);
		}
		else
		{
			//VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "_MsgFromQueueI OnTelegramReceived %d", pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgCode);
			memcpy(csSTRUCT_GSS_LINK_QUEUE_RECORD.szMsgData, pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgData, csSTRUCT_GSS_LINK_QUEUE_RECORD.wMsgLen);
			// send internal format to GSS1 and GSS2
			SendToGSS(&csSTRUCT_GSS_LINK_QUEUE_RECORD);
		}
	}
	else
	{
		// LOG something
		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "_MsgFromQueueI message to GSS not supported MSGID=%d !!!!", pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgCode);
	}
}

void CLinksManager::_MsgFromQueueE(STRUCT_MSGMANAGER_QUEUE_RECORD* pcsSTRUCT_MSGMANAGER_QUEUE_RECORD)
{
	STRUCT_CHI_NETLINK_QUEUE_RECORD csSTRUCT_CHI_NETLINK_QUEUE_RECORD;

	if (pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgCode == INT_MSG_DIAG)
	{
		// check linkstatus and send diagnostic
		if (IS_GSSLinksStatus_Changed() == TRUE)
			SendGSSLinksDiagno();
	}
	else
	{
		csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.byMsgID = pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgCode;
		csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen = pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->wMsgLen;
		memcpy(csSTRUCT_CHI_NETLINK_QUEUE_RECORD.szMsgData, pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->byMsgData, pcsSTRUCT_MSGMANAGER_QUEUE_RECORD->wMsgLen);
		// send to CHI
		G_pChIntNetLink->m_queueSend.PutItem((BYTE*)&csSTRUCT_CHI_NETLINK_QUEUE_RECORD);
	}

}


BOOL CLinksManager::InitApplication(void)
{
	CString	sString;
	CString sTmp;
	CString sFullAppname;

	::CoInitialize(NULL);

	m_csLastitemTime = CTime::GetCurrentTime();
	m_csLastitemTime += CTimeSpan(0, 0, 10, 0);

	// Create Mutex In Order To Detect MySelf

	_OpenInternal_Link();

	// REMOVE only for debug
	_OpenGSS_Link();

	return TRUE;
}

void CLinksManager::DestroyApplication(void)
{
	CString	sString;
	//
	// Release My Identification Name
	//
	// Application Exit
	/*
	STRUCT_SINGLEMSGCML	csSINGLEMSGCML;
	csSINGLEMSGCML.csHeader.wMsgId	 = wMSGID_GENERIC_KILL_THREAD;
	csSINGLEMSGCML.csHeader.wDataLen = 0;
	m_QueueAPPHND.PutItem((LPBYTE)&csSINGLEMSGCML,(DWORD)(SIZE_STRUCT_HEADERMSGCML+csSINGLEMSGCML.csHeader.wDataLen));
	*/

	_CloseInternal_Link();
	_CloseGSS_Link();



}

void CLinksManager::_OpenInternal_Link(void)
{
	STRUCT_CHI_NETLINK_INIT csInit;
	CString sIPAddr;

	sIPAddr = G_pAppSettings->m_sCHI_IPAddr;

	csInit.bSocketServer = TRUE;
	csInit.lLinkLossTimeout = 5000;
	csInit.nMaxRetries = 3;
	csInit.nClosingtimeout = 1000;
	csInit.nInstanceID = 1;
	csInit.nPortID = G_pAppSettings->m_nCHIPort;
	csInit.nQueueSendRecordSize = sizeof(STRUCT_CHI_NETLINK_QUEUE_RECORD);
	csInit.nQueueSendRecords = 500;
	csInit.nThreadPriority = THREAD_PRIORITY_NORMAL;
	wcsncpy_s(csInit.wszIPAddr, sIPAddr, sIPAddr.GetLength());

	//wcsncpy_s(csInit.wszIPAddr, sIPAddr.GetLength(), sIPAddr, sIPAddr.GetLength());

	swprintf_s(csInit.wszObjectName, L"COM%d_CHI_LINK", G_pAppSettings->m_nCommLayerID);
	swprintf_s(csInit.wszQueueSendName, L"COM%dCHI_LINKQ", G_pAppSettings->m_nCommLayerID);

	G_pChIntNetLink = new CChIntNetLink(csInit);

}

void CLinksManager::_OpenGSS_Link(void)
{
	STRUCT_GSS_LINK_INIT csInit;
	char szFormat[32];
	CString sGSS_IPAddr[2];

	sGSS_IPAddr[0] = G_pAppSettings->m_sGSS_IPAddr[0];
	sGSS_IPAddr[1] = G_pAppSettings->m_sGSS_IPAddr[1];

	if (G_pAppSettings->m_nGSSNumPorts > NUM_MAX_GSS_SOCKET)
		G_pAppSettings->m_nGSSNumPorts = NUM_MAX_GSS_SOCKET;

	for (int i = 0; i < G_pAppSettings->m_nGSSNumPorts; i++)
	{
		// GSS1 (HOST) LINKs
		csInit.nInstanceID = GSS_LINK_CHANNEL_GSS1 + i;
		swprintf_s(csInit.wszObjectName, L"GSS1_CH%d", i);
		csInit.lLinkLossTimeout = 4000;	// as GSS frequency (every 4 seconds)
		csInit.nMaxRetries = 3;
		csInit.nClosingtimeout = 1000;
		csInit.nThreadPriority = THREAD_PRIORITY_NORMAL;
		wcsncpy_s(csInit.wszIPAddr, sGSS_IPAddr[0], sGSS_IPAddr[0].GetLength());
		csInit.nPortID = G_pAppSettings->m_nGSSPort;
		csInit.nQueueSendRecordSize = sizeof(STRUCT_GSS_LINK_QUEUE_RECORD);
		csInit.nQueueSendRecords = 500;
		//wcsncpy_s(csInit.wszQueueSendName, MAX_PATH, L"QHCHM", MAX_PATH);
		swprintf_s(csInit.wszQueueSendName, L"C%d_QGSS1_CH%d", G_pAppSettings->m_nCommLayerID, i);
		//
		csInit.nFrameSizeLen = G_pAppSettings->m_nFrameSizeLen;
		csInit.nFrameSize = G_pAppSettings->m_nFrameSize;
		csInit.nGSS_ID = G_pAppSettings->m_nCommLayerID;
		sprintf_s(szFormat, "%%0%dd", csInit.nFrameSizeLen);
		sprintf(csInit.szFrameSize, szFormat, 200);
		//
		//wcsncpy_s(csInit.wszQueueSendFileName, MAX_PATH, PERSISTENT_QUEUE_FILES_PATH L"QHCHM.DAT", MAX_PATH);
		swprintf(csInit.wszQueueSendFileName, L"%sQ_C%dGSS1_CH%d.DAT", G_pAppSettings->m_sQueuePath, G_pAppSettings->m_nCommLayerID, i);

		//
		G_pGSS_Links[csInit.nInstanceID] = new CGSS_Link(csInit);
	}

	for (int i = 0; i < G_pAppSettings->m_nGSSNumPorts; i++)
	{
		// GSS2 (HOST) LINKs
		csInit.nInstanceID = GSS_LINK_CHANNEL_GSS2 + i;
		swprintf_s(csInit.wszObjectName, L"GSS2_CH%d", i);
		csInit.lLinkLossTimeout = 4000;	// as GSS frequency (every 4 seconds)
		csInit.nMaxRetries = 3;
		csInit.nClosingtimeout = 1000;
		csInit.nThreadPriority = THREAD_PRIORITY_NORMAL;
		wcsncpy_s(csInit.wszIPAddr, sGSS_IPAddr[1], sGSS_IPAddr[1].GetLength());
		csInit.nPortID = G_pAppSettings->m_nGSSPort;
		csInit.nQueueSendRecordSize = sizeof(STRUCT_GSS_LINK_QUEUE_RECORD);
		csInit.nQueueSendRecords = 500;
		//wcsncpy_s(csInit.wszQueueSendName, MAX_PATH, L"QHCHM", MAX_PATH);
		swprintf_s(csInit.wszQueueSendName, L"C%d_QGSS2_CH%d", G_pAppSettings->m_nCommLayerID, i);
		//
		csInit.nFrameSizeLen = G_pAppSettings->m_nFrameSizeLen;
		csInit.nFrameSize = G_pAppSettings->m_nFrameSize;
		sprintf_s(szFormat, "%%0%dd", csInit.nFrameSizeLen);
		sprintf(csInit.szFrameSize, szFormat, 200);
		//
		//wcsncpy_s(csInit.wszQueueSendFileName, MAX_PATH, PERSISTENT_QUEUE_FILES_PATH L"QHCHM.DAT", MAX_PATH);
		swprintf(csInit.wszQueueSendFileName, L"%sQ_C%dGSS2_CH%d.DAT", G_pAppSettings->m_sQueuePath, G_pAppSettings->m_nCommLayerID, i);

		//
		G_pGSS_Links[csInit.nInstanceID] = new CGSS_Link(csInit);
	}

}

void CLinksManager::_CloseInternal_Link(void)
{
	if (G_pChIntNetLink != NULL)
	{
		delete G_pChIntNetLink;
		G_pChIntNetLink = NULL;
	}
}

void CLinksManager::_CloseGSS_Link(void)
{

	for (int i = 0; i < G_pAppSettings->m_nGSSNumPorts; i++)
	{
		if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + i] != NULL)
		{
			delete G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + i];
			G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + i] = NULL;
		}
		//_UpdateLinkBox(LINK_STOPPED, BOX_GSS1CH0 + i);

		if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + i] != NULL)
		{
			delete G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + i];
			G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + i] = NULL;
		}
		//_UpdateLinkBox(LINK_STOPPED, BOX_GSS2CH0 + i);
	}

	m_csSTRUCT_COMMLAYER_DIAGNO.bGSS1_connected = FALSE;
	m_csSTRUCT_COMMLAYER_DIAGNO.bGSS2_connected = FALSE;

	SendGSSLinksDiagno();

}

void CLinksManager::SendToGSS(STRUCT_GSS_LINK_QUEUE_RECORD* pcsSTRUCT_GSS_LINK_QUEUE_RECORD)
{
	int nConnection;

	nConnection = m_nLastGSS1Port;

	m_nLastGSS1Port++;
	while (m_nLastGSS1Port < NUM_MAX_GSS_SOCKET)
	{
		if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + m_nLastGSS1Port] != NULL
			&& G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + m_nLastGSS1Port]->GetLinkStatus() == LINK_RUNNING)
			break;
		m_nLastGSS1Port++;
	}

	if (m_nLastGSS1Port >= NUM_MAX_GSS_SOCKET)
	{
		// not found restart from 0 
		m_nLastGSS1Port = 0;
		while (m_nLastGSS1Port <= nConnection)
		{
			if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + m_nLastGSS1Port] != NULL
				&& G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + m_nLastGSS1Port]->GetLinkStatus() == LINK_RUNNING)
				break;
			m_nLastGSS1Port++;
		}
	}

	if (m_nLastGSS1Port >= NUM_MAX_GSS_SOCKET)
	{
		m_nLastGSS1Port = 0;
	}

	if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + m_nLastGSS1Port] != NULL
		&& G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + m_nLastGSS1Port]->GetLinkStatus() == LINK_RUNNING)
	{
		G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + m_nLastGSS1Port]->m_queueSend.PutItem((BYTE*)pcsSTRUCT_GSS_LINK_QUEUE_RECORD);
	}

	nConnection = m_nLastGSS2Port;

	m_nLastGSS2Port++;
	while (m_nLastGSS2Port < NUM_MAX_GSS_SOCKET)
	{
		if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + m_nLastGSS2Port] != NULL
			&& G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + m_nLastGSS2Port]->GetLinkStatus() == LINK_RUNNING)
			break;
		m_nLastGSS2Port++;
	}

	if (m_nLastGSS2Port >= NUM_MAX_GSS_SOCKET)
	{
		// not found restart from 0 
		m_nLastGSS2Port = 0;
		while (m_nLastGSS2Port <= nConnection)
		{
			if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + m_nLastGSS2Port] != NULL
				&& G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + m_nLastGSS2Port]->GetLinkStatus() == LINK_RUNNING)
				break;
			m_nLastGSS2Port++;
		}
	}

	if (m_nLastGSS2Port >= NUM_MAX_GSS_SOCKET)
	{
		m_nLastGSS2Port = 0;
	}

	if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + m_nLastGSS2Port] != NULL
		&& G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + m_nLastGSS2Port]->GetLinkStatus() == LINK_RUNNING)
	{
		G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + m_nLastGSS2Port]->m_queueSend.PutItem((BYTE*)pcsSTRUCT_GSS_LINK_QUEUE_RECORD);
	}

}

BOOL CLinksManager::IS_GSSLinksStatus_Changed(void)
{
	STRUCT_COMMLAYER_DIAGNO csSTRUCT_COMMLAYER_DIAGNO;


	memset(&csSTRUCT_COMMLAYER_DIAGNO, 0, sizeof(csSTRUCT_COMMLAYER_DIAGNO));

	for (int i = 0; i < G_pAppSettings->m_nGSSNumPorts; i++)
	{
		if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + i] != NULL
			&& G_pGSS_Links[GSS_LINK_CHANNEL_GSS1 + i]->GetLinkStatus() == LINK_RUNNING)
		{
			csSTRUCT_COMMLAYER_DIAGNO.bGSS1_connected = TRUE; // at least one link OK
			break;
		}
	}

	for (int i = 0; i < G_pAppSettings->m_nGSSNumPorts; i++)
	{
		if (G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + i] != NULL
			&& G_pGSS_Links[GSS_LINK_CHANNEL_GSS2 + i]->GetLinkStatus() == LINK_RUNNING)
		{
			csSTRUCT_COMMLAYER_DIAGNO.bGSS2_connected = TRUE; // at least one link OK
			break;
		}
	}

	if (memcmp(&csSTRUCT_COMMLAYER_DIAGNO, &m_csSTRUCT_COMMLAYER_DIAGNO, sizeof(STRUCT_COMMLAYER_DIAGNO)))
	{
		memcpy(&m_csSTRUCT_COMMLAYER_DIAGNO, &csSTRUCT_COMMLAYER_DIAGNO, sizeof(STRUCT_COMMLAYER_DIAGNO));
		G_pCommLayerWnd->PostMessageW(WM_REFRESH_LINKS);
		return TRUE;
	}

	return FALSE;
}

void CLinksManager::SendGSSLinksDiagno(void)
{
	STRUCT_CHI_NETLINK_QUEUE_RECORD csSTRUCT_CHI_NETLINK_QUEUE_RECORD;

	csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.byMsgID = INT_MSG_DIAG;
	csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen = sizeof(STRUCT_COMMLAYER_DIAGNO);
	memcpy(csSTRUCT_CHI_NETLINK_QUEUE_RECORD.szMsgData, &m_csSTRUCT_COMMLAYER_DIAGNO, csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen);
	// send to CHI
	G_pChIntNetLink->m_queueSend.PutItem((BYTE*)&csSTRUCT_CHI_NETLINK_QUEUE_RECORD);
	VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_10_FRAMES, __FILE__, __LINE__, 0, "SendGSSLinksDiagno GSS1 connection=%d, GSS2 connection=%d!!!!", m_csSTRUCT_COMMLAYER_DIAGNO.bGSS1_connected, m_csSTRUCT_COMMLAYER_DIAGNO.bGSS2_connected);
}
