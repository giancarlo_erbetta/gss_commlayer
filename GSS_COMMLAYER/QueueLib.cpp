
#include "stdafx.h"
#include "BaseLib.h"
#include "QueueLib.h"
//
// VOLATILE QUEUE
//

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CQueue::CQueue [default]
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@    DO NOT USE !
//@
//@////////////////////////////////////////////////////////////////////
CQueue::CQueue(): m_lSizeRecord(256), m_lSizeQueue(10)
{
	m_bStatus=FALSE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CQueue::CQueue
//@
//@ Parameters:
//@		[IN]        _lRecordSize : size of each record 
//@		[IN]        _lQueueSize  : size of the whole queue ( number of records )
//@		[IN]        _pwszName    : queue name
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@    If ok m_bStatus == TRUE
//@
//@////////////////////////////////////////////////////////////////////
CQueue::CQueue(long _lRecordSize, long _lQueueSize, wchar_t* _pwszName)
: m_lSizeRecord(_lRecordSize), 
  m_lSizeQueue(_lQueueSize)
{
	::wcsncpy_s(m_wszName, MAX_PATH, _pwszName, MAX_PATH);
	m_wszName[MAX_PATH] = 0;

	m_bStatus=FALSE;
	m_lItemInQueue=0;
	m_lMaxItemInQueue=0;

	// bytes amount to be allocated
	m_lSizeByte=m_lSizeRecord*m_lSizeQueue;
	// alloca la memoria
	m_pbyTopQueue= (BYTE*) malloc(m_lSizeByte);
	//
	if(m_pbyTopQueue == NULL)
		throw CFOLibException(0, L"CQueue: m_pbyTopQueue == NULL");

	// clear and initialize
	memset(m_pbyTopQueue, 0,(m_lSizeByte));
	m_pbyRecordPut=m_pbyRecordGet=m_pbyTopQueue;
	m_pbyBottomQueue=m_pbyTopQueue+m_lSizeByte;

	try
	{
		InitializeCriticalSection(&m_cs_syncro);
	}
	catch(...)
	{
		throw CFOLibException(0, L"CQueue: InitializeCriticalSection failed");
	}

  m_hEvRead  = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	if( m_hEvRead == NULL)
		throw CFOLibException(0, L"CQueue: m_hEvRead == NULL");

	// OK!
	m_bStatus=TRUE;
	m_bMemFull=FALSE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Destructor CQueue::~CQueue
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CQueue::~CQueue()
{
  if(m_bStatus == TRUE && m_pbyTopQueue != NULL)
  {
	  free(m_pbyTopQueue);
  }

	::DeleteCriticalSection(&m_cs_syncro);
  ::CloseHandle(m_hEvRead);
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Put an item in queue
//@
//@ Parameters:
//@		[IN]        _pbyRecord : record pointer
//@				
//@ Return value:
//@    1 if success, 0 otherwise
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
int CQueue::PutItem(BYTE* _pbyRecord)
{
	// quit if not ok
	if(m_bStatus != TRUE)
	{
		return 0;
	}

	::EnterCriticalSection(&m_cs_syncro);

	// quit if the queue is full
	if(m_lItemInQueue >= m_lSizeQueue)
	{
		CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"Queue [%s] is full!", m_wszName);
		CBaseLib::SetBit(ERR_FULL_QUEUE, TRUE);
		//
		m_bMemFull=1;
		::LeaveCriticalSection(&m_cs_syncro);
		return 0;
	}
	// copy the record and move the pointer up
	::memcpy(m_pbyRecordPut,_pbyRecord,m_lSizeRecord);
	m_pbyRecordPut += m_lSizeRecord;
	if(m_pbyRecordPut >= m_pbyBottomQueue)
		m_pbyRecordPut=m_pbyTopQueue;

	// increment the number of elements in queue
	m_lItemInQueue++;
	// store the pick 
	if(m_lItemInQueue > m_lMaxItemInQueue)
		m_lMaxItemInQueue=m_lItemInQueue;

  // set data in queue event
  ::SetEvent(m_hEvRead);

	::LeaveCriticalSection(&m_cs_syncro);
	
	// ok!
	return 1;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Get an item from the queue
//@
//@ Parameters:
//@		[IN,OUT]        _pbyRecord : destination record pointer
//@				
//@ Return value:
//@    1 if success, 0 otherwise
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
int CQueue::GetItem(BYTE* _pbyRecord)
{
int nRet=0;

	// quit if not ok
	if(m_bStatus != TRUE)
	{
		return 0;
	}

	::EnterCriticalSection(&m_cs_syncro);

	// if there are elements in queue
	if(m_lItemInQueue > 0)
	{
		// copy the record and move up the pointer
		::memcpy(_pbyRecord,m_pbyRecordGet,m_lSizeRecord);
		m_pbyRecordGet += m_lSizeRecord;
		if(m_pbyRecordGet >= m_pbyBottomQueue)
			m_pbyRecordGet=m_pbyTopQueue;

	  // decrement the number of elements in queue
		m_lItemInQueue--;

    // Reset data in queue event if no items
    if(m_lItemInQueue <= 0)
      ::ResetEvent(m_hEvRead);
		// ok!
		nRet = 1;
	}
	// queue is empty!
	else 
	{
    // Reset data in queue event 
    ::ResetEvent(m_hEvRead);
		nRet = 0;
	}

	::LeaveCriticalSection(&m_cs_syncro);
	
	return nRet;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Get a copy of an item from the queue
//@
//@ Parameters:
//@		[IN,OUT]        _pbyRecord : destination record pointer
//@				
//@ Return value:
//@    1 if success, 0 otherwise
//@
//@ Note:
//@    It doesn't delete the record from the queue
//@
//@////////////////////////////////////////////////////////////////////
int CQueue::QueryNextItem(BYTE* _pbyRecord)
{
int nRet=0;
	
	// quit if not ok
	if(m_bStatus != TRUE)
		return nRet;

	::EnterCriticalSection(&m_cs_syncro);
	// if there are elements in queue
	if(m_lItemInQueue > 0)
	{
		// copy the record
		::memcpy(_pbyRecord,m_pbyRecordGet,m_lSizeRecord);
		nRet=1;	
	}
	::LeaveCriticalSection(&m_cs_syncro);

	return nRet;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Flushes the whole queue
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@    
//@
//@////////////////////////////////////////////////////////////////////
void CQueue::FlushQueue()
{
	// quit if not ok
	if(m_bStatus != TRUE)
	{
		return;
	}

	::EnterCriticalSection(&m_cs_syncro);

	// set up
	m_lItemInQueue=0;
	m_lMaxItemInQueue=0;
	m_pbyRecordPut=m_pbyRecordGet=m_pbyTopQueue;
	m_pbyBottomQueue=m_pbyTopQueue+m_lSizeByte;
  // Reset data in queue event
  ::ResetEvent(m_hEvRead);

	::LeaveCriticalSection(&m_cs_syncro);
}

//
// PERSISTENT QUEUE
//

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CPersistentQueue::CPersistentQueue [default]
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@    DO NOT USE !
//@
//@////////////////////////////////////////////////////////////////////
CPersistentQueue::CPersistentQueue(): m_lSizeRecord(256), m_lSizeQueue(10)
{
	m_bStatus=FALSE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CPersistentQueue::CPersistentQueue
//@
//@ Parameters:
//@		[IN]        _lRecordSize : size of each record 
//@		[IN]        _lQueueSize  : size of the whole queue ( number of records )
//@		[IN]        _pwszName    : queue name
//@		[IN]        _pwszFileName : Memory mapped file name
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@    If ok m_bStatus == TRUE
//@
//@////////////////////////////////////////////////////////////////////
CPersistentQueue::CPersistentQueue(long _lRecordSize, long _lQueueSize, wchar_t* _pwszName, wchar_t* _pwszFileName)
: m_lSizeRecord(_lRecordSize), 
  m_lSizeQueue(_lQueueSize)
{
  ::wcsncpy_s(m_wszName, MAX_PATH, _pwszName, MAX_PATH);
	m_wszName[MAX_PATH] = 0;

  ::wcsncpy_s(m_wszFileName, MAX_PATH, _pwszFileName, MAX_PATH);
	m_wszFileName[MAX_PATH] = 0;

	m_bStatus=FALSE;
  m_pbyTopQueue = NULL;
  m_pbyRecordPut = NULL;
  m_pbyRecordGet = NULL;
	m_pbyBottomQueue = NULL;
  m_pbyData = NULL;
  m_pcsQInfo = NULL;

	// bytes amount to be allocated
	m_lSizeByte=sizeof(tagQueueInfo)+(m_lSizeRecord*m_lSizeQueue);

	// open and map th DB ( create it if not exists )
  m_pbyData = (BYTE*)m_mmf.Open( m_wszFileName, m_wszName, m_lSizeByte, TRUE, TRUE);
  //
  if(m_pbyData == NULL)
  {
		#ifdef USE_FOLIBRARY_ENVIRONMENT	
    _Module.LogEvent(EVENTLOG_ERROR_TYPE, L"Can't open/map [%s] file", m_wszFileName);
		#endif
		return;
  } 

  m_pcsQInfo = (tagQueueInfo*)m_pbyData;

  DWORD dwFileSize = m_mmf.GetFileSize();
  // check if new/changed
  if(m_pcsQInfo->dwSignature != dwFileSize)
  {
    // Clear
    ::ZeroMemory(m_pbyData, m_lSizeByte);
    m_pcsQInfo->dwSignature = dwFileSize;
  }

  m_pbyTopQueue = m_pbyData+sizeof(tagQueueInfo);
  m_pbyRecordGet = m_pbyTopQueue+(m_pcsQInfo->lRdIndex*m_lSizeRecord);
  m_pbyRecordPut = m_pbyTopQueue+(m_pcsQInfo->lWrIndex*m_lSizeRecord);
	m_pbyBottomQueue = m_pbyTopQueue+m_lSizeByte;

	try
	{
		InitializeCriticalSection(&m_cs_syncro);
	}
	catch(...)
	{
		return;
	}

  m_hEvRead  = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	if( m_hEvRead == NULL)
		return;

  // if pending messages set data in queue event
  if(GetQueuelen() > 0)
    ::SetEvent(m_hEvRead);

	// OK!
	m_bStatus=TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Destructor CPersistentQueue::~CPersistentQueue
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CPersistentQueue::~CPersistentQueue()
{
  if(m_pbyTopQueue != NULL)
  {
    m_mmf.Flush();
    m_mmf.Close();
  }

	if( m_bStatus == TRUE)
  {
	  ::DeleteCriticalSection(&m_cs_syncro);
    ::CloseHandle(m_hEvRead);
  }
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Put an item in queue
//@
//@ Parameters:
//@		[IN]        _pbyRecord : record pointer
//@				
//@ Return value:
//@    1 if success, 0 otherwise
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
int CPersistentQueue::PutItem(BYTE* _pbyRecord)
{
	// quit if not ok
	if(m_bStatus != TRUE)
	{
		return 0;
	}

	::EnterCriticalSection(&m_cs_syncro);

	// quit if the queue is full
	if(m_pcsQInfo->lItemInQueue >= m_lSizeQueue)
	{
		CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"Queue [%s] is full!", m_wszName);
		CBaseLib::SetBit(ERR_FULL_QUEUE, TRUE);

		m_pcsQInfo->bMemFull=1;
		::LeaveCriticalSection(&m_cs_syncro);
		return 0;
	}
	// copy the record and move the pointer up
	::memcpy(m_pbyRecordPut,_pbyRecord,m_lSizeRecord);
	m_pbyRecordPut += m_lSizeRecord;
  m_pcsQInfo->lWrIndex++;

	if(m_pbyRecordPut >= m_pbyBottomQueue
  || m_pcsQInfo->lWrIndex >= m_lSizeQueue)
  {
		m_pbyRecordPut=m_pbyTopQueue;
    m_pcsQInfo->lWrIndex=0;
  }

	// increment the number of elements in queue
  m_pcsQInfo->lItemInQueue++;
  
	// store the pick 
	if(m_pcsQInfo->lItemInQueue > m_pcsQInfo->lMaxItemInQueue)
		m_pcsQInfo->lMaxItemInQueue = m_pcsQInfo->lItemInQueue;

  // set data in queue event
  ::SetEvent(m_hEvRead);

	::LeaveCriticalSection(&m_cs_syncro);
	
	// ok!
	return 1;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Get an item from the queue
//@
//@ Parameters:
//@		[IN,OUT]        _pbyRecord : destination record pointer
//@				
//@ Return value:
//@    1 if success, 0 otherwise
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
int CPersistentQueue::GetItem(BYTE* _pbyRecord)
{
int nRet=0;

	// quit if not ok
	if(m_bStatus != TRUE)
	{
		return 0;
	}

	::EnterCriticalSection(&m_cs_syncro);

	// if there are elements in queue
	if(m_pcsQInfo->lItemInQueue > 0)
	{
		// copy the record and move up the pointer
		::memcpy(_pbyRecord,m_pbyRecordGet,m_lSizeRecord);
		m_pbyRecordGet += m_lSizeRecord;
		m_pcsQInfo->lRdIndex++;

    if(m_pbyRecordGet >= m_pbyBottomQueue
    || m_pcsQInfo->lRdIndex >= m_lSizeQueue)
    {
			m_pbyRecordGet=m_pbyTopQueue;
      m_pcsQInfo->lRdIndex=0;
    }

	  // decrement the number of elements in queue
		m_pcsQInfo->lItemInQueue--;

    // Reset data in queue event if no items
    if(m_pcsQInfo->lItemInQueue <= 0)
      ::ResetEvent(m_hEvRead);
		// ok!
		nRet = 1;
	}
	// queue is empty!
	else 
	{
    // Reset data in queue event 
    ::ResetEvent(m_hEvRead);
		nRet = 0;
	}

	::LeaveCriticalSection(&m_cs_syncro);
	
	return nRet;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Get a copy of an item from the queue
//@
//@ Parameters:
//@		[IN,OUT]        _pbyRecord : destination record pointer
//@				
//@ Return value:
//@    1 if success, 0 otherwise
//@
//@ Note:
//@    It doesn't delete the record from the queue
//@
//@////////////////////////////////////////////////////////////////////
int CPersistentQueue::QueryNextItem(BYTE* _pbyRecord)
{
int nRet=0;
	
	// quit if not ok
	if(m_bStatus != TRUE)
		return nRet;

	::EnterCriticalSection(&m_cs_syncro);
	// if there are elements in queue
	if(m_pcsQInfo->lItemInQueue > 0)
	{
		// copy the record
		::memcpy(_pbyRecord,m_pbyRecordGet,m_lSizeRecord);
		nRet=1;	
	}
	::LeaveCriticalSection(&m_cs_syncro);

	return nRet;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Flushes the whole queue
//@
//@ Parameters:
//@		none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@    
//@
//@////////////////////////////////////////////////////////////////////
void CPersistentQueue::FlushQueue()
{
	// quit if not ok
	if(m_bStatus != TRUE)
	{
		return;
	}

	::EnterCriticalSection(&m_cs_syncro);

  m_pcsQInfo->lWrIndex=0;
  m_pcsQInfo->lRdIndex=0;
	m_pcsQInfo->lItemInQueue=0;
	m_pcsQInfo->lMaxItemInQueue=0;
	m_pcsQInfo->bMemFull=0;
  //
  m_pbyTopQueue = m_pbyData+sizeof(tagQueueInfo);
  m_pbyRecordGet = m_pbyTopQueue;
  m_pbyRecordPut = m_pbyTopQueue;
	m_pbyBottomQueue = m_pbyTopQueue+m_lSizeByte;

  // Reset data in queue event
  ::ResetEvent(m_hEvRead);

	::LeaveCriticalSection(&m_cs_syncro);
}



