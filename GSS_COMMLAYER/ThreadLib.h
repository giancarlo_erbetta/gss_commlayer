//@////////////////////////////////////////////////////////////
//@
//@	Class:		CThreadLib
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@
//@	Created:	06 February 2001
//@
//@	Author:		Giuseppe Alessio
//@
//@	Updated:	
//@ 28 January 2005
//@ Support to BaseLib
//@
//@ 17 February 2005
//@ - Added SuspendThread() and ResumeThread()
//@
//@
//@
//@////////////////////////////////////////////////////////////

#ifndef _CTHREADLIB_H
#define _CTHREADLIB_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CThreadLib
{
public:
  CThreadLib(wchar_t* _pwszObjectName, int _nClosingtimeout);
  virtual ~CThreadLib();

public:
  inline wchar_t* GetObjectName();
  inline BOOL   IsObjectOk();
  inline DWORD  WaitForObjectReady(int _nTimeout);
  inline BOOL   SuspendThread();
  inline BOOL   ResumeThread();

protected:
  void   ErrorClosingThreadHandler();
  BOOL   RunThread(LPTHREAD_START_ROUTINE pThrFunc, int nPriority);
  BOOL   StopThread();
	inline HANDLE GetExitHandle();
  inline DWORD  GetThreadID();
  inline HANDLE GetThreadHandle();
  inline void   SetObjectReady();

protected:
	wchar_t m_wszObjectName[MAX_PATH+1];
  BOOL   m_bObjectOk;
  int    m_nClosingTimeout;

private:
  HANDLE m_hEvExitThread;  // not signalled, manual reset
  HANDLE m_hEvObjectReady; // not signalled, manual reset
  HANDLE m_hThread;
  DWORD  m_dwIdThread;
};

inline HANDLE CThreadLib::GetExitHandle()
{
  return m_hEvExitThread;
}

inline DWORD CThreadLib::GetThreadID()
{
  return m_dwIdThread;
}

inline HANDLE CThreadLib::GetThreadHandle()
{
  return m_hThread;
}

inline BOOL CThreadLib::IsObjectOk()
{
  return m_bObjectOk;
}

inline void CThreadLib::SetObjectReady()
{
  ::SetEvent(m_hEvObjectReady);
}

inline DWORD CThreadLib::WaitForObjectReady(int _nTimeout)
{
  if( m_bObjectOk != TRUE) return WAIT_TIMEOUT;

  return ::WaitForSingleObject(m_hEvObjectReady, _nTimeout);
}

inline wchar_t* CThreadLib::GetObjectName()
{
  return m_wszObjectName;
}

inline BOOL CThreadLib::SuspendThread()
{
  ::SuspendThread(m_hThread);
	return TRUE;
}

inline BOOL CThreadLib::ResumeThread()
{
  ::ResumeThread(m_hThread);
	return TRUE;
}



#endif
