// GSS_Link.h: interface for the CGSS_Link class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ThreadLib.h"
#include "MMFLib.h"
#include "QueueLib.h"
#include "AppTrace.h"
#include "GSS_ProtocolDef.h"

class CGSS_Link : public CThreadLib  
{
public:

	CGSS_Link(STRUCT_GSS_LINK_INIT& csSTRUCT_GSS_LINK_INIT);
	virtual ~CGSS_Link();
	//
	inline void GetLDI(STRUCT_GSS_LINK_DEBUG_INFO* pLDI);
	inline void ResetLDI();
	inline int GetLinkStatus();


public:
  CPersistentQueue m_queueSend;

protected:
	virtual BOOL OnCreate()           {/*VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_7_EVENTS,__FILE__,__LINE__,0, "%s Create soket on port %d", m_szObjectName, csSTRUCT_GSS_Link_INIT.nPortID);*/ return TRUE;}
	virtual BOOL OnCreateError()			{/*VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "%s Create failed", m_szObjectName); */ return TRUE; }
	virtual BOOL OnMemoryOverflow()		{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s Memory overflow", m_szObjectName); return TRUE;}
	virtual BOOL OnOpenFailed()				{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s OpenLink failed", m_szObjectName); return TRUE;}
	virtual BOOL OnShutdown()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s Shut Down", m_szObjectName); return TRUE;}
	virtual BOOL OnRxError()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s RX Error", m_szObjectName); m_LDI.nNumRXerror++; return TRUE;}
	virtual BOOL OnRxEmpty()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s RX Empty", m_szObjectName); return TRUE;}
	virtual BOOL OnTxTimeout()				{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s TX Timeout", m_szObjectName); m_LDI.nNumTXerror++; return TRUE;}
	virtual BOOL OnTxError()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|DACO<->GSS||||%s TX Error", m_szObjectName); m_LDI.nNumTXerror++; return TRUE;}
	
	virtual BOOL OnConnected();
	virtual BOOL OnConnectionBroken();
	virtual BOOL OnReceiveBegin();
	virtual BOOL OnReceiveEnd();
	virtual BOOL OnSendBegin();
	virtual BOOL OnSendEnd();
	virtual BOOL OnHBTimeout();
	//
	virtual BOOL OnTelegramReceived(int nTelegramLen);

protected:
	static DWORD WINAPI EngineThread(LPVOID pParam);
	STRUCT_GSS_LINK_INIT m_csSTRUCT_GSS_LINK_INIT;
	
	const int   SIZE_BUF_TX;
	const int   SIZE_BUF_RX;

	DWORD m_dwWriteBytes; 
	DWORD m_dwReadBytes; 
	WSABUF m_wsaTxData;
	WSABUF m_wsaRxData;
	BYTE*  m_byDatagramBuffer;

  int m_linkStatus;
	char m_szObjectName[MAX_PATH+1];
	int m_nInstanceID;
	STRUCT_GSS_LINK_DEBUG_INFO m_LDI;
	BOOL m_bLinkON;
	int m_nTxWithoutRxCounter;

  int m_nBuildDatagramStep;
  int m_nDatagramWriterIndex;
  int m_nDatagramLength;
	int	m_nGSS_ID;

  DWORD m_dwRXBufIndex;
  int m_nNumRxGroupedTelegrams;

};

//@ Return the Link Debug Information
inline void CGSS_Link::GetLDI(STRUCT_GSS_LINK_DEBUG_INFO* pLDI)
{
	// persistent data
	wcsncpy_s(m_LDI.wszObjectName, MAX_PATH, m_csSTRUCT_GSS_LINK_INIT.wszObjectName, MAX_PATH);
	wcsncpy_s(m_LDI.wszIPAddress, MAX_PATH, m_csSTRUCT_GSS_LINK_INIT.wszIPAddr, MAX_PATH);
	m_LDI.nPortID = m_csSTRUCT_GSS_LINK_INIT.nPortID;
	m_LDI.lLinkLossTimeout = m_csSTRUCT_GSS_LINK_INIT.lLinkLossTimeout;
	m_LDI.nMaxRetries = m_csSTRUCT_GSS_LINK_INIT.nMaxRetries;
	m_LDI.nNumTXRetries = m_nTxWithoutRxCounter;
	// fetch data
//	m_LDI.nProgTx = m_nProgTx;
//	m_LDI.nProgRx = m_nProgRx;
	m_LDI.nQueueLen = m_queueSend.GetQueuelen();
	m_LDI.nQueueSize = m_queueSend.GetSizeQueue();
	m_LDI.nQueuePeak = 	m_queueSend.GetMaxQueuelen();

	*pLDI = m_LDI;
}

//@ Clears the Link Debug Informations
inline void CGSS_Link::ResetLDI()
{
	::ZeroMemory(&m_LDI, sizeof(STRUCT_GSS_LINK_DEBUG_INFO));
	m_queueSend.SetMaxQueuelen(0);
}

//@ Return the Link Status
inline int CGSS_Link::GetLinkStatus()
{
	return m_linkStatus;
}


extern CGSS_Link* G_pGSS_Links[NUM_MAX_GSS_LINK_CONNECTIONS];

