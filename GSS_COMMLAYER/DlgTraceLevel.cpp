// DlgTraceLevel.cpp : implementation file
//

#include "stdafx.h"
#include "GSS_COMMLAYER.h"
#include "DlgTraceLevel.h"
#include "afxdialogex.h"


// CDlgTraceLevel dialog

IMPLEMENT_DYNAMIC(CDlgTraceLevel, CDialogEx)

CDlgTraceLevel::CDlgTraceLevel(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlgTraceLevel::IDD, pParent)
{

}

CDlgTraceLevel::~CDlgTraceLevel()
{
}

void CDlgTraceLevel::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BOOL CDlgTraceLevel::OnInitDialog()
{
	CButton *_ctlCheck;
	CStatic *_ctlStatic;
	CString sDay;

	CDialog::OnInitDialog();

	if (G_pTrace == NULL)
		return FALSE;


	m_csSettings = G_pTrace->GetData();

	_ctlStatic = (CStatic*)GetDlgItem(IDC_EDIT1);
	sDay.Format(L"%d", m_csSettings.nPersistDaysLogFile);
	_ctlStatic->SetWindowTextW(sDay);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK1);
	if (m_csSettings.bTraceLevel[0])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK2);
	if (m_csSettings.bTraceLevel[1])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK3);
	if (m_csSettings.bTraceLevel[2])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK4);
	if (m_csSettings.bTraceLevel[3])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK5);
	if (m_csSettings.bTraceLevel[4])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK6);
	if (m_csSettings.bTraceLevel[5])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK7);
	if (m_csSettings.bTraceLevel[6])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK8);
	if (m_csSettings.bTraceLevel[7])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK9);
	if (m_csSettings.bTraceLevel[8])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK10);
	if (m_csSettings.bTraceLevel[9])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK11);
	if (m_csSettings.bTraceLevel[10])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK12);
	if (m_csSettings.bTraceLevel[11])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK13);
	if (m_csSettings.bTraceLevel[12])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK14);
	if (m_csSettings.bTraceLevel[13])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);

	_ctlCheck = (CButton*)GetDlgItem(IDC_CHECK15);
	if (m_csSettings.bTraceLevel[14])
		_ctlCheck->SetCheck(BST_CHECKED);
	else
		_ctlCheck->SetCheck(BST_UNCHECKED);


	return TRUE;
}

BEGIN_MESSAGE_MAP(CDlgTraceLevel, CDialogEx)
	ON_BN_CLICKED(IDC_CHECK1, &CDlgTraceLevel::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, &CDlgTraceLevel::OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CHECK3, &CDlgTraceLevel::OnBnClickedCheck3)
	ON_BN_CLICKED(IDC_CHECK4, &CDlgTraceLevel::OnBnClickedCheck4)
	ON_BN_CLICKED(IDC_CHECK5, &CDlgTraceLevel::OnBnClickedCheck5)
	ON_BN_CLICKED(IDC_CHECK6, &CDlgTraceLevel::OnBnClickedCheck6)
	ON_BN_CLICKED(IDC_CHECK7, &CDlgTraceLevel::OnBnClickedCheck7)
	ON_BN_CLICKED(IDC_CHECK8, &CDlgTraceLevel::OnBnClickedCheck8)
	ON_BN_CLICKED(IDC_CHECK9, &CDlgTraceLevel::OnBnClickedCheck9)
	ON_BN_CLICKED(IDC_CHECK10, &CDlgTraceLevel::OnBnClickedCheck10)
	ON_BN_CLICKED(IDC_CHECK11, &CDlgTraceLevel::OnBnClickedCheck11)
	ON_BN_CLICKED(IDC_CHECK12, &CDlgTraceLevel::OnBnClickedCheck12)
	ON_BN_CLICKED(IDC_CHECK13, &CDlgTraceLevel::OnBnClickedCheck13)
	ON_BN_CLICKED(IDC_CHECK14, &CDlgTraceLevel::OnBnClickedCheck14)
	ON_BN_CLICKED(IDC_CHECK15, &CDlgTraceLevel::OnBnClickedCheck15)
	ON_BN_CLICKED(IDOK, &CDlgTraceLevel::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgTraceLevel message handlers


void CDlgTraceLevel::OnBnClickedCheck1()
{

	m_csSettings.bTraceLevel[0] ^= 1;

}


void CDlgTraceLevel::OnBnClickedCheck2()
{
	m_csSettings.bTraceLevel[1] ^= 1;
}


void CDlgTraceLevel::OnBnClickedCheck3()
{
	m_csSettings.bTraceLevel[2] ^= 1;
}


void CDlgTraceLevel::OnBnClickedCheck4()
{
	m_csSettings.bTraceLevel[3] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck5()
{
	m_csSettings.bTraceLevel[4] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck6()
{
	m_csSettings.bTraceLevel[5] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck7()
{
	m_csSettings.bTraceLevel[6] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck8()
{
	m_csSettings.bTraceLevel[7] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck9()
{
	m_csSettings.bTraceLevel[8] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck10()
{
	m_csSettings.bTraceLevel[9] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck11()
{
	m_csSettings.bTraceLevel[10] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck12()
{
	m_csSettings.bTraceLevel[11] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck13()
{
	m_csSettings.bTraceLevel[12] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck14()
{
	m_csSettings.bTraceLevel[13] ^= 1;
}

void CDlgTraceLevel::OnBnClickedCheck15()
{
	m_csSettings.bTraceLevel[14] ^= 1;
}


void CDlgTraceLevel::OnBnClickedOk()
{
	int nDay;
	CString sDay;
	CStatic *_ctlStatic;

	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();

	if (G_pTrace == NULL)
		return;

	_ctlStatic = (CStatic*)GetDlgItem(IDC_EDIT1);
	sDay.SetString(L"0");
	if (_ctlStatic->GetWindowTextLengthW())
		_ctlStatic->GetWindowTextW(sDay);

	nDay = _wtoi(sDay);

	if (nDay > 0)
		m_csSettings.nPersistDaysLogFile = nDay;

	G_pTrace->PutData(m_csSettings);

}
