// AppSettings.h: interface for the CAppSettings class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define XML_FILE_ROOT _T("FILE")
#define XML_FILE_UPDATE	_T("LastUpdate")
#define XML_FILE_DATE	_T("LastSave")

#define XML_CONFIG_ROOT _T("Configuration")

#define XML_CONFIG_GSS1_IP _T("GSS1_IP_ADDRESS")
#define XML_CONFIG_GSS2_IP _T("GSS2_IP_ADDRESS")
#define XML_CONFIG_GSS_IP_PORT _T("GSS_IP_PORT")
#define XML_CONFIG_GSS_NUM_PORT _T("GSS_NUM_PORT")
#define XML_CONFIG_CHI_IP _T("CHI_IP_ADDRESS")
#define XML_CONFIG_CHI_IP_PORT _T("CHI_IP_PORT")
#define XML_CONFIG_SORTER_EQUIPMENT_ID _T("SORTER_EQUIPMENT_ID")

#define XML_CONFIG_FRAME_SIZELEN _T("FrameSizeLen")
#define XML_CONFIG_FRAME_SIZE	_T("FrameSize")


#define XML_LOG_ROOT _T("LOG_CFG")
#define XML_LOG_TRACE_PATH _T("LogFilePath")

enum ENUM_CAMERA_MSG_FORMAT
{
  CAMERA_MSG_TYPE_ORI = 0,
  CAMERA_MSG_TYPE_GSSM_BARCODE_SUPPORT,
  CAMERA_MSG_TYPE_MAX
};

enum ENUM_CAMERA_TYPES
{
  CAMERA_TYPE_SICK = 0,
  CAMERA_TYPE_VITRONIC,
  CAMERA_TYPE_MAX
};

enum ENUM_GSS_LINK_TYPES
{
  GSS_LINK_PRODUCTION = 0,
  GSS_LINK_TEST,
  GSS_LINK_MAX
};

class CAppSettings : public CObject  
{
DECLARE_SERIAL (CAppSettings)
public:
	//
	CString m_sAppPath;
	CString m_sConfigFilesPath;
	int m_nCommLayerID;

	CString m_sQueuePath;


  // App parameters
	CString m_sConfigFileName;
	CString m_sShutdownFileName;
	CString m_sUpdateTime;

	CString m_sDW_WkPath;
	CString m_sDW_ArchivePath;

	CString m_sAppTracePath;
	//
	CString m_sGSS_IPAddr[2];
	int m_nGSSPort;
	int m_nGSSNumPorts;

	CString m_sCHI_IPAddr;
	int m_nCHIPort;

	CString m_sSorterEquipmentID;

	int m_nFrameSizeLen;
	int m_nFrameSize;

public:
  //
	BOOL LoadXML(CString sFileName);
	BOOL StoreXML(CString sFileName, BOOL bUpdate);
	CAppSettings();
	virtual ~CAppSettings();

private:
  //void Serialize (CArchive& ar);
  void SetDefault();
  CString m_fileName;
	CRITICAL_SECTION m_cs_syncro;
};


extern CAppSettings* G_pAppSettings;
