//////////////////////////////////////////////////////////////////
//
// NAME:					CCoreAppHnd.h		
// LAST MODIFICATION DATE:	27/10/2017
// AUTHOR:					G. Erbetta
// PURPOSE:					Include
//
//////////////////////////////////////////////////////////////////
#pragma once

//////////////////////////
//	APPLICATION THREADS	//
//////////////////////////
enum
{
	THREAD_APPHND,
	//
	NUM_APPLICATION_THREADS
};

class CCoreAppHnd
{
public:
	CCoreAppHnd(void);
	virtual ~CCoreAppHnd(void);

	BOOL	InitApplication(void);
	void	DestroyApplication(void);
	CTime	m_csActualTime;
	CTime	m_csLastitemTime;

	//CQueue	m_FlatQueueAPPHND;
	//
	CWinThread*			m_pArrayTHREADS[NUM_APPLICATION_THREADS];

private:
	HANDLE				m_hMyMutex;

	static DWORD WINAPI _EngineThread(LPVOID pParam);

	void _OpenInternal_Link();
	void _OpenGSS_Link();
	void _CloseInternal_Link();
	void _CloseGSS_Link();

public:
	HANDLE				m_hHandleWaitForSingleObject;
};

