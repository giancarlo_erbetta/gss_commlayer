#pragma once

#include <process.h>

extern CWnd* G_pCommLayerWnd;
#define WM_REFRESH_LINKS     WM_USER+1


#define APP_TRACE_SETTINGS_FILE   L"COMMLAYER_TRACE_SETTINGS"

#define SHUTDOWN_FILE							L"SHUTDOWN.ME"

#define MAX_wMSGID_GSS 20

#pragma pack(1)

// CHI channel
#define NETLINK_CHI_QUEUE_DATA_LEN 512
//
struct STRUCT_NETLINK_CHI_QUEUE_RECORD
{
	WORD wMsgCode;
	WORD wMsgLen;
	BYTE byMsgData[NETLINK_CHI_QUEUE_DATA_LEN + 1];
	//
	STRUCT_NETLINK_CHI_QUEUE_RECORD() { ::ZeroMemory(this, sizeof(STRUCT_NETLINK_CHI_QUEUE_RECORD)); }
};

#define MSGMANAGER_QUEUE_DATA_LEN 512

struct STRUCT_MSGMANAGER_QUEUE_RECORD
{
	BYTE byMsgCode;
	WORD wMsgLen;
	BYTE byMsgData[MSGMANAGER_QUEUE_DATA_LEN + 1];

	STRUCT_MSGMANAGER_QUEUE_RECORD() { ::ZeroMemory(this, sizeof(STRUCT_MSGMANAGER_QUEUE_RECORD)); }
};

#pragma pack()