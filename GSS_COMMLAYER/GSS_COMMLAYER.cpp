
// GSS_COMMLAYER.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GSS_COMMLAYER.h"
#include "GSS_COMMLAYERDlg.h"
#include "Globals.h"
#include "Externals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGSS_COMMLAYERApp

BEGIN_MESSAGE_MAP(CGSS_COMMLAYERApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CGSS_COMMLAYERApp construction

CGSS_COMMLAYERApp::CGSS_COMMLAYERApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CGSS_COMMLAYERApp object

CGSS_COMMLAYERApp theApp;


// CGSS_COMMLAYERApp initialization

BOOL CGSS_COMMLAYERApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();


	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	if (::WSAStartup(MAKEWORD(2, 0), &m_wsaData) != 0)
	{
		AfxMessageBox(_T("WS2_32.DLL INITIALIZATION FAILED !"), MB_ICONSTOP);
		return FALSE;
	}

	// COMM LAYER initialization

	G_pAppSettings = new CAppSettings();

	LPWSTR *szArglist;
	int nArgs;
	szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);

	if (nArgs >= 2 && _wtoi(szArglist[1]) > 0 && _wtoi(szArglist[1]) <= 10)
	{
		G_pAppSettings->m_nCommLayerID = _wtoi(szArglist[1]);
		if (G_pAppSettings->m_nCommLayerID < 1)
		{
			AfxMessageBox(_T("PROGRAM STARTED WITH WRONG FIRST ARG - IT SHALL BE NUMERIC (FROM 1)"), MB_ICONSTOP);
			return FALSE;
		}
	}
	else
		{
			AfxMessageBox(_T("PROGRAM STARTED WITHOUT ARGS - SET LINK ID"), MB_ICONSTOP);
			return FALSE;
		}

	if (nArgs >= 3)
	{
		G_pAppSettings->m_sAppPath.Format(_T("%s"), szArglist[2]);
	}
	else
	{
		G_pAppSettings->m_sAppPath = L"C:\\FIVES_APP\\COMMLAYER\\BIN\\";
	}

	for (int i = G_pAppSettings->m_sAppPath.GetLength(); i>0; i--)
	{
		if (G_pAppSettings->m_sAppPath.Mid(i, 1) == L"\\")
		{
			G_pAppSettings->m_sAppPath = G_pAppSettings->m_sAppPath.Left(i + 1);
#ifdef _DEBUG
			//G_pAppSettings->m_sAppPath = L"C:\\FIVES_APP\\COMMLAYER\\BIN\\";
#endif
			G_pAppSettings->m_sConfigFilesPath = G_pAppSettings->m_sAppPath;
			for (int j = G_pAppSettings->m_sConfigFilesPath.GetLength() - 2; j > 0; j--)
			{
				if (G_pAppSettings->m_sConfigFilesPath.Mid(j, 1) == L"\\")
				{
					G_pAppSettings->m_sConfigFilesPath = G_pAppSettings->m_sAppPath.Left(j + 1);
					break;
				}
			}
			G_pAppSettings->m_sQueuePath = G_pAppSettings->m_sConfigFilesPath;
			G_pAppSettings->m_sQueuePath.Append(L"QUEUEFILES\\");
			G_pAppSettings->m_sConfigFilesPath.Append(L"CONFIG\\");
			break;
		}
	}

	G_pAppSettings->m_sShutdownFileName = G_pAppSettings->m_sAppPath;
	G_pAppSettings->m_sShutdownFileName.AppendFormat(_T("SHUTDOWN%d.ME"), G_pAppSettings->m_nCommLayerID);
	DeleteFile(G_pAppSettings->m_sShutdownFileName);

	G_pAppSettings->m_sConfigFileName.Format(_T("COMMLAYER_SETTINGS%d.XML"), G_pAppSettings->m_nCommLayerID);

	G_pAppSettings->LoadXML(G_pAppSettings->m_sConfigFileName);


	wchar_t wcMutex[20];

	swprintf(wcMutex, L"COMMLAYER_%d", G_pAppSettings->m_nCommLayerID);

	// allow one instance only
	m_hApplicationMutex = ::CreateMutex(NULL, TRUE, wcMutex);
	if (::GetLastError() == ERROR_ALREADY_EXISTS)
	{
		AfxMessageBox(_T("PROGRAM ALREADY RUNNING"), MB_ICONSTOP);
		return FALSE;
	}

	Sleep((G_pAppSettings->m_nCommLayerID - 1) * 500);

	CString sLogSettings;

	sLogSettings.Format(_T("%s%d"), APP_TRACE_SETTINGS_FILE, G_pAppSettings->m_nCommLayerID);

	G_pTrace = new CAppTrace(G_pAppSettings->m_sConfigFilesPath.GetBuffer(), sLogSettings.GetBuffer(), (wchar_t*)(const wchar_t*)G_pAppSettings->m_sAppTracePath, 2000, APP_TRACE_SETTINGS_FILE, 2000);
	G_pProtocolConverter_FX = new CProtocolConverter_FX();

	if (G_pProtocolConverter_FX->InitProtocolConverter(G_pAppSettings->m_sConfigFilesPath.GetBuffer()) == FALSE)
	{
		AfxMessageBox(_T("PROTOCOL INITIALIZATION FAILED"), MB_ICONSTOP);
		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_1_CHI, __FILE__, __LINE__, 0, "CGSS_COMMLAYERApp::InitInstance PROTOCOL INITIALIZATION FAILED !!!!!");
		return FALSE;
	}
	G_pLinksManager = new CLinksManager(L"LINKS_MGM", 1000);
	

	// end init

	CGSS_COMMLAYERDlg dlg;
	m_pMainWnd = &dlg;
	G_pCommLayerWnd = m_pMainWnd;
	INT_PTR nResponse = dlg.DoModal();

	if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	}


	if (G_pLinksManager != NULL)
	{
		G_pLinksManager->DestroyApplication();
		delete G_pLinksManager;
		G_pLinksManager = NULL;
	}

	if (m_hApplicationMutex != NULL)
	{
		if (::ReleaseMutex(m_hApplicationMutex) == FALSE)
			::CloseHandle(m_hApplicationMutex);
	}


	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

