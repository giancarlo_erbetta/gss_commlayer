
// GSS_COMMLAYERDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GSS_COMMLAYER.h"
#include "GSS_COMMLAYERDlg.h"
#include "afxdialogex.h"
#include "PasswordDlg.h"
#include "DlgTraceLevel.h"
#include "Linksmanager.h"
#include "ChIntNetLink.h"
#include "AppSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const UINT ID_TIMER_SECONDS = 0x1000;

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// CGSS_COMMLAYERDlg dialog



CGSS_COMMLAYERDlg::CGSS_COMMLAYERDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGSS_COMMLAYERDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
}

void CGSS_COMMLAYERDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CGSS_COMMLAYERDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CGSS_COMMLAYERDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CGSS_COMMLAYERDlg::OnBnClickedButton1)
	ON_MESSAGE(WM_REFRESH_LINKS, &CGSS_COMMLAYERDlg::OnRefreshLinks)
	//ON_WM_TIMER()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CGSS_COMMLAYERDlg message handlers

BOOL CGSS_COMMLAYERDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	_ctlStaticGSS1.SubclassDlgItem(IDC_STATIC_GSS1, this);
	_ctlStaticGSS2.SubclassDlgItem(IDC_STATIC_GSS2, this);
	_ctlStaticCHI.SubclassDlgItem(IDC_STATIC_CHI, this);
	_ctlStaticApp.SubclassDlgItem(IDC_STATIC_APP, this);
	
	CString sApp;
	
	sApp.Format(_T("COMM_LAYER_%d"), G_pAppSettings->m_nCommLayerID);
	_ctlStaticApp.SetWindowTextW(sApp);

	G_pLinksManager->InitApplication();

	SetTimer(ID_TIMER_SECONDS, 1000, 0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CGSS_COMMLAYERDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CGSS_COMMLAYERDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CGSS_COMMLAYERDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CGSS_COMMLAYERDlg::OnBnClickedCancel()
{
	CPasswordDlg	PasswordDlg;
	PasswordDlg.SetTitle(_T("Are You Sure To Exit From Application?"));
	PasswordDlg.SetPassword(_T("FXAdmin"));
	if (PasswordDlg.DoModal() != IDOK)
		return;

	KillTimer(ID_TIMER_SECONDS);
	CDialogEx::OnCancel();
}


void CGSS_COMMLAYERDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	CDlgTraceLevel dlg;
	dlg.DoModal();
}

afx_msg LRESULT CGSS_COMMLAYERDlg::OnRefreshLinks(WPARAM wParam, LPARAM lParam)
{
	//CColorStatic _ctlStatic;

	//_ctlStatic = (CColorStatic*)GetDlgItem(IDC_STATIC_GSS1);
	
	if (G_pLinksManager->m_csSTRUCT_COMMLAYER_DIAGNO.bGSS1_connected)
		_ctlStaticGSS1.SetBkColor(RGB(0, 255, 0));
	else
		_ctlStaticGSS1.SetBkColor(RGB(255, 0, 0));

	if (G_pLinksManager->m_csSTRUCT_COMMLAYER_DIAGNO.bGSS2_connected)
		_ctlStaticGSS2.SetBkColor(RGB(0, 255, 0));
	else
		_ctlStaticGSS2.SetBkColor(RGB(255, 0, 0));

	if (G_pChIntNetLink->GetLinkStatus() == LINK_RUNNING)
		_ctlStaticCHI.SetBkColor(RGB(0, 255, 0));
	else
		_ctlStaticCHI.SetBkColor(RGB(255, 0, 0));
	
	/*
	_ctlStatic = (CColorStatic*)GetDlgItem(IDC_STATIC_GSS2);
	_ctlStatic = (CColorStatic*)GetDlgItem(IDC_STATIC_CHI);
	*/
	return 1;
}




void CGSS_COMMLAYERDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnTimer(nIDEvent);

	if (nIDEvent == ID_TIMER_SECONDS)
	{
		// Do your seconds based tasks here.
		HANDLE hFind;
		WIN32_FIND_DATA FindFileData;

		hFind = FindFirstFile(G_pAppSettings->m_sShutdownFileName, &FindFileData);

		if (hFind != INVALID_HANDLE_VALUE)
		{
			DeleteFile(G_pAppSettings->m_sShutdownFileName);
			KillTimer(ID_TIMER_SECONDS);
			CDialogEx::OnCancel();
			return;
		}
	}
}
