//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GSS_COMMLAYER.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GSS_COMMLAYER_DIALOG        102
#define IDD_PSWDLG                      104
#define IDR_MAINFRAME                   128
#define IDD_TRACE_LEVEL                 130
#define IDC_BUTTON1                     1000
#define IDC_STATIC_GSS1                 1001
#define IDC_STATIC_GSS2                 1002
#define IDC_EDIT1                       1002
#define IDC_CHECK1                      1003
#define IDC_STATIC_CHI                  1003
#define IDC_CHECK2                      1004
#define IDC_STATIC_GSS3                 1004
#define IDC_STATIC_APP                  1004
#define IDC_CHECK3                      1005
#define IDC_CHECK4                      1006
#define IDC_CHECK5                      1011
#define IDC_CHECK6                      1012
#define IDC_CHECK7                      1013
#define IDC_CHECK8                      1014
#define IDC_CHECK9                      1023
#define IDC_CHECK10                     1024
#define IDC_CHECK11                     1025
#define IDC_CHECK12                     1026
#define IDC_CHECK13                     1027
#define IDC_CHECK14                     1028
#define IDC_CHECK15                     1029
#define IDC_CHECK16                     1030
#define IDC_EDIT_PASSWORD               1062

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
