// ChIntNetLink.h: interface for the CChIntNetLink class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "NetLink.h"
#include "AppTrace.h"

class CChIntNetLink : public CNetLink  
{
public:
	CChIntNetLink(STRUCT_NETLINK_INIT& rcsInit);
	virtual ~CChIntNetLink();

protected:
	virtual BOOL OnSendBeginKA();
	virtual BOOL OnSendBegin();
	virtual BOOL OnSendEnd();
	virtual BOOL OnReceiveBegin();
	virtual BOOL OnReceiveEnd();
	virtual BOOL OnConnected();
	virtual BOOL OnConnectionBroken();
	virtual BOOL OnTelegramReceived();
	// Optional
	virtual BOOL OnCreate()           {/*VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_7_EVENTS,__FILE__,__LINE__,0, "%s Create soket on port %d", m_szObjectName, m_nCurrentPortID);*/ return TRUE;}
	virtual BOOL OnCreateError()			{/*VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "%s Create failed", m_szObjectName); */ return TRUE; }
	virtual BOOL OnMemoryOverflow()		{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s Memory overflow", m_szObjectName); return TRUE;}
	virtual BOOL OnOpenFailed()				{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s OpenLink failed", m_szObjectName); return TRUE;}
	virtual BOOL OnShutdown()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s Shut Down", m_szObjectName); return TRUE;}
	virtual BOOL OnRxTimeout()				{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s RX Timeout", m_szObjectName); m_LDI.nNumTimeout++; return TRUE;}
	virtual BOOL OnRxError()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s RX Error", m_szObjectName); m_LDI.nNumRXerror++; return TRUE;}
	virtual BOOL OnRxEmpty()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s RX Empty", m_szObjectName); return TRUE;}
	virtual BOOL OnTxTimeout()				{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s TX Timeout", m_szObjectName); m_LDI.nNumTimeout++; return TRUE;}
	virtual BOOL OnTxError()					{VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|CHI||||%s TX Error", m_szObjectName); m_LDI.nNumTXerror++; return TRUE;}

protected:
	STRUCT_ENVELOPECML  m_csSTRUCT_ENVELOPECML;
	STRUCT_TAILCML      m_csSTRUCT_TAILCML;

private:
	BOOL m_LinkON;
};
