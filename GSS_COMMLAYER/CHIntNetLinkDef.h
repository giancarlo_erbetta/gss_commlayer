
#pragma once

#pragma pack()
//
struct STRUCT_CHI_NETLINK_INIT
{
	int      nThreadPriority;                   // thread priority
	wchar_t  wszObjectName[32 + 1];         // object name
	int      nInstanceID;                       // instance ID
	int      nClosingtimeout;                   // closing thread timeout (ms)
	wchar_t  wszIPAddr[32 + 1];             // IP address
	int      nPortID;                           // channel port ID
	long     lLinkLossTimeout;                  // RX timeout
	int      nMaxRetries;                       // transmission retries  
	int      nQueueSendRecordSize;              // queue send record size
	int      nQueueSendRecords;                 // queue send records
	bool     bSocketServer;
	wchar_t  wszQueueSendName[128 + 1];      // queue send name
	//
	STRUCT_CHI_NETLINK_INIT() { ::ZeroMemory(this, sizeof(STRUCT_CHI_NETLINK_INIT)); }
};

struct STRUCT_CHI_NETLINK_DEBUG_INFO
{
	wchar_t wszObjectName[32 + 1];
	int    nPortID;
	long   lLinkLossTimeout;
	int    nMaxRetries;
	int    nNumTXRetries;
	int    nNumTx;
	int    nNumRx;
	int    nNumTXTelegrams;
	int    nNumRXTelegrams;
	int    nNumTXerror;
	int    nNumRXerror;
	int    nNumRXTimeout;
	int    nQueueSize;
	int    nQueueLen;
	int    nQueuePeak;

	STRUCT_CHI_NETLINK_DEBUG_INFO() { ::ZeroMemory(this, sizeof(STRUCT_CHI_NETLINK_DEBUG_INFO)); }
};

#define INT_MSG_CFG 101
#define INT_MSG_DIAG 102
#define INT_MSG_RX_RAWDATA 103
#define INT_MSG_TX_RAWDATA 104

#pragma pack(1)
#define CHI_NETLINK_QUEUE_DATA_LEN 512
struct STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER
{
	BYTE byMsgID;
	WORD wMsgLen;
	//
	STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER() { ::ZeroMemory(this, sizeof(STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER)); }
};

struct STRUCT_CHI_NETLINK_QUEUE_RECORD
{
	STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER;
	char szMsgData[CHI_NETLINK_QUEUE_DATA_LEN + 1];
	//
	STRUCT_CHI_NETLINK_QUEUE_RECORD() { ::ZeroMemory(this, sizeof(STRUCT_CHI_NETLINK_QUEUE_RECORD)); }
};

typedef struct _STRUCT_COMMLAYER_CFG
{
	// all the strings are with the null terminator
	CHAR	szGSS1_IP_add[16];
	CHAR	szGSS2_IP_add[16];
	WORD	wGSS_port;
} STRUCT_COMMLAYER_CFG;
#define SIZE_STRUCT_COMMLAYER_CFG	sizeof(_STRUCT_COMMLAYER_CFG)
#pragma pack()


#pragma pack(1)
typedef struct _STRUCT_COMMLAYER_DIAGNO
{
	BYTE	bGSS1_connected;  // TRUE - FALSE
	BYTE	bGSS2_connected;  // TRUE - FALSE
} STRUCT_COMMLAYER_DIAGNO;



typedef struct _STRUCT_RAWDATA_MSG
{
	// all the strings are with the null terminator
	BYTE	nGSS_ID;
	CHAR	szMSG[200];
} STRUCT_RAWDATA_MSG;

#pragma pack()
