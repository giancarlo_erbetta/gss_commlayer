#pragma once

#include "ThreadLib.h"
#include "QueueLib.h"
#include "GSS_ProtocolDef.h"
#include "ChIntNetLink.h"

class CLinksManager :
	public CThreadLib
{
public:
	CLinksManager(wchar_t* _pwszObjectName, int _nClosingtimeout);
	virtual ~CLinksManager();
	BOOL	InitApplication(void);
	void	DestroyApplication(void);
	BOOL	IS_GSSLinksStatus_Changed(void);
	void	SendGSSLinksDiagno(void);

private:
	static DWORD WINAPI _EngineThread(LPVOID pParam);
	void _MsgFromQueueI(STRUCT_MSGMANAGER_QUEUE_RECORD* pcsSTRUCT_MSGMANAGER_QUEUE_RECORD);
	void _MsgFromQueueE(STRUCT_MSGMANAGER_QUEUE_RECORD* pcsSTRUCT_MSGMANAGER_QUEUE_RECORD);
	void _OpenInternal_Link();
	void _OpenGSS_Link();
	void SendToGSS(STRUCT_GSS_LINK_QUEUE_RECORD* pcsSTRUCT_GSS_LINK_QUEUE_RECORD);
	void _CloseInternal_Link();
	void _CloseGSS_Link();

public:
	CQueue m_QueueInternal;
	CQueue m_QueueExternal;
	CTime	m_csActualTime;
	CTime	m_csLastitemTime;
	STRUCT_COMMLAYER_DIAGNO m_csSTRUCT_COMMLAYER_DIAGNO;

private:
	int m_nLastGSS1Port;
	int m_nLastGSS2Port;

};

extern CLinksManager* G_pLinksManager;