#include "StdAfx.h"
#include "ProtocolConverter.h"
#include "AppTrace.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CProtocolConverter::CProtocolConverter(void)
{

//	InitProtocolConverter();

}


CProtocolConverter::~CProtocolConverter(void)
{
}

BOOL CProtocolConverter::InitProtocolConverter(LPTSTR p_sAPPL_FILES_PATH)
{
	int i,j,k,z;
	wchar_t       wszMsgFileName[MAX_PATH+1];
	
	CStdioFile myFile;
	CString sNewLine;
	CString sData[5];


	CString sFile;
	WIN32_FIND_DATA		csFindFileData;

	memset(strMsgFormat,0,sizeof(strMsgFormat));

	for (i=0;i<MAX_MESSAGES;i++)
	{
		
		sFile.Empty();

		wsprintf(wszMsgFileName, L"%s%s%d_*%s", p_sAPPL_FILES_PATH, FORMAT_FILENAME_ROOT, i, FORMAT_FILENAME_EXT);
		HANDLE hFind = ::FindFirstFile(wszMsgFileName, &csFindFileData);
		BOOL bFound = (hFind == INVALID_HANDLE_VALUE ? FALSE : TRUE);
		::FindClose(hFind);
		if (bFound == TRUE)
		{
			sFile = csFindFileData.cFileName;
			wsprintf(wszMsgFileName, L"%s%s", p_sAPPL_FILES_PATH, sFile);

			if (myFile.Open(wszMsgFileName, CFile::shareDenyNone | CFile::modeRead | CFile::typeText))
			{
				j = 0;
				while (myFile.ReadString(sNewLine))
				{
					sNewLine = sNewLine.Trim();
					sNewLine.Append(_T(",,,,"));
					for (z = 0; z<5; z++)
					{
						k = sNewLine.Find(_T(","), 0);
						sData[z] = sNewLine.Left(k);
						sData[z] = sData[z].Trim();
						sNewLine = sNewLine.Mid(k + 1);
					}

					if (sData[0] == _T("NAME"))
					{
						char szBuffer[32];
						//wcstombs_s(&nRet, szBuffer, sData[1].GetLength(), sData[1].GetBuffer(),sData[1].GetLength());
						wcstombs(szBuffer, sData[1].GetBuffer(), sData[1].GetLength());
						szBuffer[sData[1].GetLength()] = 0;
						memcpy(strMsgFormat[i].szName, szBuffer, sData[1].GetLength());
					}
					else if (sData[0].Trim() == _T("// LOG") || sData[0] == _T("//LOG"))
					{
						// memcpy(strMsgFormat[i].szName, szBuffer, sData[1].GetLength());
						strMsgFormat[i].bLOG = true;
					}
					else if (sData[0].Left(2) != _T("//") && !sNewLine.IsEmpty())
					{
						strMsgFormat[i].strFielFormat[j].nTypeI = GetTypeId(sData[1], wszMsgFileName);
						strMsgFormat[i].strFielFormat[j].nLenI = GetTypeLen(sData[1]);
						strMsgFormat[i].strFielFormat[j].nOffsetI = strMsgFormat[i].nTotalLenI;
						SetValue(strMsgFormat[i].byDefaultMsgI, strMsgFormat[i].strFielFormat[j].nTypeI, strMsgFormat[i].strFielFormat[j].nOffsetI, strMsgFormat[i].strFielFormat[j].nLenI, sData[2]);
						strMsgFormat[i].nTotalLenI += strMsgFormat[i].strFielFormat[j].nLenI;

						strMsgFormat[i].strFielFormat[j].nTypeE = GetTypeId(sData[3], wszMsgFileName);
						strMsgFormat[i].strFielFormat[j].nLenE = GetTypeLen(sData[3]);
						strMsgFormat[i].strFielFormat[j].nOffsetE = strMsgFormat[i].nTotalLenE;
						SetValue(strMsgFormat[i].byDefaultMsgE, strMsgFormat[i].strFielFormat[j].nTypeE, strMsgFormat[i].strFielFormat[j].nOffsetE, strMsgFormat[i].strFielFormat[j].nLenE, sData[4]);
						strMsgFormat[i].nTotalLenE += strMsgFormat[i].strFielFormat[j].nLenE;
						strMsgFormat[i].nTotalFields++;
						//SetValue(strMsgFormat[i].byDefaultMsgI, strMsgFormat[i].strFielFormat[j].nTypeI, 0, 10, 9);
						//SetValue(strMsgFormat[i].byDefaultMsgI, strMsgFormat[i].strFielFormat[j].nTypeI, 0, 10, 99999999);
						j++;
					}
				}

				VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|||||<CProtocolConverter::InitProtocolConverter> INFO: Processed File Format N.%d (%s) with %d fields LenI=%d LenE=%d", i, strMsgFormat[i].szName, strMsgFormat[i].nTotalFields, strMsgFormat[i].nTotalLenI, strMsgFormat[i].nTotalLenE);

				myFile.Close();
			}
			else
				return FALSE;
		}
	}

	return TRUE;

}

// Return the External message length
int CProtocolConverter::ConvertToMsgE(int nMsgId, BYTE *byBufferSrc, BYTE *byBufferDst)
{
	int i;
	int len;

	if (nMsgId!=0 && nMsgId!=11)
	{
		nMsgId=nMsgId;
	}
	memcpy(byBufferDst, strMsgFormat[nMsgId].byDefaultMsgE, strMsgFormat[nMsgId].nTotalLenE);

	len = 0;
	for (i=0;i<strMsgFormat[nMsgId].nTotalFields;i++)
	{
		ConvertField(byBufferSrc, strMsgFormat[nMsgId].strFielFormat[i].nTypeI, strMsgFormat[nMsgId].strFielFormat[i].nOffsetI, strMsgFormat[nMsgId].strFielFormat[i].nLenI, 
									byBufferDst, strMsgFormat[nMsgId].strFielFormat[i].nTypeE, strMsgFormat[nMsgId].strFielFormat[i].nOffsetE, strMsgFormat[nMsgId].strFielFormat[i].nLenE);
		len += strMsgFormat[nMsgId].strFielFormat[i].nLenE;
	}

	return len;

}

// Return the External message length
int CProtocolConverter::ConvertToMsgI(int nMsgId, BYTE *byBufferSrc, BYTE *byBufferDst, int nSrcLen)
{
	int i;
	int len;

	memcpy(byBufferDst, strMsgFormat[nMsgId].byDefaultMsgI, strMsgFormat[nMsgId].nTotalLenI);

	if (nSrcLen != strMsgFormat[nMsgId].nTotalLenI)
	{
		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|||||*** ERROR *** : ConvertToMsgI MSG%d=%s Expected internal msg length %d while in MSG file is %d", nMsgId, strMsgFormat[nMsgId].szName, nSrcLen, strMsgFormat[nMsgId].nTotalLenI);
	}

	len = 0;
	for (i = 0; i<strMsgFormat[nMsgId].nTotalFields; i++)
	{
		ConvertField(byBufferSrc, strMsgFormat[nMsgId].strFielFormat[i].nTypeE, strMsgFormat[nMsgId].strFielFormat[i].nOffsetE, strMsgFormat[nMsgId].strFielFormat[i].nLenE, 
								 byBufferDst, strMsgFormat[nMsgId].strFielFormat[i].nTypeI, strMsgFormat[nMsgId].strFielFormat[i].nOffsetI, strMsgFormat[nMsgId].strFielFormat[i].nLenI);
		len += strMsgFormat[nMsgId].strFielFormat[i].nLenE;
	}

	return len;

}

void CProtocolConverter::ConvertField(BYTE *byBufferSrc, int nTypeSrc, int nOffsetSrc, int nLenSrc, BYTE *byBufferDst, int nTypeDst, int nOffsetDst, int nLenDst)
{
DWORD dw=0;

	switch (nTypeSrc)
	{
	case ID_TYPE_I8:
		memcpy(&dw, &byBufferSrc[nOffsetSrc],1);
		SetValue(byBufferDst, nTypeDst, nOffsetDst, nLenDst, dw);
		break;

	case ID_TYPE_I16:
		memcpy(&dw, &byBufferSrc[nOffsetSrc],2);
		SetValue(byBufferDst, nTypeDst, nOffsetDst, nLenDst, dw);
		break;

	case ID_TYPE_I32:
		memcpy(&dw, &byBufferSrc[nOffsetSrc],4);
		SetValue(byBufferDst, nTypeDst, nOffsetDst, nLenDst, dw);
		break;

	case ID_TYPE_HF:
		memcpy(&dw, &byBufferSrc[nOffsetSrc],4);
		SetValue(byBufferDst, nTypeDst, nOffsetDst, nLenDst, dw);
		break;

	case ID_TYPE_ASCII:
		SetValue(byBufferDst, nTypeDst, nOffsetDst, nLenDst, &byBufferSrc[nOffsetSrc], min(nLenSrc, strlen((char*)&byBufferSrc[nOffsetSrc])));
		break;

	}
}

void CProtocolConverter::SetValue(BYTE *byBuffer, int nType, int nOffset, int nLen, CString sValue)
{	
	char szDefaultBuffer[128];
	BYTE by;
	WORD w;
	DWORD dw;

	wcstombs(szDefaultBuffer, sValue.GetBuffer(), sValue.GetLength());

	szDefaultBuffer[sValue.GetLength()]=0;

	switch (nType)
	{
	case ID_TYPE_I8:
		by=atoi(szDefaultBuffer);
		memcpy(&byBuffer[nOffset], &by,1);
		break;

	case ID_TYPE_I16:
		w=atoi(szDefaultBuffer);
		memcpy(&byBuffer[nOffset], &w,2);
		break;

	case ID_TYPE_I32:
		dw=atol(szDefaultBuffer);
		memcpy(&byBuffer[nOffset], &dw,4);

		break;

	case ID_TYPE_ASCII:
		for (int i=0;i<nLen;i++)
			sValue.Append(_T(" "));
		sValue.Right(nLen);
		wcstombs(szDefaultBuffer,sValue.GetBuffer(),nLen);
		
		szDefaultBuffer[nLen]=0;
		memcpy(&byBuffer[nOffset],szDefaultBuffer,nLen);
		break;

	}
}

void CProtocolConverter::SetValue(BYTE *byBuffer, int nType, int nOffset, int nLen, BYTE *byValue, int nValueLen)
{	
	char szDefaultBuffer[256];
	BYTE by;
	WORD w;
	DWORD dw;

	if (nType == ID_TYPE_VOID)
		return;

	memcpy(szDefaultBuffer, byValue, nValueLen);
	szDefaultBuffer[nValueLen]=0;

	switch (nType)
	{
	case ID_TYPE_I8:
		by=atoi(szDefaultBuffer);
		memcpy(&byBuffer[nOffset], &by,1);
		break;

	case ID_TYPE_I16:
		w=atoi(szDefaultBuffer);
		memcpy(&byBuffer[nOffset], &w,2);
		break;

	case ID_TYPE_I32:
		dw=atol(szDefaultBuffer);
		memcpy(&byBuffer[nOffset], &dw,4);

		break;

	case ID_TYPE_ASCII:
		memcpy(&byBuffer[nOffset], szDefaultBuffer, min(nLen, nValueLen));
		break;

	}
}

// to set a numeric value
void CProtocolConverter::SetValue(BYTE *byBuffer, int nType, int nOffset, int nLen, DWORD dwValue)
{	
	char szTempBuffer[64];
	char szTempBuffer2[64];
	BYTE by;
	WORD w;

	switch (nType)
	{
	case ID_TYPE_I8:
		by = (BYTE) dwValue & 0xFF;
		memcpy(&byBuffer[nOffset], &by, 1);
		break;

	case ID_TYPE_I16:
		w = (WORD)dwValue & 0xFFFF;
		memcpy(&byBuffer[nOffset], &w, 2);
		break;

	case ID_TYPE_I32:
		memcpy(&byBuffer[nOffset], &dwValue, 4);
		break;

	case ID_TYPE_ASCII:
		by = 0;
		// set memory with 0
		memset(&byBuffer[nOffset], '0', nLen);
		sprintf(szTempBuffer, "%ld", dwValue);
		if (strlen(szTempBuffer) > nLen)
		{
			by = strlen(szTempBuffer) - nLen;
			nLen = strlen(szTempBuffer);
		}
		memcpy(&byBuffer[nOffset + nLen - strlen(szTempBuffer)], &szTempBuffer[by], strlen(szTempBuffer) - by);
		break;

	case ID_TYPE_HF:
		memset(szTempBuffer,'0',sizeof(szTempBuffer2));
		sprintf(szTempBuffer2,"%0X",dwValue);
		for (int i=0; (i < nLen) && ((strlen(szTempBuffer2) - i) > 0); i++)
		{
			if (szTempBuffer2[strlen(szTempBuffer2) - 1 - i] < 'A')
			{
				szTempBuffer[i] = szTempBuffer2[strlen(szTempBuffer2) - 1 - i];
			}
			else
			{
				szTempBuffer[i] = szTempBuffer2[strlen(szTempBuffer2) - 1 - i] - 'A' + ':';
			}
		}
		memcpy(&byBuffer[nOffset], szTempBuffer, nLen);
		break;
	}
}


int CProtocolConverter::GetTypeId(CString sType, wchar_t *wszMsgFileName)
{

	if (sType == STR_TYPE_BYTE || sType == STR_TYPE_I8)
	{
		return ID_TYPE_I8;
	}
	else if (sType == STR_TYPE_WORD || sType == STR_TYPE_I16)
	{
		return ID_TYPE_I16;
	}
	else if (sType == STR_TYPE_DWORD || sType == STR_TYPE_I32)
	{
		return ID_TYPE_I32;
	}
	else if (sType.Left(2) == STR_TYPE_HEXSTR_FX)
	{
		return ID_TYPE_HF;
	}
	else if (sType.Left(1) == STR_TYPE_ASCII)
	{
		return ID_TYPE_ASCII;
	}
	else if (sType == STR_TYPE_VOID)
	{
		return ID_TYPE_VOID;
	}
	else
	{
		char szType[32];
		char szFile[128];

		wcstombs(szType,sType.GetBuffer(),sType.GetLength());
		szType[sType.GetLength()]=0;
		wcstombs(szFile,wszMsgFileName,wcslen(wszMsgFileName));
		szFile[wcslen(wszMsgFileName)]=0;

		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "|||||ERROR in FILE conversion format %s, Unkonown format %s", szFile, szType);
	}

	return 0;
}

int CProtocolConverter::GetTypeLen(CString sType)
{

	if (sType == STR_TYPE_BYTE || sType == STR_TYPE_I8)
	{
		return 1;
	}
	else if (sType == STR_TYPE_WORD || sType == STR_TYPE_I16)
	{
		return 2;
	}
	else if (sType == STR_TYPE_DWORD || sType == STR_TYPE_I32)
	{
		return 4;
	}
	else if (sType.Left(2) == STR_TYPE_HEXSTR_FX)
	{
		if (_ttoi(sType.Mid(2)) == 0)
			return 8;
		else
			return _ttoi(sType.Mid(2));
	}
	else if (sType.Left(1) == STR_TYPE_ASCII)
	{
		return _ttoi(sType.Mid(1));
	}

	return 0;
}
