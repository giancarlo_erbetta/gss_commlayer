// ChIntNetLink.cpp: implementation of the CChIntNetLink class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "BRIDGE_CSM.h"
#include "ChIntNetLink.h"
#include "ChIntNetLinkDef.h"
#include "OverlappedSocket.h"
#include "AppSettings.h"
#include "LinksManager.h"
#include "ProtocolConverter_FX.h"

#include "ConversionsLib.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CChIntNetLink::CChIntNetLink
//@
//@ Parameters:
//@		 [IN]       STRUCT_CHI_NETLINK_INIT& rcsInit
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CChIntNetLink::CChIntNetLink(STRUCT_CHI_NETLINK_INIT& _csSTRUCT_CHI_NETLINK_INIT) :
  CThreadLib(_csSTRUCT_CHI_NETLINK_INIT.wszObjectName, _csSTRUCT_CHI_NETLINK_INIT.nClosingtimeout),
	m_queueSend(_csSTRUCT_CHI_NETLINK_INIT.nQueueSendRecordSize, _csSTRUCT_CHI_NETLINK_INIT.nQueueSendRecords, 
	            _csSTRUCT_CHI_NETLINK_INIT.wszQueueSendName),
  SIZE_BUF_TX(4096),
	SIZE_BUF_RX(4096),
	SOM_CH(0x02),
	EOM_CH(0x03),
	m_csSTRUCT_CHI_NETLINK_INIT(_csSTRUCT_CHI_NETLINK_INIT)
{
	//
	::WideCharToMultiByte(CP_ACP, NULL, _csSTRUCT_CHI_NETLINK_INIT.wszObjectName, -1, 
												m_szObjectName, MAX_PATH, NULL, NULL);
	m_szObjectName[MAX_PATH] = 0;

	m_nInstanceID = _csSTRUCT_CHI_NETLINK_INIT.nInstanceID;

  // initialization
	m_wsaTxData.len = SIZE_BUF_TX;
  m_wsaTxData.buf = new char[m_wsaTxData.len+1];	// +1 for cap char
  if(m_wsaTxData.buf == NULL)
		throw CFOLibException(0, L"CChIntNetLink: m_wsaTxData.buf == NULL");

	m_wsaRxData.len = SIZE_BUF_RX;
  m_wsaRxData.buf = new char[m_wsaRxData.len+1];	// +1 for cap char
  if(m_wsaRxData.buf == NULL)
		throw CFOLibException(0, L"CChIntNetLink: m_wsaRxData.buf == NULL");

  m_byDatagramBuffer = new BYTE[SIZE_BUF_RX+1];	// +1 for cap char
  if(m_byDatagramBuffer == NULL)
		throw CFOLibException(0, L"CChIntNetLink: m_byDatagramBuffer == NULL");

	m_dwWriteBytes=0; 
	m_dwReadBytes=0; 
	m_nTxTimeout = 0;
	m_bLinkON=FALSE;
  m_nTxRetryCounter=0;
  m_nBuildDatagramStep=0;
  m_nDatagramWriterIndex=0;
  m_dwRXBufIndex=0;
  
	// run the worker thread
  CThreadLib::m_bObjectOk = CThreadLib::RunThread( CChIntNetLink::EngineThread, m_csSTRUCT_CHI_NETLINK_INIT.nThreadPriority);
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Distructor CChIntNetLink::~CChIntNetLink
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CChIntNetLink::~CChIntNetLink()
{
  CThreadLib::StopThread();
	// make some Sleeps to allow thread stop before releasing buffers
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);
	Sleep(10);

	if(m_wsaTxData.buf != NULL)
	{
		delete[] m_wsaTxData.buf;
		m_wsaTxData.buf = NULL;
	}

	if(m_wsaRxData.buf != NULL)
	{
		delete[] m_wsaRxData.buf;
		m_wsaRxData.buf = NULL;
	}
	
	if(m_byDatagramBuffer != NULL)
	{
	  delete m_byDatagramBuffer;
	  m_byDatagramBuffer = NULL;
	}

}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Prepare the message to be sent
//@ 
//@
BOOL CChIntNetLink::OnSendBegin(STRUCT_CHI_NETLINK_QUEUE_RECORD& csSTRUCT_CHI_NETLINK_QUEUE_RECORD)
{
	if(m_wsaTxData.buf == NULL)
	  return FALSE;

	::ZeroMemory(m_wsaTxData.buf, SIZE_BUF_TX);
  //
  BYTE* pbyData= (BYTE*)m_wsaTxData.buf;

  // ...
	pbyData[0] = SOM_CH;
	pbyData++;
  //VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS,__FILE__,__LINE__,0, "%s SEND CALLED !!!");
	memcpy(pbyData, (BYTE*)&csSTRUCT_CHI_NETLINK_QUEUE_RECORD, sizeof(STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER) + csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen);
	pbyData += sizeof(STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER) + csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen;
	pbyData[0] = EOM_CH;
	pbyData++;

  // set total number of bytes to be sent
	m_wsaTxData.len = (ULONG)(pbyData - (BYTE*)m_wsaTxData.buf);
	
	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Message has been sent properly
//@
BOOL CChIntNetLink::OnSendEnd()
{
	m_LDI.nNumTx++;

	return TRUE;
}


//@//////////////////////////////////////////////////////////////////////
//@
//@ 
//@
BOOL CChIntNetLink::OnReceiveBegin()
{
	if(m_wsaRxData.buf == NULL)
	  return FALSE;

  if(m_byDatagramBuffer == NULL)
    return FALSE;

  m_wsaRxData.len=SIZE_BUF_RX;
  ::ZeroMemory(m_wsaRxData.buf, SIZE_BUF_RX);

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ 
//@
BOOL CChIntNetLink::OnReceiveEnd()
{
  char cValue;
	STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER  *pcsSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER = (STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER *)m_byDatagramBuffer;

  // restart TX retry counter on RX timeout
  m_nTxRetryCounter=0;
	
	// parse all RX buffer
	while(m_dwRXBufIndex < m_dwReadBytes)
	{
		// force restart if destination buffer overflow
		if(m_nDatagramWriterIndex >= SIZE_BUF_RX)
			m_nDatagramWriterIndex=0;
		
		cValue = m_wsaRxData.buf[m_dwRXBufIndex++];
		

		switch(m_nBuildDatagramStep)
		{
			// WAIT FOR STX
			case 0:
				
				if(cValue != SOM_CH)
				  break;
				//
				::ZeroMemory(m_byDatagramBuffer, SIZE_BUF_RX);
				m_nDatagramWriterIndex=0;
        
        // next step: receive message body
				m_nBuildDatagramStep = 1;
			break;
			
			// WAIT FOR ETX, RECEIVE MESSAGE BODY
			case 1:
			  
				// ETX ?
				if (m_nDatagramWriterIndex >= sizeof(STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER) && cValue == EOM_CH && m_nDatagramWriterIndex >= pcsSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER->wMsgLen + sizeof(STRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER) )
				{				  
          m_byDatagramBuffer[m_nDatagramWriterIndex] = 0;    
          
          // PROCESS TELEGRAM
          OnTelegramReceived(m_nDatagramWriterIndex);
					//VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|CHI||||%s OnTelegramReceived %d/%d", m_szObjectName, m_nDatagramWriterIndex, m_dwReadBytes);

          // next step: receive SOM
          m_nBuildDatagramStep = 0;
        }
				else
				{
					m_byDatagramBuffer[m_nDatagramWriterIndex++] = cValue;
				}
			  
				break;
    }
  } // while(m_dwRXBufIndex < m_dwReadBytes)

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Process received telegram
//@
BOOL CChIntNetLink::OnTelegramReceived(int nTelegramLen)
{
	STRUCT_MSGMANAGER_QUEUE_RECORD csSTRUCT_MSGMANAGER_QUEUE_RECORD;
	STRUCT_CHI_NETLINK_QUEUE_RECORD csSTRUCT_CHI_NETLINK_QUEUE_RECORD;

	// count telegrams
	m_LDI.nNumRXTelegrams++;
  
	char szLogBuffer[128];
	memset(szLogBuffer, 0, sizeof(szLogBuffer));
	for (int i = 0; i < nTelegramLen; i++)
	{
		sprintf(szLogBuffer, "%s%.02X-", szLogBuffer, m_byDatagramBuffer[i]);
		if ((i + 1) % 16 == 0)
		{
			VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_1_CHI, __FILE__, __LINE__, 0, "|CHI||||%s RX: %s", m_szObjectName, szLogBuffer);
			memset(szLogBuffer, 0, sizeof(szLogBuffer));
		}
	}
	VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_1_CHI, __FILE__, __LINE__, 0, "|CHI||||%s RX: %s", m_szObjectName, szLogBuffer);

	if (nTelegramLen > sizeof(csSTRUCT_CHI_NETLINK_QUEUE_RECORD))
	{
		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|CHI||||%s RX msg too long: %d bytes", m_szObjectName, nTelegramLen);
		return FALSE;
	}

	memcpy(&csSTRUCT_CHI_NETLINK_QUEUE_RECORD, m_byDatagramBuffer, nTelegramLen);


	// Heartreat message  (first byte MsgId 0=heart beat)
	if (csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.byMsgID == G_pProtocolConverter_FX->GetGSSMsgIndex("HB"))  // 
	{
		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_0_DEBUG, __FILE__, __LINE__, 0, "|CHI||||%s RX: HB (LIFE MSG)", m_szObjectName);
		return TRUE;
	}

  
	csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode = csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.byMsgID; //TODO define MSG_INT;
	csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen = csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen;
	if (csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen > sizeof(csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgData))
	{
		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|CHI||||%s message %d too big %d !!!!!!", m_szObjectName, csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode, csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen);
		csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen = 0;
	}
	else
	{
		memcpy(csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgData, csSTRUCT_CHI_NETLINK_QUEUE_RECORD.szMsgData, csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen);
		VALIDPTR(G_pLinksManager)->m_QueueInternal.PutItem((BYTE*)&csSTRUCT_MSGMANAGER_QUEUE_RECORD);
	}

	return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Process RX timeout
//@
//@ No message was received within rx timeout.
//@ 
//@ 
//@
BOOL CChIntNetLink::OnRXTimeout()
{
    m_LDI.nNumRXTimeout++;
		m_nTxTimeout++;

		if (m_nTxTimeout < 3)
			return FALSE;

		VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|CHI||||%s RX TIMEOUT (Missing LIFE)", m_szObjectName);

	  return TRUE;
}



//@//////////////////////////////////////////////////////////////////////
//@
//@ Connected
//@
BOOL CChIntNetLink::OnConnected()
{
	m_nTxTimeout = 0; 
	m_bLinkON=TRUE;
  m_nTxRetryCounter=0;
  m_nBuildDatagramStep=0;
  m_nDatagramWriterIndex=0;

	VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_8_WARNINGS,__FILE__,__LINE__,0, "|CHI||||%s CONNECTED", m_szObjectName);
	VALIDPTR(G_pLinksManager)->SendGSSLinksDiagno();
	G_pCommLayerWnd->PostMessageW(WM_REFRESH_LINKS);

  return TRUE;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Connection broken
//@
BOOL CChIntNetLink::OnConnectionBroken()
{
  // one shot action
  if(	m_bLinkON==TRUE)
  {
	  m_bLinkON=FALSE;
	  VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_8_WARNINGS,__FILE__,__LINE__,0, "|CHI||||%s CONNECTION DROPPED", m_szObjectName);
  }

	G_pCommLayerWnd->PostMessageW(WM_REFRESH_LINKS);

  return TRUE;

}

BOOL CChIntNetLink::On_TX_HB_Timeout()
{
	STRUCT_MSGMANAGER_QUEUE_RECORD csSTRUCT_MSGMANAGER_QUEUE_RECORD;
	STRUCT_CHI_NETLINK_QUEUE_RECORD csSTRUCT_CHI_NETLINK_QUEUE_RECORD;

	//VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|CHI||||%s On_TX_HB_Timeout", m_szObjectName);

	// send a dummy message as life telegram
	csSTRUCT_CHI_NETLINK_QUEUE_RECORD.csSTRUCT_CHI_NETLINK_QUEUE_RECORD_HEADER.wMsgLen = 0; // sizeof(csSTRUCT_MSGMANAGER_QUEUE_RECORD.byMsgCode) + sizeof(csSTRUCT_MSGMANAGER_QUEUE_RECORD.wMsgLen);
	m_queueSend.PutItem((BYTE*)&csSTRUCT_CHI_NETLINK_QUEUE_RECORD);

	return TRUE;

}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Worker thread for TIM protocol channel management
//@
//@ Parameters:
//@		[IN]       pParam  : pointer to parent object (this)
//@				
//@ Return value:
//@    exit value
//@
//@ Note:
//@    worker thread, Server - Full duplex
//@
//@////////////////////////////////////////////////////////////////////
/*static*/DWORD WINAPI CChIntNetLink::EngineThread(LPVOID pParam)
{
enum tagEventsName
{
  ID_EXIT_EVENT=0,
	ID_LINK_LOSS_TIMEOUT_EVENT,
	ID_TX_HB_TIMEOUT_EVENT,
	ID_RECV_EVENT,
  ID_READ_SENDQUEUE_EVENT,

  NUM_EVENTS
};

enum tagSteps
{
  STEP_CREATE=0,
  STEP_OPEN,
  STEP_IDLE,
  STEP_RECEIVE,
  STEP_SEND,
  STEP_RECYCLE,
  STEP_EXIT,
  STEP_EXIT_ON_ERROR,
  STEP_FREE,

  NUM_STEPS
};

const long TO_MILLISECONDS = -10000L;  // for milliseconds conversion
const long NET_TIMEOUT    = 1000L;    // Network timeout

CChIntNetLink* _this=NULL;
BOOL bRun=TRUE;
tagSteps step=STEP_IDLE;
//
LARGE_INTEGER liLinkLossTimeout;
LARGE_INTEGER li_TX_HB_Timeout;
//
HANDLE hEvents[NUM_EVENTS];
COverlappedSocket* pLink=NULL;


try
{
  _this = (CChIntNetLink*)pParam;
  hEvents[ID_EXIT_EVENT] = _this->GetExitHandle();
  hEvents[ID_LINK_LOSS_TIMEOUT_EVENT] = CreateWaitableTimer(NULL, FALSE, NULL);
	hEvents[ID_TX_HB_TIMEOUT_EVENT] = CreateWaitableTimer(NULL, FALSE, NULL);
  hEvents[ID_READ_SENDQUEUE_EVENT] = _this->m_queueSend.GetEvReadHandle();

  _this->SetObjectReady();
  
	step=STEP_CREATE;

	liLinkLossTimeout.QuadPart = _this->m_csSTRUCT_CHI_NETLINK_INIT.lLinkLossTimeout * TO_MILLISECONDS;
	li_TX_HB_Timeout.QuadPart = 5000 * TO_MILLISECONDS;
  // Loop
  while(bRun)
  {
    switch(step)
    {
      case STEP_CREATE:

        // check for exit request
        if(::WaitForSingleObject(hEvents[ID_EXIT_EVENT] ,0) == 0)
        {
          step=STEP_EXIT;
          break;
        }

        if( pLink == NULL)
        {
          // Create a client object
					if (_this->m_csSTRUCT_CHI_NETLINK_INIT.bSocketServer)
						pLink = new COverlappedSocketSR;
					else
						pLink = new COverlappedSocketCL;

          if(pLink == NULL)
					{
            step=STEP_EXIT_ON_ERROR;
						_this->OnMemoryOverflow();
          }
        }

			  _this->OnCreate();

        if( pLink->Create(_this->m_csSTRUCT_CHI_NETLINK_INIT.wszIPAddr, _this->m_csSTRUCT_CHI_NETLINK_INIT.nPortID) != TRUE)
        {  
          // step=STEP_EXIT_ON_ERROR;
					
					// Retry instead of quit
					step=STEP_RECYCLE;
					_this->m_linkStatus = LINK_ERROR;
          //
					_this->OnCreateError();
        }
        else
        {
          hEvents[ID_RECV_EVENT] = pLink->GetNetEventHandle();
          _this->m_linkStatus = LINK_WAITING;
          
					// next step
					step=STEP_OPEN;
        }
      break;

      case STEP_OPEN:
        if( pLink->OpenLink( hEvents[ID_EXIT_EVENT], 1000/*WSA_INFINITE*/) != TRUE)
        {  
          switch(pLink->GetLastError())
          {
            case COverlappedSocketCL::ERR_USER_ABORT:
              step=STEP_EXIT;
            break;

            case COverlappedSocketCL::ERR_TIMEOUT:
						case COverlappedSocketCL::ERR_CONNECT_IN_PROGRESS:
            break;

            // errore
            default:
							step=STEP_EXIT_ON_ERROR;
              //
							_this->OnOpenFailed();
            break;
          }
        }
        else
        {
          // wait for peer ready
					::Sleep(100);

          // start link loss timer if required
					//if (_this->m_csSTRUCT_CHI_NETLINK_INIT.lLinkLossTimeout > 0)
					if(li_TX_HB_Timeout.QuadPart != 0)
					{
						SetWaitableTimer(hEvents[ID_LINK_LOSS_TIMEOUT_EVENT], &li_TX_HB_Timeout, 0L, NULL, NULL, FALSE);
						SetWaitableTimer(hEvents[ID_TX_HB_TIMEOUT_EVENT], &li_TX_HB_Timeout, 0L, NULL, NULL, FALSE);
					}

          _this->m_linkStatus = LINK_RUNNING;
 				  _this->OnConnected();
          
					// next step
          step=STEP_IDLE;
        }
      break;

      case STEP_IDLE:
				// Waiting
        switch(::WaitForMultipleObjects(NUM_EVENTS, hEvents, FALSE, INFINITE))
        {
          // Message to send
          case ID_READ_SENDQUEUE_EVENT:

						step = STEP_SEND;

          break;

          // Message received
          case ID_RECV_EVENT:

            // restart link loss timer if required
            //if(_this->m_csSTRUCT_CHI_NETLINK_INIT.lLinkLossTimeout > 0)
            SetWaitableTimer(hEvents[ID_LINK_LOSS_TIMEOUT_EVENT] ,&liLinkLossTimeout,0L,NULL,NULL,FALSE);

						//VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_9_ERRORS, __FILE__, __LINE__, 0, "|CHI||||%s RX: ID_RECV_EVENT", _this->m_szObjectName);

						_this->m_nTxTimeout = 0;

            step=STEP_RECEIVE;
          break;

          // receiving timeout
          case ID_LINK_LOSS_TIMEOUT_EVENT:
						// 
						SetWaitableTimer(hEvents[ID_LINK_LOSS_TIMEOUT_EVENT], &li_TX_HB_Timeout, 0L, NULL, NULL, FALSE);
						if (_this->OnRXTimeout() == TRUE)
						{
							// comment it out to avoid closing channel during debuggin
							step = STEP_RECYCLE;
						}
						else
						{
              // restart link loss timer if required
              //if(_this->m_csSTRUCT_CHI_NETLINK_INIT.lLinkLossTimeout > 0)
              //  SetWaitableTimer(hEvents[ID_LINK_LOSS_TIMEOUT_EVENT] ,&liLinkLossTimeout,0L,NULL,NULL,FALSE);
						};
          break;

					case ID_TX_HB_TIMEOUT_EVENT:
						SetWaitableTimer(hEvents[ID_TX_HB_TIMEOUT_EVENT], &li_TX_HB_Timeout, 0L, NULL, NULL, FALSE);
						// send life telegram
						_this->On_TX_HB_Timeout();
						break;

          // Exit request
          case ID_EXIT_EVENT:
						step = STEP_EXIT;
					default:
            step=STEP_EXIT;
          break;

				} // end switch
			break;

      case STEP_RECEIVE:

				if( _this->OnReceiveBegin() != TRUE)
				{
          step = STEP_IDLE;
					break;
				}

				VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_0_DEBUG, __FILE__, __LINE__, 0, "|CHI||||%s RX new MSG", _this->m_szObjectName);

				if (pLink->ReadMessage(&_this->m_wsaRxData, &_this->m_dwReadBytes) != TRUE)
        {
					int nLasterror = pLink->GetLastError();
					switch (nLasterror)
          {
            case COverlappedSocketCL::ERR_NO_DATA:
							_this->OnRxEmpty();
              step=STEP_IDLE;
            break;

            default:
							_this->OnRxError(nLasterror);
              step=STEP_RECYCLE;
            break;
          }  
        }
        else
        {
					step=STEP_IDLE;

					VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_0_DEBUG, __FILE__, __LINE__, 0, "|CHI||||%s RX new MSG Len: %d", _this->m_szObjectName, _this->m_dwReadBytes);

					// dump telegram
					_this->m_wsaRxData.buf[_this->m_dwReadBytes] = 0;

          _this->m_LDI.nNumRx++;
          _this->m_dwRXBufIndex = 0;
          //					
				  _this->OnReceiveEnd();

        } // end else
			break;

      case STEP_SEND:
      {
      	STRUCT_CHI_NETLINK_QUEUE_RECORD csSTRUCT_CHI_NETLINK_QUEUE_RECORD;
	      //
	      while(_this->m_queueSend.GetItem((BYTE*)&csSTRUCT_CHI_NETLINK_QUEUE_RECORD) )
	      {
				  if( _this->OnSendBegin(csSTRUCT_CHI_NETLINK_QUEUE_RECORD) != TRUE)
				  {
            step = STEP_IDLE;
					  break;
				  }

          if( pLink->WriteMessage(hEvents[ID_EXIT_EVENT], NET_TIMEOUT/*WSA_INFINITE*/, &_this->m_wsaTxData, &_this->m_dwWriteBytes) != TRUE)
          {  
            switch(pLink->GetLastError())
            {
              case COverlappedSocketCL::ERR_USER_ABORT:
                step=STEP_EXIT;
              break;

              case COverlappedSocketCL::ERR_TIMEOUT:
							  _this->OnTxTimeout();
                step=STEP_RECYCLE;
              break;

              // errore
              default:
							  _this->OnTxError();
                step=STEP_RECYCLE;
              break;
            }
          }
          
          else
          {
						char szLogBuffer[128];
						memset(szLogBuffer, 0, sizeof(szLogBuffer));

						// dump telegram
					  _this->m_wsaTxData.buf[_this->m_dwWriteBytes] = 0;

						for (int i = 1; i < _this->m_dwWriteBytes - 1; i++)
						{
							sprintf(szLogBuffer, "%s%.02X-", szLogBuffer, _this->m_wsaTxData.buf[i]);
							if ((i + 1) % 16 == 0)
							{
								VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_1_CHI, __FILE__, __LINE__, 0, "|CHI||||%s TX: %s", _this->m_szObjectName, szLogBuffer);
								memset(szLogBuffer, 0, sizeof(szLogBuffer));
							}
						}
						VALIDPTR(G_pTrace)->TRACE_LIB(LEVEL_1_CHI, __FILE__, __LINE__, 0, "|CHI||||%s TX: %s", _this->m_szObjectName, szLogBuffer);

						SetWaitableTimer(hEvents[ID_TX_HB_TIMEOUT_EVENT], &li_TX_HB_Timeout, 0L, NULL, NULL, FALSE);

						_this->OnSendEnd();
            step=STEP_IDLE;
          }
        } // end while
      }
      break;

      case STEP_RECYCLE:
				
				_this->OnConnectionBroken();

				_this->m_linkStatus = LINK_ERROR;

        // Stop timers
        CancelWaitableTimer(hEvents[ID_LINK_LOSS_TIMEOUT_EVENT]);

        if(pLink != NULL)
        {
          delete pLink;
          pLink = NULL;
        }

        step=STEP_CREATE;
				// Retry delay on <create> error
        ::Sleep(500);
      break;

      case STEP_EXIT:
        _this->m_linkStatus = LINK_STOPPED;
        step=STEP_FREE;
      break;
  

      case STEP_EXIT_ON_ERROR:
        _this->m_linkStatus = LINK_ERROR;
        step=STEP_FREE;
      break;

      case STEP_FREE:

        if(hEvents[ID_LINK_LOSS_TIMEOUT_EVENT] != NULL)
          CloseHandle(hEvents[ID_LINK_LOSS_TIMEOUT_EVENT]);

        if(pLink != NULL)
        {
          delete pLink;
          pLink = NULL;
        }

        bRun=FALSE;

				_this->OnShutdown();
      break;

		} // end switch(step)

		::Sleep(1);
	
	} // end while(bRun)


	return 0;
}
catch(CFOLibException& e)
{
	CBaseLib::SetBit(13, TRUE);
	CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"%.250s", e.m_szDescription);	
	return 1;
}
catch(...)
{
	CBaseLib::SetBit(13, TRUE);
	CBaseLib::LogEvent(EVENTLOG_ERROR_TYPE, L"Exception on Thread %.20s", _this->CThreadLib::m_wszObjectName);
	return 1;
}
	
}

