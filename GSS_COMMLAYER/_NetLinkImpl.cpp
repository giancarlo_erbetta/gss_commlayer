// NetLinkImpl.cpp: implementation of the CNetLinkImpl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NetLinkImpl.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//@//////////////////////////////////////////////////////////////////////
//@
//@ Constructor CNetLinkImpl::CNetLinkImpl
//@
//@ Parameters:
//@		 [IN]       STRUCT_NETLINK_INIT& rcsInit
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CNetLinkImpl::CNetLinkImpl(STRUCT_NETLINK_INIT& rcsInit)
:	m_queueSend(rcsInit.nQueueSendRecordSize, rcsInit.nQueueSendRecords, rcsInit.wszQueueSendName),
	m_queueLamp(rcsInit.nQueueLampRecordSize, rcsInit.nQueueLampRecords, rcsInit.wszQueueLampName),
  SIZE_BUF_TX(5120),
	SIZE_BUF_RX(5120),
	SOM_CH(0xFD),
	EOM_CH(0xFE),
	m_bySourceNode(rcsInit.bySourceNode),
	m_byDestNode(rcsInit.byDestNode)
{
	m_dwWriteBytes=0; 
	m_dwReadBytes=0; 
	m_byProgTx=1;
	m_byProgRx=0;

  // initialization
	m_wsaTxData.len = SIZE_BUF_TX;
  m_wsaTxData.buf = new char[m_wsaTxData.len];
  if(m_wsaTxData.buf == NULL)
		throw CFOLibException(0, L"CNetLinkImpl: m_wsaTxData.buf == NULL");

	m_wsaRxData.len = SIZE_BUF_RX;
  m_wsaRxData.buf = new char[m_wsaRxData.len];
  if(m_wsaRxData.buf == NULL)
		throw CFOLibException(0, L"CNetLinkImpl: m_wsaRxData.buf == NULL");

  m_pbyRxMsg = new BYTE[SIZE_BUF_RX];
  if(m_pbyRxMsg == NULL)
		throw CFOLibException(0, L"CNetLinkImpl: m_pbyRxMsg == NULL");

	//
	m_nRxStep = 0;
}

//@//////////////////////////////////////////////////////////////////////
//@
//@ Distructor CNetLinkImpl::~CNetLinkImpl
//@
//@ Parameters:
//@    none
//@				
//@ Return value:
//@    none
//@
//@ Note:
//@
//@////////////////////////////////////////////////////////////////////
CNetLinkImpl::~CNetLinkImpl()
{
	if(m_wsaTxData.buf != NULL)
	{
		delete[] m_wsaTxData.buf;
		m_wsaTxData.buf = NULL;
	}

	if(m_wsaRxData.buf != NULL)
	{
		delete[] m_wsaRxData.buf;
		m_wsaRxData.buf = NULL;
	}

  if(m_pbyRxMsg != NULL)
	{
		delete m_pbyRxMsg;
		m_pbyRxMsg = NULL;
	}

}

