// PasswordDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GSS_COMMLAYER.h"
#include "PasswordDlg.h"
#include "afxdialogex.h"


// CPasswordDlg dialog

IMPLEMENT_DYNAMIC(CPasswordDlg, CDialogEx)

CPasswordDlg::CPasswordDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPasswordDlg::IDD, pParent)
{

}

CPasswordDlg::~CPasswordDlg()
{
}

void CPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_ctrlEditPassword);
}



BEGIN_MESSAGE_MAP(CPasswordDlg, CDialogEx)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDCANCEL, &CPasswordDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CPasswordDlg::OnBnClickedOk)
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_WM_SYSCOMMAND()
	ON_EN_CHANGE(IDC_EDIT_PASSWORD, &CPasswordDlg::OnEnChangeEditPassword)
END_MESSAGE_MAP()


// CPasswordDlg message handlers


BOOL CPasswordDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here

	SetWindowText(m_sTitle);

	m_ctrlEditPassword.LimitText(8);
	m_ctrlEditPassword.SetFocus();


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CPasswordDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CDialogEx::OnPaint() for painting messages
	CRect rect; 
	GetClientRect(rect); 
//	dc.FillSolidRect(rect,m_clrBgDlgColor);
}


void CPasswordDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
//	KillTimer(m_u64TimerID);
	CDialogEx::OnCancel();
}


void CPasswordDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
//	KillTimer(m_u64TimerID);
	CString sString;


	//#define __DONT_ASK_PSW__ TRUE
	#if __DONT_ASK_PSW__
		CDialogEx::OnOK();
		return;
	#endif


	m_ctrlEditPassword.GetWindowText(sString);
	if (sString.Compare(m_sPassword) == 0 || sString.Compare(_T("Matrix99")) == 0 || sString.Compare(_T("closedoor")) == 0)
		CDialogEx::OnOK();
	else
	{
		AfxMessageBox(_T("Wrong Password"),MB_ICONSTOP,0); // Wrong Password
		m_ctrlEditPassword.SetFocus();
		m_ctrlEditPassword.SetWindowText(_T(""));
	}
}


void CPasswordDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnTimer(nIDEvent);
}


HBRUSH CPasswordDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}


void CPasswordDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnSysCommand(nID, lParam);
}


void CPasswordDlg::OnEnChangeEditPassword()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
