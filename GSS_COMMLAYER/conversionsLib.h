//@////////////////////////////////////////////////////////////
//@
//@	Class:		CconversionsLib
//@
//@	Compiler:	Visual C++
//@	Tested on:	Visual C++ 6.0 and Windows 2000
//@				
//@	Created:	06 February 2001
//@
//@	Author:		Giuseppe Alessio
//@
//@	Updated:	
//@ 17 April 2002
//@
//@ 28 January 2005
//@ Support to BaseLib
//@
//@ 11 February 2005
//@ Added SWAP functions
//@
//@////////////////////////////////////////////////////////////

#ifndef _CONVERSIONSLIB_H
#define _CONVERSIONSLIB_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CConversionsLib  
{
public:
	inline void TimetToFileTime( time_t t, LPFILETIME pft );
	inline long DeltaSystemTimeS(const SYSTEMTIME st1, const SYSTEMTIME st2); // t2 - t1
	//
  inline void IntelToMotorola( WORD* pwDest, WORD wSrc);
  inline void IntelToMotorola( DWORD* pdwDest, DWORD dwSrc);
  inline void MotorolaToIntel( WORD* pwDest, WORD wSrc);
  inline void MotorolaToIntel( DWORD* pdwDest, DWORD dwSrc);
	// same as above (old functions kept for compatibility)
  inline static void SWAP( WORD* pwDest, WORD wSrc);
	inline static void SWAP( DWORD* pdwDest, DWORD dwSrc);
	//
	inline static int ToInt32(char* pszString, int nStringLen);
	inline static DWORD ToDWORD(char* pszString, int nStringLen);
	inline static float ToFloat(char* pszString, int nStringLen);
	//	
	CConversionsLib();
	virtual ~CConversionsLib();

};

inline long CConversionsLib::DeltaSystemTimeS(const SYSTEMTIME st1, const SYSTEMTIME st2) // t2 - t1
{
	union timeunion {
      FILETIME fileTime;
      ULARGE_INTEGER ul;
  } ;

  timeunion ft1;
  timeunion ft2;

  SystemTimeToFileTime(&st1, &ft1.fileTime);
  SystemTimeToFileTime(&st2, &ft2.fileTime);

  return (long) ((ft2.ul.QuadPart - ft1.ul.QuadPart)/10000000);

}

// Converts a time_t value to a FILETIME value
inline void CConversionsLib::TimetToFileTime(time_t t, LPFILETIME pft)
{
	LONGLONG ll = Int32x32To64(t, 10000000) + 116444736000000000;
	pft->dwLowDateTime = (DWORD) ll;
#pragma warning(disable : 4244)
	pft->dwHighDateTime = ll >>32;
#pragma warning(default : 4244)
}

// Swaps a word from Intel to Motorola format
inline void CConversionsLib::IntelToMotorola( WORD* pwDest, WORD wSrc)
{
  WORD wTemp = wSrc;
  ((BYTE*)pwDest)[0]=((BYTE*)&wTemp)[1];
  ((BYTE*)pwDest)[1]=((BYTE*)&wTemp)[0];
}

// Swaps a double word from Intel to Motorola format
inline void CConversionsLib::IntelToMotorola( DWORD* pdwDest, DWORD dwSrc)
{
  DWORD dwTemp=dwSrc;
  ((BYTE*)pdwDest)[0]=((BYTE*)&dwTemp)[3];
  ((BYTE*)pdwDest)[1]=((BYTE*)&dwTemp)[2];
  ((BYTE*)pdwDest)[2]=((BYTE*)&dwTemp)[1];
  ((BYTE*)pdwDest)[3]=((BYTE*)&dwTemp)[0];
}

// Swaps a word from Motorola to Intel format
inline void CConversionsLib::MotorolaToIntel( WORD* pwDest, WORD wSrc)
{
  WORD wTemp = wSrc;
  ((BYTE*)pwDest)[1]=((BYTE*)&wTemp)[0];
  ((BYTE*)pwDest)[0]=((BYTE*)&wTemp)[1];
}

// Swaps a double word from Motorola to Intel format
inline void CConversionsLib::MotorolaToIntel( DWORD* pdwDest, DWORD dwSrc)
{
  DWORD dwTemp=dwSrc;
  ((BYTE*)pdwDest)[3]=((BYTE*)&dwTemp)[0];
  ((BYTE*)pdwDest)[2]=((BYTE*)&dwTemp)[1];
  ((BYTE*)pdwDest)[1]=((BYTE*)&dwTemp)[2];
  ((BYTE*)pdwDest)[0]=((BYTE*)&dwTemp)[3];
}

// Swaps a word 
void CConversionsLib::SWAP( WORD* pwDest, WORD wSrc)
{
  WORD wTemp = wSrc;
  ((BYTE*)pwDest)[1]=((BYTE*)&wTemp)[0];
  ((BYTE*)pwDest)[0]=((BYTE*)&wTemp)[1];
}

// Swaps a double word
void CConversionsLib::SWAP( DWORD* pdwDest, DWORD dwSrc)
{
  DWORD dwTemp=dwSrc;
  ((BYTE*)pdwDest)[3]=((BYTE*)&dwTemp)[0];
  ((BYTE*)pdwDest)[2]=((BYTE*)&dwTemp)[1];
  ((BYTE*)pdwDest)[1]=((BYTE*)&dwTemp)[2];
  ((BYTE*)pdwDest)[0]=((BYTE*)&dwTemp)[3];
}

int CConversionsLib::ToInt32(char* pszString, int nStringLen)
{
	if( pszString == NULL || nStringLen <= 0 || nStringLen > MAX_PATH)
		return -1;

	char szString[MAX_PATH+1];
	memcpy(szString, pszString, nStringLen);
	szString[nStringLen]=NULL;
	return atoi(szString);
}

DWORD CConversionsLib::ToDWORD(char* pszString, int nStringLen)
{
	if( pszString == NULL || nStringLen <= 0 || nStringLen > MAX_PATH)
		return -1;

	char szString[MAX_PATH+1];
	memcpy(szString, pszString, nStringLen);
	szString[nStringLen]=NULL;
	return (DWORD)atol(szString);
}

float CConversionsLib::ToFloat(char* pszString, int nStringLen)
{
	if( pszString == NULL || nStringLen <= 0 || nStringLen > MAX_PATH)
		return -1.0;

	char szString[MAX_PATH+1];
	memcpy(szString, pszString, nStringLen);
	szString[nStringLen]=NULL;
	return (float)atof(szString);
}

#endif
